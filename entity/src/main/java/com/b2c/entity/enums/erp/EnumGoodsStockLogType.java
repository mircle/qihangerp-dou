package com.b2c.entity.enums.erp;

/**
 * 描述：
 * 商品库存日志类型
 *
 * @author qlp
 * @date 2019-09-26 09:17
 */
public enum EnumGoodsStockLogType {
    IN("入库", 1),
    OUT("出库", 2);
//    PUR_RETURN("采购退货出库",3);

    private String name;
    private int index;

    // 构造方法
    private EnumGoodsStockLogType(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumGoodsStockLogType c : EnumGoodsStockLogType.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
