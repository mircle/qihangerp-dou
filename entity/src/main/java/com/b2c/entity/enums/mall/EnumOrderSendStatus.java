package com.b2c.entity.enums.mall;

/**
 * 描述：
 * 订单发货状态
 *
 * @author qlp
 * @date 2019-10-17 09:53
 */
public enum EnumOrderSendStatus {
    //发货状态（0待出库1拣货中2已拣货3已出库4已发货）
    WaitOut("待出库", 0),
    Picking("拣货中", 1),
    Picked("已拣货", 2),
    Outed("已出库", 3),
    Send("已发货", 4);

    private String name;
    private int index;

    // 构造方法
    private EnumOrderSendStatus(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumOrderSendStatus c : EnumOrderSendStatus.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
