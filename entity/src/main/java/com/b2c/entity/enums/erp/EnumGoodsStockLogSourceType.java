package com.b2c.entity.enums.erp;

/**
 * 描述：
 * 商品库存明细 来源type
 *
 * @author qlp
 * @date 2019-10-08 16:43
 */
public enum EnumGoodsStockLogSourceType {
    CheckoutIn("采购验货入库", 1),
    PurchaseStockIn("采购入库", 8),
    OrderSend("订单出库", 2),
    PUR_RETURN("采购退货出库",3),
    MOVE_IN("商品移库",4),
    Order_Return_In("退货入库",5),
    COUNTED("盘点",10),
    LOSS("报损出库",9);

    private String name;
    private int index;

    // 构造方法
    private EnumGoodsStockLogSourceType(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumGoodsStockLogSourceType c : EnumGoodsStockLogSourceType.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
