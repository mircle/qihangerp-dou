package com.b2c.entity.enums.mall;

/**
 * 描述：
 * 订单支付状态
 *
 * @author qlp
 * @date 2019-02-21 15:00
 */
public enum EnumOrderPaymentState {
    //支付状态（0未支付1支付中2支付成功3支付失败）
    NotPay("未支付", 0),
    Paying("支付中", 1),
    Success("已支付", 2),//支付成功
    Fail("支付失败", 3);

    // 成员变量
    private String name;
    private int index;

    // 构造方法
    private EnumOrderPaymentState(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumOrderPaymentState c : EnumOrderPaymentState.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }


}
