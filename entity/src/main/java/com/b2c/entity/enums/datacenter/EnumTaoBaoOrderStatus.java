package com.b2c.entity.enums.datacenter;


/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-23 09:33
 */
public enum EnumTaoBaoOrderStatus {
    waitbuyerpay("等待付款", 1),
    waitsellersend("等待卖家发货", 2),
    waitsend("等待发货", 2),
    send_goods("已发货", 3),
    Complete("交易成功", 9);

    private String name;
    private int index;

    // 构造方法
    private EnumTaoBaoOrderStatus(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumTaoBaoOrderStatus c : EnumTaoBaoOrderStatus.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // 普通方法
    public static Integer getIndex(String name) {
        for (EnumTaoBaoOrderStatus c : EnumTaoBaoOrderStatus.values()) {
            if (c.getName().equals(name)) {
                return c.index;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
