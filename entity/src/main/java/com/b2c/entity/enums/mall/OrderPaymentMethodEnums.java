package com.b2c.entity.enums.mall;//package com.b2c.enums.mall;
//
///**
// * 描述：
// * 订单支付方式
// *
// * @author qlp
// * @date 2019-02-21 15:03
// */
//public enum OrderPaymentMethodEnums {
//    //支付方式（1微信支付，2支付宝支付）
//    WxPay("微信", 1),
//    AliPay("支付宝", 2),
//    OFFLINE_PAY("线下支付", 3),
//    Third_PAY("第三方平台支付", 4);
//
//    // 成员变量
//    private String name;
//    private int index;
//
//    // 构造方法
//    private OrderPaymentMethodEnums(String name, int index) {
//        this.name = name;
//        this.index = index;
//    }
//
//    // 普通方法
//    public static String getName(int index) {
//        for (OrderPaymentMethodEnums c : OrderPaymentMethodEnums.values()) {
//            if (c.getIndex() == index) {
//                return c.name;
//            }
//        }
//        return null;
//    }
//
//    // get set 方法
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public int getIndex() {
//        return index;
//    }
//
//    public void setIndex(int index) {
//        this.index = index;
//    }
//}
