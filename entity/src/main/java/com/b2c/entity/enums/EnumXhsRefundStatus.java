package com.b2c.entity.enums;

public enum EnumXhsRefundStatus {
    //售后状态 1:待审核 2:待用户寄回 3:待收货 4:完成 5:取消 6:关闭 9:拒绝 9999:删除
    STATUS_1("待审核",  1),
    STATUS_2("待用户寄回",  2),
    STATUS_3("待收货",  3),
    STATUS_4("完成",  4),
    STATUS_5("取消",  5),
    STATUS_6("关闭",  6),
    STATUS_9("拒绝",  9),
    DELETE("删除", 9999);

    // 成员变量
    private String name;
    private Integer index;

    // 构造方法
    private EnumXhsRefundStatus(String name, Integer index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getStatusName(Integer index) {
        for (EnumXhsRefundStatus c : EnumXhsRefundStatus.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
