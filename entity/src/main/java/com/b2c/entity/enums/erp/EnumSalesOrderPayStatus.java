package com.b2c.entity.enums.erp;

public enum EnumSalesOrderPayStatus {
    //付款状态（0未付款1部分付款2完全付款）
    NotPay("未付款", 0),
    PartPay("部分付款", 1),
    CompletePay("完全付款", 2);

    private String name;
    private int index;

    // 构造方法
    private EnumSalesOrderPayStatus(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumSalesOrderPayStatus c : EnumSalesOrderPayStatus.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
