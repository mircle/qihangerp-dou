package com.b2c.entity;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-09 14:51
 */
public class ErpStockInFormItemEntity {
    private Long id;//` bigint(20) NOT NULL AUTO_INCREMENT,
    private Long formId;//` bigint(20) NOT NULL COMMENT '入库单id',
    private Integer goodsId;//` int(11) NOT NULL COMMENT '商品id',
    private Integer specId;//` int(11) NOT NULL COMMENT '商品规格id',
    private Integer locationId;//` int(11) NOT NULL COMMENT '入库仓位',
    private Long quantity;//` bigint(20) NOT NULL COMMENT '总数量',
    private String goodsName;
    private String goodsNumber;
    private String specNumber;
    private String colorImage;
    private String colorValue;
    private String sizeValue;
    private String unitName;
    private String locationNumber;

    public String getColorImage() {
        return colorImage;
    }

    public void setColorImage(String colorImage) {
        this.colorImage = colorImage;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public String getLocationNumber() {
        return locationNumber;
    }

    public void setLocationNumber(String locationNumber) {
        this.locationNumber = locationNumber;
    }
}
