package com.b2c.entity.xhs;

public class XhsRefundEntity {
    private Long id;//` BIGINT(20) NOT NULL AUTO_INCREMENT,
    private String returnsId;//` VARCHAR(50) NOT NULL COMMENT '小红书店铺售后id' COLLATE 'utf8_general_ci',
    private Integer returnType;//` INT(11) NOT NULL COMMENT '退货类型 1-退货退款, 2-换货, 3:仅退款(old) 4:仅退款(new) 理论上不会有3出现 5:未发货仅退款',
    private Integer reasonId;//` INT(11) NULL DEFAULT NULL COMMENT '售后原因id',
    private String reason;//` VARCHAR(50) NOT NULL COMMENT '售后原因说明' COLLATE 'utf8_general_ci',
    private Integer status;//` INT(11) NOT NULL COMMENT '售后状态 1:待审核 2:待用户寄回 3:待收货 4:完成 5:取消 6:关闭 9:拒绝 9999:删除',
    private Integer subStatus;//` INT(11) NULL DEFAULT NULL COMMENT '售后子状态 301-待审核 302-快递已签收 304-收货异常',
    private Short receiveAbnormalType;//` INT(11) NULL DEFAULT NULL COMMENT '收货异常类型',
    private String packageId;//` VARCHAR(50) NOT NULL COMMENT '订单id' COLLATE 'utf8_general_ci',
    private String exchangePackageId;//` VARCHAR(50) NOT NULL COMMENT '换货订单id' COLLATE 'utf8_general_ci',
    private Long createdTime;//` BIGINT(20) NOT NULL DEFAULT '0',
    private String returnExpressNo;//` VARCHAR(50) NOT NULL DEFAULT '0' COLLATE 'utf8_general_ci',
    private String returnExpressCompany;//` VARCHAR(50) NOT NULL DEFAULT '0' COLLATE 'utf8_general_ci',
    private String returnAddress;//` VARCHAR(50) NOT NULL DEFAULT '0' COLLATE 'utf8_general_ci',
    private Integer shipNeeded;//` INT(11) NOT NULL DEFAULT '0' COMMENT '是否需要寄回 1-需要 0-不需要',
    private Integer refunded;//` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '是否已退款',
    private Integer refundStatus;//` INT(11) NOT NULL DEFAULT '0' COMMENT '退款状态 108触发退款 1退款中 3退款失败 2退款成功 401已取消 101已创建 201待审核 301审核通过 302审核不通过 402自动关闭',
    private Long autoReceiveDeadline;//` INT(11) NOT NULL DEFAULT '0' COMMENT '自动确认收货时间',
    private Long updateTime;//` INT(11) NOT NULL DEFAULT '0',
    private String returnExpressCompanyCode;//` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT '退货快递公司编号' COLLATE 'utf8_general_ci',
    private String createOn;//` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    private String modifyOn;//` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    private Integer shopId;
    private Integer erpSendStatus;

    public Integer getErpSendStatus() {
        return erpSendStatus;
    }

    public void setErpSendStatus(Integer erpSendStatus) {
        this.erpSendStatus = erpSendStatus;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReturnsId() {
        return returnsId;
    }

    public void setReturnsId(String returnsId) {
        this.returnsId = returnsId;
    }

    public Integer getReturnType() {
        return returnType;
    }

    public void setReturnType(Integer returnType) {
        this.returnType = returnType;
    }

    public Integer getReasonId() {
        return reasonId;
    }

    public void setReasonId(Integer reasonId) {
        this.reasonId = reasonId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSubStatus() {
        return subStatus;
    }

    public void setSubStatus(Integer subStatus) {
        this.subStatus = subStatus;
    }

    public Short getReceiveAbnormalType() {
        return receiveAbnormalType;
    }

    public void setReceiveAbnormalType(Short receiveAbnormalType) {
        this.receiveAbnormalType = receiveAbnormalType;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getExchangePackageId() {
        return exchangePackageId;
    }

    public void setExchangePackageId(String exchangePackageId) {
        this.exchangePackageId = exchangePackageId;
    }

    public Long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Long createdTime) {
        this.createdTime = createdTime;
    }

    public String getReturnExpressNo() {
        return returnExpressNo;
    }

    public void setReturnExpressNo(String returnExpressNo) {
        this.returnExpressNo = returnExpressNo;
    }

    public String getReturnExpressCompany() {
        return returnExpressCompany;
    }

    public void setReturnExpressCompany(String returnExpressCompany) {
        this.returnExpressCompany = returnExpressCompany;
    }

    public String getReturnAddress() {
        return returnAddress;
    }

    public void setReturnAddress(String returnAddress) {
        this.returnAddress = returnAddress;
    }

    public Integer getShipNeeded() {
        return shipNeeded;
    }

    public void setShipNeeded(Integer shipNeeded) {
        this.shipNeeded = shipNeeded;
    }

    public Integer getRefunded() {
        return refunded;
    }

    public void setRefunded(Integer refunded) {
        this.refunded = refunded;
    }

    public Integer getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(Integer refundStatus) {
        this.refundStatus = refundStatus;
    }

    public Long getAutoReceiveDeadline() {
        return autoReceiveDeadline;
    }

    public void setAutoReceiveDeadline(Long autoReceiveDeadline) {
        this.autoReceiveDeadline = autoReceiveDeadline;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public String getReturnExpressCompanyCode() {
        return returnExpressCompanyCode;
    }

    public void setReturnExpressCompanyCode(String returnExpressCompanyCode) {
        this.returnExpressCompanyCode = returnExpressCompanyCode;
    }

    public String getCreateOn() {
        return createOn;
    }

    public void setCreateOn(String createOn) {
        this.createOn = createOn;
    }

    public String getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(String modifyOn) {
        this.modifyOn = modifyOn;
    }
}
