package com.b2c.entity.apitao;

import java.math.BigDecimal;
import java.util.Date;

public class TaoOrderSalesDataEntity {
    /**
     * 店铺id
     */
    private Integer shopId;
    /**
     * 订单日期
     */
    private Date date;

    /**
     * 订单金额
     */
    private BigDecimal orderAmt;

    /**
     * 订单数量
     */
    private Integer orderNum;

    /**
     * 订单商品数量
     */
    private Integer goodNum;

    /**
     * sku数量
     */
    private Integer skuNum;

    /**
     * 订单销售金额
     */
    private BigDecimal salesAmt;

    /**
     * 赠品订单
     */
    private Integer giftNum;

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getOrderAmt() {
        return orderAmt;
    }

    public void setOrderAmt(BigDecimal orderAmt) {
        this.orderAmt = orderAmt;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getGoodNum() {
        return goodNum;
    }

    public void setGoodNum(Integer goodNum) {
        this.goodNum = goodNum;
    }

    public Integer getSkuNum() {
        return skuNum;
    }

    public void setSkuNum(Integer skuNum) {
        this.skuNum = skuNum;
    }

    public BigDecimal getSalesAmt() {
        return salesAmt;
    }

    public void setSalesAmt(BigDecimal salesAmt) {
        this.salesAmt = salesAmt;
    }

    public Integer getGiftNum() {
        return giftNum;
    }

    public void setGiftNum(Integer giftNum) {
        this.giftNum = giftNum;
    }
}
