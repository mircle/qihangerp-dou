package com.b2c.entity.ecom;

import java.math.BigDecimal;

public class GoodsStyleDataEntity {
    private Long id;
    private String goodsId;
    private String goodsNum;
    private String img;
    private String startDate;
    private String endDate;
    private BigDecimal ctr;
    private BigDecimal cvr;
    private Integer collects;
    private Integer carts;
    private Integer orders;
    private String remark;
    private String platform;
    private String createTime;
    private String modifyTime;


    
    public Integer getCollects() {
        return collects;
    }
    public void setCollects(Integer collects) {
        this.collects = collects;
    }
    public Integer getCarts() {
        return carts;
    }
    public void setCarts(Integer carts) {
        this.carts = carts;
    }
    public Integer getOrders() {
        return orders;
    }
    public void setOrders(Integer orders) {
        this.orders = orders;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getGoodsId() {
        return goodsId;
    }
    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }
    public String getGoodsNum() {
        return goodsNum;
    }
    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum;
    }
    public String getImg() {
        return img;
    }
    public void setImg(String img) {
        this.img = img;
    }
    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    public String getEndDate() {
        return endDate;
    }
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    public BigDecimal getCtr() {
        return ctr;
    }
    public void setCtr(BigDecimal ctr) {
        this.ctr = ctr;
    }
    public BigDecimal getCvr() {
        return cvr;
    }
    public void setCvr(BigDecimal cvr) {
        this.cvr = cvr;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    public String getModifyTime() {
        return modifyTime;
    }
    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }
    public String getPlatform() {
        return platform;
    }
    public void setPlatform(String platform) {
        this.platform = platform;
    }

    
}
