package com.b2c.entity.erp.vo;

/**
 * 描述：
 * 待拣货商品list
 *
 * @author qlp
 * @date 2021-06-29 10:57 AM
 */
public class FaHuoDaiJianHuoGoodsVo {
    private Long skuId;//skuId
    private Long goodsId;
    private String specNumber;
    private String goodsNumber;
    private String goodsName;
    private String colorValue;
    private String colorImage;
    private String sizeValue;
    private String styleValue;
    private Integer saleQty;//销售数量
//    private Integer lockedQty;//锁定库存
    private Integer currentQty;//当前库存
    private Integer pickingQty;//
    private Integer orderItemCount;//订单item 数量
    private String orderItems;//所包含的订单items
    private Integer orderCount;//订单数量
    private String orders;//所包含的orders
    private Integer locationId;
    private String locationNumber;

    public String getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(String orderItems) {
        this.orderItems = orderItems;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getLocationNumber() {
        return locationNumber;
    }

    public void setLocationNumber(String locationNumber) {
        this.locationNumber = locationNumber;
    }

    public Integer getPickingQty() {
        return pickingQty;
    }

    public void setPickingQty(Integer pickingQty) {
        this.pickingQty = pickingQty;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getColorImage() {
        return colorImage;
    }

    public void setColorImage(String colorImage) {
        this.colorImage = colorImage;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public String getStyleValue() {
        return styleValue;
    }

    public void setStyleValue(String styleValue) {
        this.styleValue = styleValue;
    }

    public Integer getSaleQty() {
        return saleQty;
    }

    public void setSaleQty(Integer saleQty) {
        this.saleQty = saleQty;
    }

//    public Integer getLockedQty() {
//        return lockedQty;
//    }
//
//    public void setLockedQty(Integer lockedQty) {
//        this.lockedQty = lockedQty;
//    }

    public Integer getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Integer currentQty) {
        this.currentQty = currentQty;
    }

    public Integer getOrderItemCount() {
        return orderItemCount;
    }

    public void setOrderItemCount(Integer orderItemCount) {
        this.orderItemCount = orderItemCount;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }
}
