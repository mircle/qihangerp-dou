package com.b2c.entity.erp;

/**
 * 描述：
 * 商品规格Entity
 *
 * @author qlp
 * @date 2019-03-21 15:56
 */
public class GoodsSpecEntity {
    private Integer id;
    private Integer goodsId;
    private Long quantity;
    private String specName;
    private String specNumber;
    private String barCode;
    private Double purPrice;
    private Double salePrice;
    private Double unitCost;
    private Double amount;
    private Double vipPrice;
    private Double wholesalePrice;
    private Double discountRate1;
    private Double discountRate2;
    private String remark;
    private String propertys;
    private Integer status;
    private Long lowQty;
    private Long highQty;
    private Integer locationId;
    private String locationName;
    private String skuAssistId;
    private String files;
    private Integer disable;
    private String property;
    private Double safeDays;
    private Double advanceDay;
    private Integer isWarranty;
    private Integer isDelete;
    private String warehouseWarning;

    private Integer colorId;
    private String colorName;
    private Integer sizeId;
    private String sizeValue;
    private Integer styleId;
    private String styleName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public Double getPurPrice() {
        return purPrice;
    }

    public void setPurPrice(Double purPrice) {
        this.purPrice = purPrice;
    }

    public Double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    public Double getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(Double unitCost) {
        this.unitCost = unitCost;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getVipPrice() {
        return vipPrice;
    }

    public void setVipPrice(Double vipPrice) {
        this.vipPrice = vipPrice;
    }

    public Double getWholesalePrice() {
        return wholesalePrice;
    }

    public void setWholesalePrice(Double wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }

    public Double getDiscountRate1() {
        return discountRate1;
    }

    public void setDiscountRate1(Double discountRate1) {
        this.discountRate1 = discountRate1;
    }

    public Double getDiscountRate2() {
        return discountRate2;
    }

    public void setDiscountRate2(Double discountRate2) {
        this.discountRate2 = discountRate2;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPropertys() {
        return propertys;
    }

    public void setPropertys(String propertys) {
        this.propertys = propertys;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getLowQty() {
        return lowQty;
    }

    public void setLowQty(Long lowQty) {
        this.lowQty = lowQty;
    }

    public Long getHighQty() {
        return highQty;
    }

    public void setHighQty(Long highQty) {
        this.highQty = highQty;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getSkuAssistId() {
        return skuAssistId;
    }

    public void setSkuAssistId(String skuAssistId) {
        this.skuAssistId = skuAssistId;
    }

    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }

    public Integer getDisable() {
        return disable;
    }

    public void setDisable(Integer disable) {
        this.disable = disable;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Double getSafeDays() {
        return safeDays;
    }

    public void setSafeDays(Double safeDays) {
        this.safeDays = safeDays;
    }

    public Double getAdvanceDay() {
        return advanceDay;
    }

    public void setAdvanceDay(Double advanceDay) {
        this.advanceDay = advanceDay;
    }

    public Integer getIsWarranty() {
        return isWarranty;
    }

    public void setIsWarranty(Integer isWarranty) {
        this.isWarranty = isWarranty;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getWarehouseWarning() {
        return warehouseWarning;
    }

    public void setWarehouseWarning(String warehouseWarning) {
        this.warehouseWarning = warehouseWarning;
    }

    public Integer getColorId() {
        return colorId;
    }

    public void setColorId(Integer colorId) {
        this.colorId = colorId;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public Integer getSizeId() {
        return sizeId;
    }

    public void setSizeId(Integer sizeId) {
        this.sizeId = sizeId;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public Integer getStyleId() {
        return styleId;
    }

    public void setStyleId(Integer styleId) {
        this.styleId = styleId;
    }

    public String getStyleName() {
        return styleName;
    }

    public void setStyleName(String styleName) {
        this.styleName = styleName;
    }
}
