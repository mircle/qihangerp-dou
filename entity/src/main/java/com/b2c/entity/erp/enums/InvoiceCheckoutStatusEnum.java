package com.b2c.entity.erp.enums;


public enum InvoiceCheckoutStatusEnum {
    NoChecked("未检验", 0),
    HasCheckedGoods("已检验", 1),
    Abnormal("异常", 2);

    private String name;
    private int index;

    InvoiceCheckoutStatusEnum(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (InvoiceCheckoutStatusEnum c : InvoiceCheckoutStatusEnum.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
