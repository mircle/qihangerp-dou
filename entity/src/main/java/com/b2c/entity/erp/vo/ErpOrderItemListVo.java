package com.b2c.entity.erp.vo;

import com.b2c.entity.ErpOrderItemEntity;

/**
 * 描述：
 * 订单商品list vo用于商城订单与仓库系统对接显示
 *
 * @author qlp
 * @date 2019-06-21 17:57
 */
public class ErpOrderItemListVo extends ErpOrderItemEntity {
    private String goodsImg;
    private Long erpOrderId;
    private Long erpStockOutGoodsId;//仅用于更新已jianhuo
    private String specNumber;//规格编码SKU
    private String goodsName;//商品名
    private String unitName;//单位
//    private String specName;//规格名
    private String colorValue;//颜色
    private String sizeValue;//尺码
    private String styleValue;//款式
//    private Integer locationId;//默认仓库id
    private String orderNum;
    private Long orderTime;
    private Integer saleType;
    private String contactPerson;
    private Long deliveryTime;
    private String shopName;
    private String address;
    private String logisticsCompany;
    private String logisticsCode;
    private Integer orderStatus;
    private String stockInfoItemId;//库存批次ID

    public String getStockInfoItemId() {
        return stockInfoItemId;
    }

    public void setStockInfoItemId(String stockInfoItemId) {
        this.stockInfoItemId = stockInfoItemId;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Integer getSaleType() {
        return saleType;
    }

    public void setSaleType(Integer saleType) {
        this.saleType = saleType;
    }

    public Long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Long getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Long orderTime) {
        this.orderTime = orderTime;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public String getStyleValue() {
        return styleValue;
    }

    public void setStyleValue(String styleValue) {
        this.styleValue = styleValue;
    }

    public Long getErpStockOutGoodsId() {
        return erpStockOutGoodsId;
    }

    public void setErpStockOutGoodsId(Long erpStockOutGoodsId) {
        this.erpStockOutGoodsId = erpStockOutGoodsId;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
//
//    public String getSpecName() {
//        return specName;
//    }
//
//    public void setSpecName(String specName) {
//        this.specName = specName;
//    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public String getLogisticsCode() {
        return logisticsCode;
    }

    public void setLogisticsCode(String logisticsCode) {
        this.logisticsCode = logisticsCode;
    }

    public Long getErpOrderId() {
        return erpOrderId;
    }

    public void setErpOrderId(Long erpOrderId) {
        this.erpOrderId = erpOrderId;
    }
    //    public Integer getLocationId() {
//        return locationId;
//    }
//
//    public void setLocationId(Integer locationId) {
//        this.locationId = locationId;
//    }

//    public Integer getReservoirId() {
//        return reservoirId;
//    }
//
//    public void setReservoirId(Integer reservoirId) {
//        this.reservoirId = reservoirId;
//    }

//    public String getReservoirName() {
//        return reservoirName;
//    }
//
//    public void setReservoirName(String reservoirName) {
//        this.reservoirName = reservoirName;
//    }

//    public Integer getShelfId() {
//        return shelfId;
//    }
//
//    public void setShelfId(Integer shelfId) {
//        this.shelfId = shelfId;
//    }

//    public String getShelfName() {
//        return shelfName;
//    }
//
//    public void setShelfName(String shelfName) {
//        this.shelfName = shelfName;
//    }
}
