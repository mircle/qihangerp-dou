package com.b2c.entity.erp.enums;

/**
 * 描述：
 * 仓库订单来源
 *
 * @author qlp
 * @date 2019-09-17 16:20
 */
public enum ErpOrderSourceEnum {
    ALIBABA("阿里订单", "ALIBABA"),
    TAOBAO("淘宝订单", "TAOBAO"),
    TMALL("天猫订单", "TMALL"),
    YOUZAN("有赞订单", "YOUZAN"),
    YUNGOU("云购订单", "YUNGOU"),
    OFFLINE("批发系统订单", "OFFLINE"),
    DaiFa("直播机构订单", "DaiFa"),
    Sales("渠道订单", "Sales"),
    ERP("ERP订单", "ERP"),
    PDD("拼多多订单","PDD"),
    DOUYIN("抖音订单","DOUYIN"),
    KWAI("快手订单","KWAI");

    private String name;
    private String index;

    // 构造方法
    private ErpOrderSourceEnum(String name, String index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(String index) {
        for (ErpOrderSourceEnum c : ErpOrderSourceEnum.values()) {
            if (c.getIndex().equals(index)) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }


}
