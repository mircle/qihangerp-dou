package com.b2c.entity.erp.vo;

/**
 * 描述：
 * 商品搜索Vo
 *
 * @author qlp
 * @date 2019-03-22 19:19
 */
public class GoodsSearchByNumberVo {
    private Integer id;
    private String name;
    private String number;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
