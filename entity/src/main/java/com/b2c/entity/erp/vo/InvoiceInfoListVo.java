package com.b2c.entity.erp.vo;

import com.b2c.entity.ErpGoodsStockInfoEntity;

import java.util.List;

/**
 * 描述：
 * 表单商品list
 *
 * @author qlp
 * @date 2019-05-29 18:04
 */
public class InvoiceInfoListVo {
    private Long id;//主键id
    private Long iid;//invoiceId
    private Integer goodsId;//商品id
    private String goodsNumber;//商品编码
    private Integer specId;//规格sku id
    private String specNumber;//规格编码
    private String goodsName;//商品名
    private String specName;//规格名
    private String colorImage;//图片
    private String colorValue;//颜色
    private String sizeValue;//尺码
    private String styleValue;//款式
    private String unitName;//单位
    private long quantity;//数量
    private long inQuantity = 0;//已入库数量
    private long qualifiedQuantity;//合格数量
    private long abnormalQuantity;//异常数量
    private Integer locationId;//默认仓库id
    private String locationName;//仓库名
    private Double price;//购货单价
    private Double amount;//购货金额
    private Integer status;//状态
    private String billNo;
    private String billDate;
    private String billStatus;
    private String contactName;
    private String description;
    

    private List<ErpGoodsStockInfoEntity> stocks;

//    private Integer reservoirId; //库区
//    private String reservoirName;
//    private Integer shelfId;        //货架
//    private String shelfName;

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName;
    }

    public String getColorImage() {
        return colorImage;
    }

    public void setColorImage(String colorImage) {
        this.colorImage = colorImage;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public void setInQuantity(long inQuantity) {
        this.inQuantity = inQuantity;
    }

    public void setQualifiedQuantity(long qualifiedQuantity) {
        this.qualifiedQuantity = qualifiedQuantity;
    }

    public void setAbnormalQuantity(long abnormalQuantity) {
        this.abnormalQuantity = abnormalQuantity;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public String getStyleValue() {
        return styleValue;
    }

    public void setStyleValue(String styleValue) {
        this.styleValue = styleValue;
    }

    public Long getQualifiedQuantity() {
        return qualifiedQuantity;
    }

    public void setQualifiedQuantity(Long qualifiedQuantity) {
        this.qualifiedQuantity = qualifiedQuantity;
    }

    public Long getAbnormalQuantity() {
        return abnormalQuantity;
    }

    public void setAbnormalQuantity(Long abnormalQuantity) {
        this.abnormalQuantity = abnormalQuantity;
    }

//    public Integer getReservoirId() {
//        return reservoirId;
//    }
//
//    public void setReservoirId(Integer reservoirId) {
//        this.reservoirId = reservoirId;
//    }
//
//    public String getReservoirName() {
//        return reservoirName;
//    }
//
//    public void setReservoirName(String reservoirName) {
//        this.reservoirName = reservoirName;
//    }
//
//    public Integer getShelfId() {
//        return shelfId;
//    }
//
//    public void setShelfId(Integer shelfId) {
//        this.shelfId = shelfId;
//    }
//
//    public String getShelfName() {
//        return shelfName;
//    }
//
//    public void setShelfName(String shelfName) {
//        this.shelfName = shelfName;
//    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Long getIid() {
        return iid;
    }

    public void setIid(Long iid) {
        this.iid = iid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public Long getInQuantity() {
        return inQuantity;
    }

    public void setInQuantity(Long inQuantity) {
        this.inQuantity = inQuantity;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

//    public String getSpecName() {
//        return specName;
//    }
//
//    public void setSpecName(String specName) {
//        this.specName = specName;
//    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public List<ErpGoodsStockInfoEntity> getStocks() {
        return stocks;
    }

    public void setStocks(List<ErpGoodsStockInfoEntity> stocks) {
        this.stocks = stocks;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
