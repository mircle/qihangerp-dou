package com.b2c.entity.erp;

public class ErpPullOrderLogEntity {
    private Long id;
    private Integer shopId;//店铺id（dc_shop）
    private String shopName;//店铺名
    private Long startTime;//开始时间
    private Long endTime;//结束时间
    private Integer addCount;//添加数量
    private Integer updCount;//更新数量
    private Integer failCount;//失败数量
    private Long createOn;//创建时间

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Integer getAddCount() {
        return addCount;
    }

    public void setAddCount(Integer addCount) {
        this.addCount = addCount;
    }

    public Integer getUpdCount() {
        return updCount;
    }

    public void setUpdCount(Integer updCount) {
        this.updCount = updCount;
    }

    public Integer getFailCount() {
        return failCount;
    }

    public void setFailCount(Integer failCount) {
        this.failCount = failCount;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Long getCreateOn() {
        return createOn;
    }

    public void setCreateOn(Long createOn) {
        this.createOn = createOn;
    }
}
