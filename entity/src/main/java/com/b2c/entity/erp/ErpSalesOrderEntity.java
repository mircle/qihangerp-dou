package com.b2c.entity.erp;

import java.math.BigDecimal;

/**
 * 销售外发订单Entity
 */
public class ErpSalesOrderEntity {

    private Long id;//` BIGINT(20) NOT NULL AUTO_INCREMENT,
    private String orderNum;//` VARCHAR(30) NOT NULL COMMENT '订单编号',
    private Integer buyerUserId;//` INT(11) NOT NULL COMMENT '买家用户id',
    private String buyerName;//` VARCHAR(20) NULL DEFAULT NULL COMMENT '买家名',
    private Long goodsCount;//` INT(11) NOT NULL COMMENT '商品数量（累积总数）',
    private Long goodsSpecCount;//` INT(11) NOT NULL COMMENT '商品SKU件数（按规格计件数）',
    private Long refundQuantity;//退货总数
    private BigDecimal goodsTotalAmount;//` DECIMAL(11,2) NOT NULL COMMENT '商品总金额',
    private BigDecimal shippingFee;//` DECIMAL(11,2) NOT NULL COMMENT '运费',
    private BigDecimal discountAmount;//` DECIMAL(11,2) NOT NULL COMMENT '优惠金额',
    private BigDecimal totalAmount;//` DECIMAL(10,2) NOT NULL COMMENT '订单总价（goodsTotalAmount+shippingFee-discountAmount）',
    private String buyerFeedback;//` VARCHAR(100) NULL DEFAULT NULL COMMENT '订单备注（买家）',
    private String sellerMemo;//` VARCHAR(100) NULL DEFAULT NULL COMMENT '卖家备注',
    private String contactPerson;//` VARCHAR(15) NULL DEFAULT NULL COMMENT '收货人姓名',
    private String contactMobile;//` VARCHAR(11) NULL DEFAULT NULL COMMENT '收货人手机号',
    private String province;//` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '省',
    private String city;//` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '市',
    private String area;//` VARCHAR(30) NULL DEFAULT NULL COMMENT '区',
    private String address;//` VARCHAR(145) NOT NULL COMMENT '收获详细地址，包括省市区',
    private Integer saleType;//` INT(11) NULL DEFAULT '0' COMMENT '订单类型,0:样品;1:实售',
    private Integer status;//` INT(11) NOT NULL COMMENT '订单状态0:待提交,1:待审核,2:待发货3:已发货,4:已收货,5:已完成  EnumErpOrderStatus',
    private Long createOn;//` BIGINT(20) NOT NULL COMMENT '创建时间',
    private String createBy;//` VARCHAR(15) NULL DEFAULT NULL,
    private Long modifyOn;//` BIGINT(20) NULL DEFAULT NULL,
    private String modifyBy;//` VARCHAR(15) NULL DEFAULT NULL,
    private Long deliveredTime;//` BIGINT(20) NULL DEFAULT NULL COMMENT '发货时间',
    private Integer deliveredStatus;//` INT(11) NOT NULL DEFAULT '0' COMMENT '发货状态（0待出库1拣货中2已拣货3已出库4已发货）',
    private String logisticsCompany;//` VARCHAR(20) NULL DEFAULT NULL COMMENT '发货物流公司',
    private String logisticsCompanyCode;//` VARCHAR(15) NULL DEFAULT NULL COMMENT '发货物流公司代码',
    private String logisticsCode;//` VARCHAR(30) NULL DEFAULT NULL COMMENT '发货物流单号',
    private Long receivingTime;//` BIGINT(20) NULL DEFAULT NULL COMMENT '收货时间',
    private Integer auditStatus;//` INT(11) NULL DEFAULT '0' COMMENT '订单审核状态（0待审核1已审核）OMS确认订单',
    private Integer settleState;//` INT(11) NOT NULL DEFAULT '0' COMMENT '结算状态（0：未结清1：已结清）',
    private Integer developerId;//` INT(11) NOT NULL DEFAULT '0' COMMENT '业务员（开发者）id',
    private String developerName;
    private String source;//订单来源
    private Long orderTime;//下单时间，第三方平台下单时间
    private Integer shopId;
    private BigDecimal payAmount;
    private String shopName;
    private Integer payMethod;//支付方式
    private Integer payStatus;//付款状态
    private Long payTime;//付款时间
    private String orderDate;//订单日期

    /**
     * 手机号
     */
    private String buyerMobile;

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Long getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Long orderTime) {
        this.orderTime = orderTime;
    }

    public Long getRefundQuantity() {
        return refundQuantity;
    }

    public void setRefundQuantity(Long refundQuantity) {
        this.refundQuantity = refundQuantity;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDeveloperName() {
        return developerName;
    }

    public void setDeveloperName(String developerName) {
        this.developerName = developerName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getBuyerUserId() {
        return buyerUserId;
    }

    public void setBuyerUserId(Integer buyerUserId) {
        this.buyerUserId = buyerUserId;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public Long getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(Long goodsCount) {
        this.goodsCount = goodsCount;
    }

    public Long getGoodsSpecCount() {
        return goodsSpecCount;
    }

    public void setGoodsSpecCount(Long goodsSpecCount) {
        this.goodsSpecCount = goodsSpecCount;
    }

    public BigDecimal getGoodsTotalAmount() {
        return goodsTotalAmount;
    }

    public void setGoodsTotalAmount(BigDecimal goodsTotalAmount) {
        this.goodsTotalAmount = goodsTotalAmount;
    }

    public BigDecimal getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(BigDecimal shippingFee) {
        this.shippingFee = shippingFee;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getBuyerFeedback() {
        return buyerFeedback;
    }

    public void setBuyerFeedback(String buyerFeedback) {
        this.buyerFeedback = buyerFeedback;
    }

    public String getSellerMemo() {
        return sellerMemo;
    }

    public void setSellerMemo(String sellerMemo) {
        this.sellerMemo = sellerMemo;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactMobile() {
        return contactMobile;
    }

    public void setContactMobile(String contactMobile) {
        this.contactMobile = contactMobile;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getSaleType() {
        return saleType;
    }

    public void setSaleType(Integer saleType) {
        this.saleType = saleType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getCreateOn() {
        return createOn;
    }

    public void setCreateOn(Long createOn) {
        this.createOn = createOn;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Long getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(Long modifyOn) {
        this.modifyOn = modifyOn;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Long getDeliveredTime() {
        return deliveredTime;
    }

    public void setDeliveredTime(Long deliveredTime) {
        this.deliveredTime = deliveredTime;
    }

    public Integer getDeliveredStatus() {
        return deliveredStatus;
    }

    public void setDeliveredStatus(Integer deliveredStatus) {
        this.deliveredStatus = deliveredStatus;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public String getLogisticsCompanyCode() {
        return logisticsCompanyCode;
    }

    public void setLogisticsCompanyCode(String logisticsCompanyCode) {
        this.logisticsCompanyCode = logisticsCompanyCode;
    }

    public String getLogisticsCode() {
        return logisticsCode;
    }

    public void setLogisticsCode(String logisticsCode) {
        this.logisticsCode = logisticsCode;
    }

    public Long getReceivingTime() {
        return receivingTime;
    }

    public void setReceivingTime(Long receivingTime) {
        this.receivingTime = receivingTime;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Integer getSettleState() {
        return settleState;
    }

    public void setSettleState(Integer settleState) {
        this.settleState = settleState;
    }

    public Integer getDeveloperId() {
        return developerId;
    }

    public void setDeveloperId(Integer developerId) {
        this.developerId = developerId;
    }

    public String getBuyerMobile() {
        return buyerMobile;
    }

    public void setBuyerMobile(String buyerMobile) {
        this.buyerMobile = buyerMobile;
    }

    public Integer getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(Integer payMethod) {
        this.payMethod = payMethod;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Long getPayTime() {
        return payTime;
    }

    public void setPayTime(Long payTime) {
        this.payTime = payTime;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

}
