package com.b2c.entity;

/**
 * 库存盘点单
 */
public class ErpStocktakingInvoice {
    private Long id;//` BIGINT(20) NOT NULL AUTO_INCREMENT,
	private String billNo;//` VARCHAR(30) NULL DEFAULT '' COMMENT '单据编号',
    private String billDate;//` DATE NULL DEFAULT NULL COMMENT '单据日期',
    private Integer userId;//` SMALLINT(6) NULL DEFAULT '0' COMMENT '制单人id',
    private String userName;//` VARCHAR(50) NULL DEFAULT '' COMMENT '制单人',
    private Integer countedUserId;
    private String countedUserName;
    private Long totalSku;//` BIGINT(20) NULL DEFAULT '0' COMMENT '总SKU数',
    private Long total;//盘点仓位总数
    private String description;//` VARCHAR(255) NULL DEFAULT NULL COMMENT '备注',
    private Integer isDelete;//` TINYINT(1) NULL DEFAULT '0' COMMENT '1删除  0正常',
    private Long createTime;//` BIGINT(20) NULL DEFAULT NULL COMMENT '创建时间',
    private Long modifyTime;//` BIGINT(20) NULL DEFAULT NULL COMMENT '更新时间',
    private Long locationId;//` INT(11) NOT NULL COMMENT '仓库id',
    private String locationName;//` 仓库',
    private Integer status;//` INT(11) NULL DEFAULT '0' COMMENT '状态 0 未审核 1待盘点录入2已录入3已完成4已取消',

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCountedUserId() {
        return countedUserId;
    }

    public void setCountedUserId(Integer countedUserId) {
        this.countedUserId = countedUserId;
    }

    public String getCountedUserName() {
        return countedUserName;
    }

    public void setCountedUserName(String countedUserName) {
        this.countedUserName = countedUserName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getTotalSku() {
        return totalSku;
    }

    public void setTotalSku(Long totalSku) {
        this.totalSku = totalSku;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Long modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
