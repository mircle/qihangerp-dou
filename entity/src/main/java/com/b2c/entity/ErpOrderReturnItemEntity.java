package com.b2c.entity;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-15 15:04
 */
public class ErpOrderReturnItemEntity {
    private Long id;//` bigint(20) NOT NULL AUTO_INCREMENT,
    private Long orderId;//` bigint(20) NOT NULL COMMENT 'order_return_id',
    private Long orderItemId;//erp_order_item表id
    private Integer goodsId;//` int(11) NOT NULL COMMENT '商品id',
    private Integer skuId;//` int(11) NOT NULL COMMENT '商品规格id',
    private String skuNumber;//` varchar(25) DEFAULT NULL COMMENT '商品规格编码',
    private Long quantity;//` bigint(20) NOT NULL COMMENT '数量',
    private Long inQuantity;        //入库数量
    private Long badQuantity;       //不良品数量
    private Integer receiveType;//` int(11) NOT NULL COMMENT '收货处理方式1入库2报损   0代表没处理',
    private Long createTime;//` bigint(20) NOT NULL COMMENT '创建时间',
    private Long modifyTime;//` bigint(20) DEFAULT NULL COMMENT '修改时间',
    private Long receiveTime;//` bigint(20) DEFAULT NULL COMMENT '收货处理时间',
    private String remark;//` varchar(50) DEFAULT '' COMMENT '备注',
    private Integer status;//` int(11) NOT NULL COMMENT '状态（0未处理1已处理）',

    private Long stockInfoItemId;

    
    public Long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Long orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Long getInQuantity() {
        return inQuantity;
    }

    public void setInQuantity(Long inQuantity) {
        this.inQuantity = inQuantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }

    public String getSkuNumber() {
        return skuNumber;
    }

    public void setSkuNumber(String skuNumber) {
        this.skuNumber = skuNumber;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Integer getReceiveType() {
        return receiveType;
    }

    public void setReceiveType(Integer receiveType) {
        this.receiveType = receiveType;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Long modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Long getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(Long receiveTime) {
        this.receiveTime = receiveTime;
    }

    public Long getBadQuantity() {
        return badQuantity;
    }

    public void setBadQuantity(Long badQuantity) {
        this.badQuantity = badQuantity;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getStockInfoItemId() {
        return stockInfoItemId;
    }

    public void setStockInfoItemId(Long stockInfoItemId) {
        this.stockInfoItemId = stockInfoItemId;
    }
}
