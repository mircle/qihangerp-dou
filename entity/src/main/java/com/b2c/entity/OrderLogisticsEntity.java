package com.b2c.entity;

/**
 * @Description:物流信息 pbd add 2019/2/13 9:24
 */
public class OrderLogisticsEntity {
    /**
     * id
     */
    private int id;
    /**
     * 物流信息
     */
    private String comment;
    /**
     * 订单id
     */
    private int orderId;
    /**
     * 创建时间
     */
    private String createOn;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 物流状态
     */
    private String state;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getCreateOn() {
        return createOn;
    }

    public void setCreateOn(String createOn) {
        this.createOn = createOn;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
