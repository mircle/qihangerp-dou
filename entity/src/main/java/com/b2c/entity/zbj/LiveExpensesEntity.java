package com.b2c.entity.zbj;

public class LiveExpensesEntity {
    private Long id;
    private Long liveId;
    private Double expenses1 = 0.0;
    private Double expenses2=0.0;
    private Double expenses3=0.0;
    private Double expenses4=0.0;
    private Double expenses5=0.0;
    private Double expensesTotal=0.0;
    private String createTime;
    private String modifyTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLiveId() {
        return liveId;
    }

    public void setLiveId(Long liveId) {
        this.liveId = liveId;
    }

    public Double getExpenses1() {
        return expenses1;
    }

    public void setExpenses1(Double expenses1) {
        this.expenses1 = expenses1;
    }

    public Double getExpenses2() {
        return expenses2;
    }

    public void setExpenses2(Double expenses2) {
        this.expenses2 = expenses2;
    }

    public Double getExpenses3() {
        return expenses3;
    }

    public void setExpenses3(Double expenses3) {
        this.expenses3 = expenses3;
    }

    public Double getExpenses4() {
        return expenses4;
    }

    public void setExpenses4(Double expenses4) {
        this.expenses4 = expenses4;
    }

    public Double getExpenses5() {
        return expenses5;
    }

    public void setExpenses5(Double expenses5) {
        this.expenses5 = expenses5;
    }

    public Double getExpensesTotal() {
        return expensesTotal;
    }

    public void setExpensesTotal(Double expensesTotal) {
        this.expensesTotal = expensesTotal;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }
}
