package com.b2c.entity.zbj;

public class LiveDataEntity {
    private Long id;
    private String authorImg;
    private String authorName;
    private String authorAccount;
    private String liveStartTime;
    private String liveEndTime;
    private String liveDate;//直播日期
    private Float liveTime;//直播时长
    private Long showUV;//直播曝光人数
    private Long showPV;//直播曝光次数
    private Long UV;//累计观看人数
    private Long PV;//累计观看人次
    private Long PCU;//最高在线人数
    private Long ACU;//平均在线人数
    private Float TS;//人均观看时长(分钟)
    private Long comments;//评论次数
    private Long xjt;//新加团人数
    private Long xzfs;//新增粉丝
    private Long qgfs;//取关粉丝
    private Double kbfszb;//看播粉丝占比(老粉)
    private Long goodsCount;//带货商品数量
    private Long goodsShow;//直播间商品曝光人数
    private Long goodsClick;//直播间商品点击人数
    private Long goodsShowTotal;//直播间商品曝光次数
    private Long goodsClickTotal;//直播间商品点击次数
    private Long orderCount;//直播间成交订单数
    private Double orderAmount;//直播间成交金额
    private Long orderGoodsCount;//直播间成交件数
    private Long orderUser;//直播间成交人数
    private Long orderRefundCount;//直播间订单退款数
    private Double orderRefundAmount;//直播间订单退款金额
    private Long orderRefundUser;//直播间订单退款人数
    private Double commission;//预估佣金支出
    private Double goodsClickRate;//商品点击率(次数)
    private Double goodsClickRate2;//商品点击率(人数)
    private Double goodsClickTransactionRate;//点击成交转化率(次数)
    private Double goodsClickTransactionRate2;//点击成交转化率(人数)
    private Double viewTransactionRate;//看播成交转化率(次数)
    private Double viewTransactionRate2;//看播成交转化率(人数)
    private Long preOrderCount;//预售订单数
    private Double preOrderAmount;//预售创建金额
    private String createTime;
    private String modifyTime;
    private Double expensesTotal;
    private String repalyFilePath;//复盘文件地址

    public String getRepalyFilePath() {
        return repalyFilePath;
    }

    public void setRepalyFilePath(String repalyFilePath) {
        this.repalyFilePath = repalyFilePath;
    }

    public Double getExpensesTotal() {
        return expensesTotal;
    }

    public void setExpensesTotal(Double expensesTotal) {
        this.expensesTotal = expensesTotal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLiveDate() {
        return liveDate;
    }

    public void setLiveDate(String liveDate) {
        this.liveDate = liveDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getAuthorImg() {
        return authorImg;
    }

    public void setAuthorImg(String authorImg) {
        this.authorImg = authorImg;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorAccount() {
        return authorAccount;
    }

    public void setAuthorAccount(String authorAccount) {
        this.authorAccount = authorAccount;
    }

    public String getLiveStartTime() {
        return liveStartTime;
    }

    public void setLiveStartTime(String liveStartTime) {
        this.liveStartTime = liveStartTime;
    }

    public String getLiveEndTime() {
        return liveEndTime;
    }

    public void setLiveEndTime(String liveEndTime) {
        this.liveEndTime = liveEndTime;
    }

    public Float getLiveTime() {
        return liveTime;
    }

    public void setLiveTime(Float liveTime) {
        this.liveTime = liveTime;
    }

    public Long getShowUV() {
        return showUV;
    }

    public void setShowUV(Long showUV) {
        this.showUV = showUV;
    }

    public Long getShowPV() {
        return showPV;
    }

    public void setShowPV(Long showPV) {
        this.showPV = showPV;
    }

    public Long getUV() {
        return UV;
    }

    public void setUV(Long UV) {
        this.UV = UV;
    }

    public Long getPV() {
        return PV;
    }

    public void setPV(Long PV) {
        this.PV = PV;
    }

    public Long getPCU() {
        return PCU;
    }

    public void setPCU(Long PCU) {
        this.PCU = PCU;
    }

    public Long getACU() {
        return ACU;
    }

    public void setACU(Long ACU) {
        this.ACU = ACU;
    }

    public Float getTS() {
        return TS;
    }

    public void setTS(Float TS) {
        this.TS = TS;
    }

    public Long getComments() {
        return comments;
    }

    public void setComments(Long comments) {
        this.comments = comments;
    }

    public Long getXjt() {
        return xjt;
    }

    public void setXjt(Long xjt) {
        this.xjt = xjt;
    }

    public Long getXzfs() {
        return xzfs;
    }

    public void setXzfs(Long xzfs) {
        this.xzfs = xzfs;
    }

    public Long getQgfs() {
        return qgfs;
    }

    public void setQgfs(Long qgfs) {
        this.qgfs = qgfs;
    }

    public Double getKbfszb() {
        return kbfszb;
    }

    public void setKbfszb(Double kbfszb) {
        this.kbfszb = kbfszb;
    }

    public Long getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(Long goodsCount) {
        this.goodsCount = goodsCount;
    }

    public Long getGoodsShow() {
        return goodsShow;
    }

    public void setGoodsShow(Long goodsShow) {
        this.goodsShow = goodsShow;
    }

    public Long getGoodsClick() {
        return goodsClick;
    }

    public void setGoodsClick(Long goodsClick) {
        this.goodsClick = goodsClick;
    }

    public Long getGoodsShowTotal() {
        return goodsShowTotal;
    }

    public void setGoodsShowTotal(Long goodsShowTotal) {
        this.goodsShowTotal = goodsShowTotal;
    }

    public Long getGoodsClickTotal() {
        return goodsClickTotal;
    }

    public void setGoodsClickTotal(Long goodsClickTotal) {
        this.goodsClickTotal = goodsClickTotal;
    }

    public Long getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Long orderCount) {
        this.orderCount = orderCount;
    }

    public Double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(Double orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Long getOrderGoodsCount() {
        return orderGoodsCount;
    }

    public void setOrderGoodsCount(Long orderGoodsCount) {
        this.orderGoodsCount = orderGoodsCount;
    }

    public Long getOrderUser() {
        return orderUser;
    }

    public void setOrderUser(Long orderUser) {
        this.orderUser = orderUser;
    }

    public Long getOrderRefundCount() {
        return orderRefundCount;
    }

    public void setOrderRefundCount(Long orderRefundCount) {
        this.orderRefundCount = orderRefundCount;
    }

    public Double getOrderRefundAmount() {
        return orderRefundAmount;
    }

    public void setOrderRefundAmount(Double orderRefundAmount) {
        this.orderRefundAmount = orderRefundAmount;
    }

    public Long getOrderRefundUser() {
        return orderRefundUser;
    }

    public void setOrderRefundUser(Long orderRefundUser) {
        this.orderRefundUser = orderRefundUser;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public Double getGoodsClickRate() {
        return goodsClickRate;
    }

    public void setGoodsClickRate(Double goodsClickRate) {
        this.goodsClickRate = goodsClickRate;
    }

    public Double getGoodsClickRate2() {
        return goodsClickRate2;
    }

    public void setGoodsClickRate2(Double goodsClickRate2) {
        this.goodsClickRate2 = goodsClickRate2;
    }

    public Double getGoodsClickTransactionRate() {
        return goodsClickTransactionRate;
    }

    public void setGoodsClickTransactionRate(Double goodsClickTransactionRate) {
        this.goodsClickTransactionRate = goodsClickTransactionRate;
    }

    public Double getGoodsClickTransactionRate2() {
        return goodsClickTransactionRate2;
    }

    public void setGoodsClickTransactionRate2(Double goodsClickTransactionRate2) {
        this.goodsClickTransactionRate2 = goodsClickTransactionRate2;
    }

    public Double getViewTransactionRate() {
        return viewTransactionRate;
    }

    public void setViewTransactionRate(Double viewTransactionRate) {
        this.viewTransactionRate = viewTransactionRate;
    }

    public Double getViewTransactionRate2() {
        return viewTransactionRate2;
    }

    public void setViewTransactionRate2(Double viewTransactionRate2) {
        this.viewTransactionRate2 = viewTransactionRate2;
    }

    public Long getPreOrderCount() {
        return preOrderCount;
    }

    public void setPreOrderCount(Long preOrderCount) {
        this.preOrderCount = preOrderCount;
    }

    public Double getPreOrderAmount() {
        return preOrderAmount;
    }

    public void setPreOrderAmount(Double preOrderAmount) {
        this.preOrderAmount = preOrderAmount;
    }
}
