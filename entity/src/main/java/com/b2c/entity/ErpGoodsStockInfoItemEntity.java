package com.b2c.entity;

import java.math.BigDecimal;

/**
 * 描述：库存批次entity
 *
 * @author qlp
 * @date 2021-07-21 14:01
 */
public class ErpGoodsStockInfoItemEntity {
    private Long id;
    private String specNumber;//SKU编码
    private Long stockInfoId;//库存信息id
    private Long currentQty;//当前库存数量
    private Long inQty;//当前库存数量
    private BigDecimal purPrice;//价格
    private Long invoiceId;//采购单ID
    private Long invoiceInfoId;//采购单itemId
    private Integer locationId;//仓位ID
    private String locationNumber;//仓位number
    private String createOn;//入库时间
    private Long stockInFormId;//入库单ID

    public Long getInQty() {
        return inQty;
    }

    public void setInQty(Long inQty) {
        this.inQty = inQty;
    }

    public Long getStockInFormId() {
        return stockInFormId;
    }
    public void setStockInFormId(Long stockInFormId) {
        this.stockInFormId = stockInFormId;
    }
    public String getCreateOn() {
        return createOn;
    }
    public void setCreateOn(String createOn) {
        this.createOn = createOn;
    }
    public String getLocationNumber() {
        return locationNumber;
    }
    public void setLocationNumber(String locationNumber) {
        this.locationNumber = locationNumber;
    }
    public String getSpecNumber() {
        return specNumber;
    }
    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }
    public Integer getLocationId() {
        return locationId;
    }
    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getStockInfoId() {
        return stockInfoId;
    }
    public void setStockInfoId(Long stockInfoId) {
        this.stockInfoId = stockInfoId;
    }
    public Long getCurrentQty() {
        return currentQty;
    }
    public void setCurrentQty(Long currentQty) {
        this.currentQty = currentQty;
    }
    public BigDecimal getPurPrice() {
        return purPrice;
    }
    public void setPurPrice(BigDecimal purPrice) {
        this.purPrice = purPrice;
    }
    public Long getInvoiceId() {
        return invoiceId;
    }
    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }
    public Long getInvoiceInfoId() {
        return invoiceInfoId;
    }
    public void setInvoiceInfoId(Long invoiceInfoId) {
        this.invoiceInfoId = invoiceInfoId;
    }

    

    
}
