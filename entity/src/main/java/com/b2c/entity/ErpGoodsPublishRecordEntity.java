package com.b2c.entity;

public class ErpGoodsPublishRecordEntity {
    private Integer id;
    private Integer erpGoodsId;
    private String goodsName;
    private String goodsNum;
    private String goodsImage;
    private String imagePath;
    private String sourceUrl;
    private String shopIds;
    private String publishDate;
    private Float goodsCost;
    
    private Integer goodsSupplierId;

    public Integer getGoodsSupplierId() {
        return goodsSupplierId;
    }

    public void setGoodsSupplierId(Integer goodsSupplierId) {
        this.goodsSupplierId = goodsSupplierId;
    }

    public String getGoodsName() {
        return goodsName;
    }
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    public String getGoodsNum() {
        return goodsNum;
    }
    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum;
    }
    public String getGoodsImage() {
        return goodsImage;
    }
    public void setGoodsImage(String goodsImage) {
        this.goodsImage = goodsImage;
    }
    public String getSourceUrl() {
        return sourceUrl;
    }
    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }
    public String getShopIds() {
        return shopIds;
    }
    public void setShopIds(String shopIds) {
        this.shopIds = shopIds;
    }
    public String getPublishDate() {
        return publishDate;
    }
    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getErpGoodsId() {
        return erpGoodsId;
    }
    public void setErpGoodsId(Integer erpGoodsId) {
        this.erpGoodsId = erpGoodsId;
    }
    
    public String getImagePath() {
        return imagePath;
    }
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
    public Float getGoodsCost() {
        return goodsCost;
    }
    public void setGoodsCost(Float goodsCost) {
        this.goodsCost = goodsCost;
    }

    
}
