package com.b2c.entity;

import java.sql.Timestamp;

/**
 * @Description: pbd add 2019/9/28 11:01
 */
public class WmsManagePermissionEntity {
    /**
     * id
     */
    private Integer id;

    /**
     * name
     */
    private String name;

    /**
     * url
     */
    private String url;

    /**
     * 权限标识
     */
    private String permissionKey;

    /**
     * 是否是菜单：是、否
     */
    private String isMenu;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 上级id
     */
    private Integer parentId;
    private String parentKey;

    /**
     * 状态：0正常、1禁用
     */
    private Integer status;

    /**
     * 添加时间
     */
    private Timestamp addTime;

    private String icon;
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentKey() {
        return parentKey;
    }

    public void setParentKey(String parentKey) {
        this.parentKey = parentKey;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPermissionKey() {
        return permissionKey;
    }

    public void setPermissionKey(String permissionKey) {
        this.permissionKey = permissionKey;
    }

    public String getIsMenu() {
        return isMenu;
    }

    public void setIsMenu(String isMenu) {
        this.isMenu = isMenu;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Timestamp getAddTime() {
        return addTime;
    }

    public void setAddTime(Timestamp addTime) {
        this.addTime = addTime;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
