package com.b2c.entity;

public class ErpGoodsSpecMoveFromEntiy {
    private Integer id;
    //规格编码id,(erp_goods_spec.id)
    private  Integer specId;

    //单据编号
    private  String moveNo;

    //移动数量
    private  Integer quantity;

    //创建日期
    private  Long createTime;

    //创建人
    private  String createBy;


    private  Integer oldLocationId;


    private  Integer oldReservoirId;


    private  Integer oldShelfId;


    private  Integer newLocationId;


    private  Integer newReservoirId;


    private  Integer newShelfId;

    //备注
    private  String remarks;


    private String dataTime;

    private String old_locationName;
    private String old_reservoirName;
    private String old_shelfName;

    private String newlocationName;
    private String newReservoirName;
    private String newShelfName;

    private Integer goodsId;
    private String goodsName;
    private String specName;
    private String specNumber;

    private String reservoirName;
    private String shelfName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getMoveNo() {
        return moveNo;
    }

    public void setMoveNo(String moveNo) {
        this.moveNo = moveNo;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Integer getOldLocationId() {
        return oldLocationId;
    }

    public void setOldLocationId(Integer oldLocationId) {
        this.oldLocationId = oldLocationId;
    }

    public Integer getOldReservoirId() {
        return oldReservoirId;
    }

    public void setOldReservoirId(Integer oldReservoirId) {
        this.oldReservoirId = oldReservoirId;
    }

    public Integer getOldShelfId() {
        return oldShelfId;
    }

    public void setOldShelfId(Integer oldShelfId) {
        this.oldShelfId = oldShelfId;
    }

    public Integer getNewLocationId() {
        return newLocationId;
    }

    public void setNewLocationId(Integer newLocationId) {
        this.newLocationId = newLocationId;
    }

    public Integer getNewReservoirId() {
        return newReservoirId;
    }

    public void setNewReservoirId(Integer newReservoirId) {
        this.newReservoirId = newReservoirId;
    }

    public Integer getNewShelfId() {
        return newShelfId;
    }

    public void setNewShelfId(Integer newShelfId) {
        this.newShelfId = newShelfId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDataTime() {
        return dataTime;
    }

    public void setDataTime(String dataTime) {
        this.dataTime = dataTime;
    }

    public String getOld_locationName() {
        return old_locationName;
    }

    public void setOld_locationName(String old_locationName) {
        this.old_locationName = old_locationName;
    }

    public String getOld_reservoirName() {
        return old_reservoirName;
    }

    public void setOld_reservoirName(String old_reservoirName) {
        this.old_reservoirName = old_reservoirName;
    }

    public String getOld_shelfName() {
        return old_shelfName;
    }

    public void setOld_shelfName(String old_shelfName) {
        this.old_shelfName = old_shelfName;
    }

    public String getNewlocationName() {
        return newlocationName;
    }

    public void setNewlocationName(String newlocationName) {
        this.newlocationName = newlocationName;
    }

    public String getNewReservoirName() {
        return newReservoirName;
    }

    public void setNewReservoirName(String newReservoirName) {
        this.newReservoirName = newReservoirName;
    }

    public String getNewShelfName() {
        return newShelfName;
    }

    public void setNewShelfName(String newShelfName) {
        this.newShelfName = newShelfName;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public String getReservoirName() {
        return reservoirName;
    }

    public void setReservoirName(String reservoirName) {
        this.reservoirName = reservoirName;
    }

    public String getShelfName() {
        return shelfName;
    }

    public void setShelfName(String shelfName) {
        this.shelfName = shelfName;
    }
}
