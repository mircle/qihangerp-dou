package com.b2c.entity.douyin;

public class DouyinOrdersRefundEntity {

        private long id;
        //售后Id
        private Long aftersaleId;
        //售后类型 ,0 售后退货退款1 售后退款2 售前退款3 换货
        private Integer aftersaleType;
        private String orderId;
        private String subOrderId;
        private Integer shopId;
        private String comboId;
        private String productId;
        private String productPic;
        private String productName;
        private String specDesc;
        private String specCode;
        private String goodsNumber;
        private int comboNum;
        private String logisticsCompany;
        private String logisticsCode;
        private String logisticsTime;
        private String receiptTime;
        private String cancelReason;
    
        private double orderAmount;
        private double comboAmount;
    
        private int erpGoodsSpecId;
        private int erpGoodsId;
        
        
        private String questionDesc;
        private String applyTime;
        //是否确认0:否1：是
        private Integer auditStatus;
        
        //退货售后状态，枚举为6(待商家同意),7(待买家退货),11(待商家二次同意),12(售后成功),13(换货待买家收货),14(换货成功),27(商家一次拒绝),28(售后失败),29(商家二次拒绝)
        private Integer refundStatus;
        private String createOn;
        private String modifyOn;
        private String remark;



        
        public String getComboId() {
            return comboId;
        }
        public void setComboId(String comboId) {
            this.comboId = comboId;
        }
        public String getProductId() {
            return productId;
        }
        public void setProductId(String productId) {
            this.productId = productId;
        }
        public long getId() {
            return id;
        }
        public void setId(long id) {
            this.id = id;
        }
        public Long getAftersaleId() {
            return aftersaleId;
        }
        public void setAftersaleId(Long aftersaleId) {
            this.aftersaleId = aftersaleId;
        }
        public Integer getAftersaleType() {
            return aftersaleType;
        }
        public void setAftersaleType(Integer aftersaleType) {
            this.aftersaleType = aftersaleType;
        }
        public String getOrderId() {
            return orderId;
        }
        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }
        public String getSubOrderId() {
            return subOrderId;
        }
        public void setSubOrderId(String subOrderId) {
            this.subOrderId = subOrderId;
        }
        public Integer getShopId() {
            return shopId;
        }
        public void setShopId(Integer shopId) {
            this.shopId = shopId;
        }
        public String getProductPic() {
            return productPic;
        }
        public void setProductPic(String productPic) {
            this.productPic = productPic;
        }
        public String getProductName() {
            return productName;
        }
        public void setProductName(String productName) {
            this.productName = productName;
        }
        public String getSpecDesc() {
            return specDesc;
        }
        public void setSpecDesc(String specDesc) {
            this.specDesc = specDesc;
        }
        public String getSpecCode() {
            return specCode;
        }
        public void setSpecCode(String specCode) {
            this.specCode = specCode;
        }
        public String getGoodsNumber() {
            return goodsNumber;
        }
        public void setGoodsNumber(String goodsNumber) {
            this.goodsNumber = goodsNumber;
        }
        public int getComboNum() {
            return comboNum;
        }
        public void setComboNum(int comboNum) {
            this.comboNum = comboNum;
        }
        public String getLogisticsCompany() {
            return logisticsCompany;
        }
        public void setLogisticsCompany(String logisticsCompany) {
            this.logisticsCompany = logisticsCompany;
        }
        public String getLogisticsCode() {
            return logisticsCode;
        }
        public void setLogisticsCode(String logisticsCode) {
            this.logisticsCode = logisticsCode;
        }
        public String getLogisticsTime() {
            return logisticsTime;
        }
        public void setLogisticsTime(String logisticsTime) {
            this.logisticsTime = logisticsTime;
        }
        public String getReceiptTime() {
            return receiptTime;
        }
        public void setReceiptTime(String receiptTime) {
            this.receiptTime = receiptTime;
        }
        public String getCancelReason() {
            return cancelReason;
        }
        public void setCancelReason(String cancelReason) {
            this.cancelReason = cancelReason;
        }
        public double getOrderAmount() {
            return orderAmount;
        }
        public void setOrderAmount(double orderAmount) {
            this.orderAmount = orderAmount;
        }
        public double getComboAmount() {
            return comboAmount;
        }
        public void setComboAmount(double comboAmount) {
            this.comboAmount = comboAmount;
        }
        public int getErpGoodsSpecId() {
            return erpGoodsSpecId;
        }
        public void setErpGoodsSpecId(int erpGoodsSpecId) {
            this.erpGoodsSpecId = erpGoodsSpecId;
        }
        public int getErpGoodsId() {
            return erpGoodsId;
        }
        public void setErpGoodsId(int erpGoodsId) {
            this.erpGoodsId = erpGoodsId;
        }
        public String getQuestionDesc() {
            return questionDesc;
        }
        public void setQuestionDesc(String questionDesc) {
            this.questionDesc = questionDesc;
        }
        public String getApplyTime() {
            return applyTime;
        }
        public void setApplyTime(String applyTime) {
            this.applyTime = applyTime;
        }
        public Integer getAuditStatus() {
            return auditStatus;
        }
        public void setAuditStatus(Integer auditStatus) {
            this.auditStatus = auditStatus;
        }
        public Integer getRefundStatus() {
            return refundStatus;
        }
        public void setRefundStatus(Integer refundStatus) {
            this.refundStatus = refundStatus;
        }
        public String getCreateOn() {
            return createOn;
        }
        public void setCreateOn(String createOn) {
            this.createOn = createOn;
        }
        public String getModifyOn() {
            return modifyOn;
        }
        public void setModifyOn(String modifyOn) {
            this.modifyOn = modifyOn;
        }
        public String getRemark() {
            return remark;
        }
        public void setRemark(String remark) {
            this.remark = remark;
        }
        
    
        
        
    
        
}
