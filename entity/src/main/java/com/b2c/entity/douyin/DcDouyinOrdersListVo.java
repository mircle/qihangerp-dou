package com.b2c.entity.douyin;

import java.util.List;

public class DcDouyinOrdersListVo extends DcDouyinOrdersEntity {
    private List<DcDouyinOrdersItemsEntity> items;

    public List<DcDouyinOrdersItemsEntity> getItems() {
        return items;
    }

    public void setItems(List<DcDouyinOrdersItemsEntity> items) {
        this.items = items;
    }
}
