package com.b2c.entity.douyin;

public class DcDouyinOrdersRefundItemEntity {
    private String sku_order_id;
    private String product_image;
    private int pay_amount;
    private int aftersale_item_num;
    private String logistics_code;
    private String product_name;
    private int item_num;
    private int order_status;
    private String shop_sku_code;
    private Double price;

    public String getSku_order_id() {
        return sku_order_id;
    }

    public void setSku_order_id(String sku_order_id) {
        this.sku_order_id = sku_order_id;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public int getPay_amount() {
        return pay_amount;
    }

    public void setPay_amount(int pay_amount) {
        this.pay_amount = pay_amount;
    }

    public int getAftersale_item_num() {
        return aftersale_item_num;
    }

    public void setAftersale_item_num(int aftersale_item_num) {
        this.aftersale_item_num = aftersale_item_num;
    }

    public String getLogistics_code() {
        return logistics_code;
    }

    public void setLogistics_code(String logistics_code) {
        this.logistics_code = logistics_code;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public int getItem_num() {
        return item_num;
    }

    public void setItem_num(int item_num) {
        this.item_num = item_num;
    }

    public int getOrder_status() {
        return order_status;
    }

    public void setOrder_status(int order_status) {
        this.order_status = order_status;
    }

    public String getShop_sku_code() {
        return shop_sku_code;
    }

    public void setShop_sku_code(String shop_sku_code) {
        this.shop_sku_code = shop_sku_code;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
