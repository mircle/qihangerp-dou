package com.b2c.entity;

import com.b2c.entity.erp.ErpGoodsEntity;

/**
 * 描述：商品中心
 *
 * @author qlp
 * @date 2019-11-08 17:44
 */
public class GoodsCenterEntity extends ErpGoodsEntity {
    private Long aliProductId;
    private String aliTitle;
    private String aliImage;
    private Long pddProductId;
    private String pddTitle;
    private String pddImage;
    private Long ygProductId;
    private String ygTitle;
    private String ygImage;
    private Long currentQty;//当前库存

    public String getAliTitle() {
        return aliTitle;
    }

    public void setAliTitle(String aliTitle) {
        this.aliTitle = aliTitle;
    }

    public String getAliImage() {
        return aliImage;
    }

    public void setAliImage(String aliImage) {
        this.aliImage = aliImage;
    }

    public String getPddTitle() {
        return pddTitle;
    }

    public void setPddTitle(String pddTitle) {
        this.pddTitle = pddTitle;
    }

    public String getPddImage() {
        return pddImage;
    }

    public void setPddImage(String pddImage) {
        this.pddImage = pddImage;
    }

    public String getYgTitle() {
        return ygTitle;
    }

    public void setYgTitle(String ygTitle) {
        this.ygTitle = ygTitle;
    }

    public String getYgImage() {
        return ygImage;
    }

    public void setYgImage(String ygImage) {
        this.ygImage = ygImage;
    }

    public Long getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Long currentQty) {
        this.currentQty = currentQty;
    }

    public Long getAliProductId() {
        return aliProductId;
    }

    public void setAliProductId(Long aliProductId) {
        this.aliProductId = aliProductId;
    }

    public Long getPddProductId() {
        return pddProductId;
    }

    public void setPddProductId(Long pddProductId) {
        this.pddProductId = pddProductId;
    }

    public Long getYgProductId() {
        return ygProductId;
    }

    public void setYgProductId(Long ygProductId) {
        this.ygProductId = ygProductId;
    }

}
