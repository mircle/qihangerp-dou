package com.b2c.entity;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description: pbd add 2019/1/9 13:37
 */
public class OrdersEntity {
    private Long id;
    private Integer shopId;
    /**
     * 订单编号
     */
    private String orderNum;
    /**
     * 用户id
     */
    private int userId;
    /**
     * 商品累计数量
     */
    private int goodsNum;
    /**
     * 商品单位件数
     */
    private int goodsUnitNum;
    /**
     * 订单商品详情
     */
    private String goodsDetail;
    /**
     * 商品总价
     */
    private BigDecimal goodsTotalPrice;
    /**
     * 运费
     */
    private BigDecimal freight;
    /**
     * 优惠抵扣价格
     */
    private BigDecimal discountPrice;
    /**
     * 订单总价
     */
    private BigDecimal orderTotalPrice;
    /**
     * 使用的余额金额
     */
    private BigDecimal usedBalance;
    /**
     * 使用的红包金额
     */
    private BigDecimal usedRedPacket;
    /**
     * 支付最终价格
     */
    private BigDecimal paymentPrice;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 区
     */
    private String district;
    /**
     * 收货详细地址
     */
    private String address;
    /**
     * 支付方式（1微信支付，2支付宝支付）
     */
    private int paymentMethod;
    /**
     * 支付状态 （0未支付1支付中2支付成功3支付失败）
     */
    private int paymentState;
    /**
     * 发起支付时间
     */
    private Long paymentTime;
    /**
     * 支付成功时间
     */
    private Long paymentResultTime;

    /**
     * 微信支付订单号
     */
    private String paymentTransactionId;

    private String paymentBankType;
    private String paymentTradeType;
    private String paymentPrepayId;
    /**
     * 订单状态0已取消1待支付2支付成功（待发货）3已发货（待收货）4已收货5已完成（过了售后期限）6售后中7已退货8退货失败
     */
    private int state;
    /**
     * 售后状态0未申请售后1售后申请中2同意退货3买家已发货4平台已收货5退款退货成功
     */
//    private int afterSaleState;
    /**
     *
     */
    private Long createOn; //创建时间
    private String createBy;
    private Long modifyOn;
    private String modifyBy;
    private String orderDetail;

    /**
     * 备注
     */
    private String comment;//订单备注(买家)
    private String sellerMemo;//卖家备注

    private String consignee; //收货人姓名
    private String consigneeMobile; //收货人手机号

    private String sendCompany; //发货物流公司
    private String sendCompanyCode; //发货物流公司代码
    private String sendCode; //发货物流单号
    private Long sendTime; //发货时间
    private int sendStatus; //send_status

    private Integer auditStatus;
    /**
     * 订单类型,0:平台1:有赞
     */
    private Integer type; //type
    //优惠券Id
    private Integer ticketId;

    private Long aliOrderId;//阿里订单Id

    private Integer addressId;//地址id

    private Integer isComment;//订单是否评价

    private BigDecimal paidPrice;//订单已支付价格

    private Integer buyerOrderType;//采购订单类型

    private Integer salesmanId;//导购师id
    private Integer developerId;//业务员id
    private String orderDevice;//订单设备（H5、PC、XIAO、APP）

    private String buyerName;//买家名

    private String source;//订单来源

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public Integer getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Integer salesmanId) {
        this.salesmanId = salesmanId;
    }

    public Integer getDeveloperId() {
        return developerId;
    }

    public void setDeveloperId(Integer developerId) {
        this.developerId = developerId;
    }

    public String getOrderDevice() {
        return orderDevice;
    }

    public void setOrderDevice(String orderDevice) {
        this.orderDevice = orderDevice;
    }

    public BigDecimal getPaidPrice() {
        return paidPrice;
    }

    public void setPaidPrice(BigDecimal paidPrice) {
        this.paidPrice = paidPrice;
    }

    public Integer getBuyerOrderType() {
        return buyerOrderType;
    }

    public void setBuyerOrderType(Integer buyerOrderType) {
        this.buyerOrderType = buyerOrderType;
    }

    public String getSendCompanyCode() {
        return sendCompanyCode;
    }

    public void setSendCompanyCode(String sendCompanyCode) {
        this.sendCompanyCode = sendCompanyCode;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(String orderDetail) {
        this.orderDetail = orderDetail;
    }

    public String getSendCompany() {
        return sendCompany;
    }

    public void setSendCompany(String sendCompany) {
        this.sendCompany = sendCompany;
    }

    public String getSendCode() {
        return sendCode;
    }

    public void setSendCode(String sendCode) {
        this.sendCode = sendCode;
    }

    public Long getSendTime() {
        return sendTime;
    }

    public void setSendTime(Long sendTime) {
        this.sendTime = sendTime;
    }

    public String getPaymentTransactionId() {
        return paymentTransactionId;
    }

    public int getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(int sendStatus) {
        this.sendStatus = sendStatus;
    }

    public void setPaymentTransactionId(String paymentTransactionId) {
        this.paymentTransactionId = paymentTransactionId;
    }

    public String getSellerMemo() {
        return sellerMemo;
    }

    public void setSellerMemo(String sellerMemo) {
        this.sellerMemo = sellerMemo;
    }

    public String getPaymentBankType() {
        return paymentBankType;
    }

    public void setPaymentBankType(String paymentBankType) {
        this.paymentBankType = paymentBankType;
    }

    public String getPaymentTradeType() {
        return paymentTradeType;
    }

    public void setPaymentTradeType(String paymentTradeType) {
        this.paymentTradeType = paymentTradeType;
    }

    public String getPaymentPrepayId() {
        return paymentPrepayId;
    }

    public void setPaymentPrepayId(String paymentPrepayId) {
        this.paymentPrepayId = paymentPrepayId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(int goodsNum) {
        this.goodsNum = goodsNum;
    }

    public int getGoodsUnitNum() {
        return goodsUnitNum;
    }

    public void setGoodsUnitNum(int goodsUnitNum) {
        this.goodsUnitNum = goodsUnitNum;
    }

    public String getGoodsDetail() {
        return goodsDetail;
    }

    public void setGoodsDetail(String goodsDetail) {
        this.goodsDetail = goodsDetail;
    }

    public BigDecimal getGoodsTotalPrice() {
        return goodsTotalPrice;
    }

    public void setGoodsTotalPrice(BigDecimal goodsTotalPrice) {
        this.goodsTotalPrice = goodsTotalPrice;
    }

    public BigDecimal getFreight() {
        return freight;
    }

    public void setFreight(BigDecimal freight) {
        this.freight = freight;
    }

    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }

    public BigDecimal getOrderTotalPrice() {
        return orderTotalPrice;
    }

    public void setOrderTotalPrice(BigDecimal orderTotalPrice) {
        this.orderTotalPrice = orderTotalPrice;
    }

    public BigDecimal getUsedBalance() {
        return usedBalance;
    }

    public void setUsedBalance(BigDecimal usedBalance) {
        this.usedBalance = usedBalance;
    }

    public BigDecimal getUsedRedPacket() {
        return usedRedPacket;
    }

    public void setUsedRedPacket(BigDecimal usedRedPacket) {
        this.usedRedPacket = usedRedPacket;
    }

    public BigDecimal getPaymentPrice() {
        return paymentPrice;
    }

    public void setPaymentPrice(BigDecimal paymentPrice) {
        this.paymentPrice = paymentPrice;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(int paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public int getPaymentState() {
        return paymentState;
    }

    public void setPaymentState(int paymentState) {
        this.paymentState = paymentState;
    }

    public Long getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Long paymentTime) {
        this.paymentTime = paymentTime;
    }

    public Long getPaymentResultTime() {
        return paymentResultTime;
    }

    public void setPaymentResultTime(Long paymentResultTime) {
        this.paymentResultTime = paymentResultTime;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

//    public int getAfterSaleState() {
//        return afterSaleState;
//    }
//
//    public void setAfterSaleState(int afterSaleState) {
//        this.afterSaleState = afterSaleState;
//    }

    public Long getCreateOn() {
        return createOn;
    }

    public void setCreateOn(Long createOn) {
        this.createOn = createOn;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Long getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(Long modifyOn) {
        this.modifyOn = modifyOn;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getConsigneeMobile() {
        return consigneeMobile;
    }

    public void setConsigneeMobile(String consigneeMobile) {
        this.consigneeMobile = consigneeMobile;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public Long getAliOrderId() {
        return aliOrderId;
    }

    public void setAliOrderId(Long aliOrderId) {
        this.aliOrderId = aliOrderId;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public Integer getIsComment() {
        return isComment;
    }

    public void setIsComment(Integer isComment) {
        this.isComment = isComment;
    }
}
