package com.b2c.entity;

/**
 * @Description:物流发货信息 pbd add 2019/6/17 14:47
 */
public class ExpressEntity {
    private Integer id;
    //订单编号,引用值（order.order_num）
    private String orderNum;
    //发货物流公司代码
    private String sendCompanyCode;
    //发货物流单号
    private String sendCode;
    //发货物流打印图片
    private String sendPrintImg;
    //创建时间
    private Long createOn;
    //菜鸟打印数据
    private String sendPrintStr;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getSendCompanyCode() {
        return sendCompanyCode;
    }

    public void setSendCompanyCode(String sendCompanyCode) {
        this.sendCompanyCode = sendCompanyCode;
    }

    public String getSendCode() {
        return sendCode;
    }

    public void setSendCode(String sendCode) {
        this.sendCode = sendCode;
    }

    public String getSendPrintImg() {
        return sendPrintImg;
    }

    public void setSendPrintImg(String sendPrintImg) {
        this.sendPrintImg = sendPrintImg;
    }

    public Long getCreateOn() {
        return createOn;
    }

    public void setCreateOn(Long createOn) {
        this.createOn = createOn;
    }

    public String getSendPrintStr() {
        return sendPrintStr;
    }

    public void setSendPrintStr(String sendPrintStr) {
        this.sendPrintStr = sendPrintStr;
    }
}
