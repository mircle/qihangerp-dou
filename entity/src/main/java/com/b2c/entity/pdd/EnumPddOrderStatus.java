package com.b2c.entity.pdd;


/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-16 20:49
 */
public enum EnumPddOrderStatus {
    //订单发货状态，1：待发货，2：已发货待签收，3：已签收 0：异常
    WaitSend("待发货", 1),
    HasSend("已发货待签收",2),
    Received("已签收",3),
    Exception("异常",0);

    // 成员变量
    private String name;
    private int index;


    // 构造方法
    private EnumPddOrderStatus(String name, int index) {
        this.name = name;
        this.index = index;

    }

    // 普通方法
    public static String getName(int index) {
        for (EnumPddOrderStatus c : EnumPddOrderStatus.values()) {
            if (c.getIndex()==index) {
                return c.name;
            }
        }
        return null;
    }


    public static int getIndex(String name) {
        for (EnumPddOrderStatus c : EnumPddOrderStatus.values()) {
            if (c.toString().equals(name)) {
                return c.index;
            }
        }
        return 0;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
