package com.b2c.entity.pdd;

import java.util.List;

/**
 * 拼多多订单entity
 */
public class OrderPddEntity {
    private Long id;//主键id
    private String orderSn;//订单编号
    private Integer tradeType;//订单类型 0-普通订单 ，1- 定金订单
    private Integer confirm_status;//成交状态：0：未成交、1：已成交、2：已取消
    private Integer free_sf;//是否顺丰包邮，1-是 0-否
    private Integer group_status;//成团状态：0：拼团中、1：已成团、2：团失败
    private Double capital_free_discount;//团长免单金额，单位：元
    private Double seller_discount;//商家优惠金额，单位：元
    private Double platform_discount;//平台优惠金额，单位：元
    private String remark;//订单备注
    private String updated_at;//订单的更新时间
    private Integer refund_status;//售后状态 1：无售后或售后关闭，2：售后处理中，3：退款中，4： 退款成功
    private Integer is_lucky_flag;//是否是抽奖订单，1-非抽奖订单，2-抽奖订单
    private Integer order_status;//订单状态
    private String shipping_time;//发货时间
    private String tracking_number;//快递单号
    private String tracking_company;//快递单号
    private String pay_type;//支付方式，枚举值：QQ,WEIXIN,ALIPAY,LIANLIANPAY
    private String pay_no;//支付单号
    private Double postage;//邮费，单位：元
    private Double discount_amount;//折扣金额，单位：元，折扣金额=平台优惠+商家优惠+团长免单优惠金额
    private Double goods_amount;//商品金额，单位：元，商品金额=商品销售价格*商品数量-改价金额（接口暂无该字段）
    private Double pay_amount;//支付金额，单位：元，支付金额=商品金额-折扣金额+邮费
    private String receiver_phone;//收件人电话，仅订单状态=待发货状态下返回明文，其他状态下返回脱敏手机号，例如“1387677****”
    private String receiver_name;//收件人姓名
    private String address;//详细地址
    private String town;
    private String city;
    private String province;
    private String country;
    private String created_time;//订单创建时间
    private String pay_time;//支付时间
    private String confirm_time;//成交时间
    private String receive_time;//确认收货时间
    private String last_ship_time;//订单承诺发货时间
    private String buyer_memo;//买家留言信息
    private Integer after_sales_status;//售后状态 0：无售后 2：买家申请退款，待商家处理 3：退货退款，待商家处理 4：商家同意退款，退款中 5：平台同意退款，退款中 6：驳回退款， 待买家处理 7：已同意退货退款,待用户发货 8：平台处理中 9：平台拒 绝退款，退款关闭 10：退款成功 11：买家撤销 12：买家逾期未处 理，退款失败 13：买家逾期，超过有效期 14 : 换货补寄待商家处理 15:换货补寄待用户处理 16:换货补寄成功 17:换货补寄失败 18:换货补寄待用户确认完成
    private Long orderConfirmTime;//订单成交时间
    private Integer auditStatus;
    private Integer shopId;
    private Integer shopType;
    private String shopName;
    private Integer sendStatus;//发货状态（0待出库1拣货中2已拣货3已出库4已发货）
    private String tag;//标签(1：实售2：淘宝客3：刷单4：返现)
    private String excelMsg;
    private Integer excelLogId = 0;
    private String encryptedData;
    private String signature;
    private Integer totalQty;//总数量
    private String nameKey;
    private String phoneKey;
    private String addressKey;
    private String result;
    private String receiver_name1;
    private String receiver_phone1;
    private String receiver_address1;
    


    public Integer getShopType() {
        return shopType;
    }

    public void setShopType(Integer shopType) {
        this.shopType = shopType;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getReceiver_name1() {
        return receiver_name1;
    }

    public void setReceiver_name1(String receiver_name1) {
        this.receiver_name1 = receiver_name1;
    }

    public String getReceiver_phone1() {
        return receiver_phone1;
    }

    public void setReceiver_phone1(String receiver_phone1) {
        this.receiver_phone1 = receiver_phone1;
    }

    public String getReceiver_address1() {
        return receiver_address1;
    }

    public void setReceiver_address1(String receiver_address1) {
        this.receiver_address1 = receiver_address1;
    }

    public String getLast_ship_time() {
        return last_ship_time;
    }

    public void setLast_ship_time(String last_ship_time) {
        this.last_ship_time = last_ship_time;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getTracking_company() {
        return tracking_company;
    }

    public void setTracking_company(String tracking_company) {
        this.tracking_company = tracking_company;
    }

    private List<OrderPddItemEntity> items;//订单明细


    public Long getOrderConfirmTime() {
        return orderConfirmTime;
    }

    public void setOrderConfirmTime(Long orderConfirmTime) {
        this.orderConfirmTime = orderConfirmTime;
    }

    public List<OrderPddItemEntity> getItems() {
        return items;
    }

    public void setItems(List<OrderPddItemEntity> items) {
        this.items = items;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Integer getTradeType() {
        return tradeType;
    }

    public void setTradeType(Integer tradeType) {
        this.tradeType = tradeType;
    }

    public Integer getConfirm_status() {
        return confirm_status;
    }

    public void setConfirm_status(Integer confirm_status) {
        this.confirm_status = confirm_status;
    }

    public Integer getFree_sf() {
        return free_sf;
    }

    public void setFree_sf(Integer free_sf) {
        this.free_sf = free_sf;
    }

    public Integer getGroup_status() {
        return group_status;
    }

    public void setGroup_status(Integer group_status) {
        this.group_status = group_status;
    }

    public Double getCapital_free_discount() {
        return capital_free_discount;
    }

    public void setCapital_free_discount(Double capital_free_discount) {
        this.capital_free_discount = capital_free_discount;
    }

    public Double getSeller_discount() {
        return seller_discount;
    }

    public void setSeller_discount(Double seller_discount) {
        this.seller_discount = seller_discount;
    }

    public Double getPlatform_discount() {
        return platform_discount;
    }

    public void setPlatform_discount(Double platform_discount) {
        this.platform_discount = platform_discount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public Integer getRefund_status() {
        return refund_status;
    }

    public void setRefund_status(Integer refund_status) {
        this.refund_status = refund_status;
    }

    public Integer getIs_lucky_flag() {
        return is_lucky_flag;
    }

    public void setIs_lucky_flag(Integer is_lucky_flag) {
        this.is_lucky_flag = is_lucky_flag;
    }

    public Integer getOrder_status() {
        return order_status;
    }

    public void setOrder_status(Integer order_status) {
        this.order_status = order_status;
    }

    public String getShipping_time() {
        return shipping_time;
    }

    public void setShipping_time(String shipping_time) {
        this.shipping_time = shipping_time;
    }

    public String getTracking_number() {
        return tracking_number;
    }

    public void setTracking_number(String tracking_number) {
        this.tracking_number = tracking_number;
    }

    public String getPay_type() {
        return pay_type;
    }

    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }

    public String getPay_no() {
        return pay_no;
    }

    public void setPay_no(String pay_no) {
        this.pay_no = pay_no;
    }

    public Double getPostage() {
        return postage;
    }

    public void setPostage(Double postage) {
        this.postage = postage;
    }

    public Double getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(Double discount_amount) {
        this.discount_amount = discount_amount;
    }

    public Double getGoods_amount() {
        return goods_amount;
    }

    public void setGoods_amount(Double goods_amount) {
        this.goods_amount = goods_amount;
    }

    public Double getPay_amount() {
        return pay_amount;
    }

    public void setPay_amount(Double pay_amount) {
        this.pay_amount = pay_amount;
    }



    public String getReceiver_name() {
        return receiver_name;
    }

    public void setReceiver_name(String receiver_name) {
        this.receiver_name = receiver_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public String getPay_time() {
        return pay_time;
    }

    public void setPay_time(String pay_time) {
        this.pay_time = pay_time;
    }

    public String getConfirm_time() {
        return confirm_time;
    }

    public void setConfirm_time(String confirm_time) {
        this.confirm_time = confirm_time;
    }

    public String getReceive_time() {
        return receive_time;
    }

    public void setReceive_time(String receive_time) {
        this.receive_time = receive_time;
    }

    public String getBuyer_memo() {
        return buyer_memo;
    }

    public void setBuyer_memo(String buyer_memo) {
        this.buyer_memo = buyer_memo;
    }

    public Integer getAfter_sales_status() {
        return after_sales_status;
    }

    public void setAfter_sales_status(Integer after_sales_status) {
        this.after_sales_status = after_sales_status;
    }

    public Integer getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(Integer sendStatus) {
        this.sendStatus = sendStatus;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getExcelMsg() {
        return excelMsg;
    }

    public void setExcelMsg(String excelMsg) {
        this.excelMsg = excelMsg;
    }

    public Integer getExcelLogId() {
        return excelLogId;
    }

    public void setExcelLogId(Integer excelLogId) {
        this.excelLogId = excelLogId;
    }

    public String getReceiver_phone() {
        return receiver_phone;
    }

    public void setReceiver_phone(String receiver_phone) {
        this.receiver_phone = receiver_phone;
    }

    public String getEncryptedData() {
        return encryptedData;
    }

    public void setEncryptedData(String encryptedData) {
        this.encryptedData = encryptedData;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Integer getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(Integer totalQty) {
        this.totalQty = totalQty;
    }

    public String getNameKey() {
        return nameKey;
    }

    public void setNameKey(String nameKey) {
        this.nameKey = nameKey;
    }

    public String getPhoneKey() {
        return phoneKey;
    }

    public void setPhoneKey(String phoneKey) {
        this.phoneKey = phoneKey;
    }

    public String getAddressKey() {
        return addressKey;
    }

    public void setAddressKey(String addressKey) {
        this.addressKey = addressKey;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }


}
