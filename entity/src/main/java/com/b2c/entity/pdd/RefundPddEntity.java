package com.b2c.entity.pdd;

public class RefundPddEntity {
    private Long id;//` BIGINT(20) NOT NULL COMMENT '售后编号',
    private String order_sn;//` VARCHAR(50) NOT NULL COMMENT '订单编号',
    private Integer shopId;//` INT(11) NOT NULL COMMENT '内部店铺ID',
    private String shopName;
    private Integer after_sales_type;//` INT(11) NOT NULL COMMENT '必填，售后类型2：仅退款 3：退货退款 4：换货 5：缺货补寄',
    private Integer after_sales_status;//` INT(11) NOT NULL COMMENT '必填，售后状态 1：全部 2：买家申请退款，待商家处理 3：退货退款，待商家处理 4：商家同意退款，退款中 5：平台同意退款，退款中 6：驳回退款， 待买家处理 7：已同意退货退款,待用户发货 8：平台处理中 9：平台拒 绝退款，退款关闭 10：退款成功 11：买家撤销 12：买家逾期未处 理，退款失败 13：买家逾期，超过有效期 14 : 换货补寄待商家处理 15:换货补寄待用户处理 16:换货补寄成功 17:换货补寄失败 18:换货补寄待用户确认完成 31：商家同意拒收退款，待用户拒收;32: 待商家补寄发货',
    private String after_sale_reason;//` VARCHAR(50) NOT NULL COMMENT '售后原因',
    private Long confirm_time;//` BIGINT(20) NOT NULL COMMENT '时间',
    private Long created_time;//` BIGINT(20) NOT NULL COMMENT '时间',
    private Double discount_amount;//` DOUBLE NOT NULL COMMENT '订单折扣金额（元）',
    private Double order_amount;//` DOUBLE NOT NULL COMMENT '订单金额（元）',
    private Double refund_amount;//` DOUBLE NOT NULL COMMENT '退款金额（元）',
    private String goods_image;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '商品图片',
    private Long goodsId;//
    private String goods_name;//` VARCHAR(100) NULL DEFAULT NULL COMMENT '商品名称',
    private String goods_number;//商品编码,
    private String skuNumber;//商品编码,
    private String skuInfo;//商品编码,
    private Double goods_price;//` DOUBLE NOT NULL COMMENT '商品价格，单位：元',
    private Integer quantity;//退货数量
    private String updated_time;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '更新时间',
    private String tracking_number;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '快递单号',
    private String tracking_company;//快递公司
    private Integer auditStatus;//审核状态
    private Integer shippingStatus;//
    private Integer userShippingStatus;//0-未勾选 1-消费者选择的收货状态为未收到货 2-消费者选择的收货状态为已收到货\
    private Integer orderStatus;//订单状态
    private String remark;

    private String sign;
    private String auditTime;//处理时间
    private Long skuId;
    private String describe;
    private String erpGoodsName;
    private String GoodsSpec;
    private Integer orderReturnStatus;//仓库退回状态
    
    
    
    public String getErpGoodsName() {
        return erpGoodsName;
    }

    public void setErpGoodsName(String erpGoodsName) {
        this.erpGoodsName = erpGoodsName;
    }

    public String getGoodsSpec() {
        return GoodsSpec;
    }

    public void setGoodsSpec(String goodsSpec) {
        GoodsSpec = goodsSpec;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public Integer getShippingStatus() {
        return shippingStatus;
    }

    public void setShippingStatus(Integer shippingStatus) {
        this.shippingStatus = shippingStatus;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getTracking_company() {
        return tracking_company;
    }

    public void setTracking_company(String tracking_company) {
        this.tracking_company = tracking_company;
    }

    public String getSkuInfo() {
        return skuInfo;
    }

    public void setSkuInfo(String skuInfo) {
        this.skuInfo = skuInfo;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getSkuNumber() {
        return skuNumber;
    }

    public void setSkuNumber(String skuNumber) {
        this.skuNumber = skuNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrder_sn() {
        return order_sn;
    }

    public void setOrder_sn(String order_sn) {
        this.order_sn = order_sn;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getAfter_sales_type() {
        return after_sales_type;
    }

    public void setAfter_sales_type(Integer after_sales_type) {
        this.after_sales_type = after_sales_type;
    }

    public Integer getAfter_sales_status() {
        return after_sales_status;
    }

    public void setAfter_sales_status(Integer after_sales_status) {
        this.after_sales_status = after_sales_status;
    }

    public String getAfter_sale_reason() {
        return after_sale_reason;
    }

    public void setAfter_sale_reason(String after_sale_reason) {
        this.after_sale_reason = after_sale_reason;
    }

    public Long getConfirm_time() {
        return confirm_time;
    }

    public void setConfirm_time(Long confirm_time) {
        this.confirm_time = confirm_time;
    }

    public Long getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Long created_time) {
        this.created_time = created_time;
    }

    public Double getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(Double discount_amount) {
        this.discount_amount = discount_amount;
    }

    public Double getOrder_amount() {
        return order_amount;
    }

    public void setOrder_amount(Double order_amount) {
        this.order_amount = order_amount;
    }

    public Double getRefund_amount() {
        return refund_amount;
    }

    public void setRefund_amount(Double refund_amount) {
        this.refund_amount = refund_amount;
    }

    public String getGoods_image() {
        return goods_image;
    }

    public void setGoods_image(String goods_image) {
        this.goods_image = goods_image;
    }


    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getGoods_number() {
        return goods_number;
    }

    public void setGoods_number(String goods_number) {
        this.goods_number = goods_number;
    }

    public Double getGoods_price() {
        return goods_price;
    }

    public void setGoods_price(Double goods_price) {
        this.goods_price = goods_price;
    }

    public String getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(String updated_time) {
        this.updated_time = updated_time;
    }

    public String getTracking_number() {
        return tracking_number;
    }

    public void setTracking_number(String tracking_number) {
        this.tracking_number = tracking_number;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Integer getUserShippingStatus() {
        return userShippingStatus;
    }

    public void setUserShippingStatus(Integer userShippingStatus) {
        this.userShippingStatus = userShippingStatus;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(String auditTime) {
        this.auditTime = auditTime;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getOrderReturnStatus() {
        return orderReturnStatus;
    }

    public void setOrderReturnStatus(Integer orderReturnStatus) {
        this.orderReturnStatus = orderReturnStatus;
    }
}
