package com.b2c.entity.pdd;


/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-16 20:49
 */
public enum EnumPddPrintCompany {
    //订单发货状态，1：待发货，2：已发货待签收，3：已签收 0：异常
    YUNDA ("YUNDA","韵达快递","https://file-link.pinduoduo.com/yunda_std"),
    JITU("JTSD","极兔速递","https://file-link.pinduoduo.com/jtsd_one"),
    BAISHI("HT","百世快递","https://file-link.pinduoduo.com/ht_std");

    // 成员变量
    private String index;
    private String name;
    private String value;


    // 构造方法
    private EnumPddPrintCompany( String index,String name,String value) {
        this.index = index;
        this.name=name;
        this.value = value;

    }

    // 普通方法
    public static String getValue(String index) {
        for (EnumPddPrintCompany c : EnumPddPrintCompany.values()) {
            if (c.getIndex().equals(index)) {
                return c.value;
            }
        }
        return null;
    }
    public static String getValueByName(String name) {
        for (EnumPddPrintCompany c : EnumPddPrintCompany.values()) {
            if (c.getName().equals(name)) {
                return c.value;
            }
        }
        return null;
    }
    public static String getName(String index) {
        for (EnumPddPrintCompany c : EnumPddPrintCompany.values()) {
            if (c.getIndex().equals(index)) {
                return c.name;
            }
        }
        return null;
    }

    public static String getIndexByName(String name) {
        for (EnumPddPrintCompany c : EnumPddPrintCompany.values()) {
            if (c.getName().equals(name)) {
                return c.index;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
