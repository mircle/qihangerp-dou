package com.b2c.entity.pdd;

import java.math.BigDecimal;

public class OrderPddItemEntity {
    private Long id;
    private Long orderId;
    private String orderSn;
    private String goodsName;
    private String goodsImg;
    private String goodsNum;
    private String goodsSpec;
    private String goodsSpecNum;
    private Integer quantity;
    private Integer erpGoodsId;
    private Integer erpGoodsSpecId;
    private Double goodsPrice;
    private Double goodsPurPrice;//采购价
    private Integer isGift;//是否礼品0否1是
    private Long pddGoodId;
    private Long pddSkuId;
    private Double itemAmount;//子订单金额


    public Long getPddSkuId() {
        return pddSkuId;
    }

    public void setPddSkuId(Long pddSkuId) {
        this.pddSkuId = pddSkuId;
    }

    private Integer erpContactId;
    private Integer refundCount;
    private Long currentQty;
    private String lastShipTime;
    private String result;
    private Long pickingQty;
    private String remark;
    


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getIsGift() {
        return isGift;
    }

    public void setIsGift(Integer isGift) {
        this.isGift = isGift;
    }

    public Integer getErpGoodsId() {
        return erpGoodsId;
    }

    public void setErpGoodsId(Integer erpGoodsId) {
        this.erpGoodsId = erpGoodsId;
    }

    public Integer getErpGoodsSpecId() {
        return erpGoodsSpecId;
    }

    public void setErpGoodsSpecId(Integer erpGoodsSpecId) {
        this.erpGoodsSpecId = erpGoodsSpecId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    public String getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getGoodsSpec() {
        return goodsSpec;
    }

    public void setGoodsSpec(String goodsSpec) {
        this.goodsSpec = goodsSpec;
    }

    public String getGoodsSpecNum() {
        return goodsSpecNum;
    }

    public void setGoodsSpecNum(String goodsSpecNum) {
        this.goodsSpecNum = goodsSpecNum;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(Double goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Long getPddGoodId() {
        return pddGoodId;
    }

    public void setPddGoodId(Long pddGoodId) {
        this.pddGoodId = pddGoodId;
    }

    public Integer getErpContactId() {
        return erpContactId;
    }

    public void setErpContactId(Integer erpContactId) {
        this.erpContactId = erpContactId;
    }

    public Integer getRefundCount() {
        return refundCount;
    }

    public void setRefundCount(Integer refundCount) {
        this.refundCount = refundCount;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Long getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Long currentQty) {
        this.currentQty = currentQty;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getLastShipTime() {
        return lastShipTime;
    }

    public void setLastShipTime(String lastShipTime) {
        this.lastShipTime = lastShipTime;
    }

    public Long getPickingQty() {
        return pickingQty;
    }

    public void setPickingQty(Long pickingQty) {
        this.pickingQty = pickingQty;
    }

    public Double getGoodsPurPrice() {
        return goodsPurPrice;
    }

    public void setGoodsPurPrice(Double goodsPurPrice) {
        this.goodsPurPrice = goodsPurPrice;
    }

    public Double getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(Double itemAmount) {
        this.itemAmount = itemAmount;
    }


}
