package com.b2c.entity.datacenter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 描述：
 * 阿里订单entity
 *
 * @author qlp
 * @date 2019-09-16 16:15
 */
public class DcAliOrderEntity {
    private Long id;//` bigint(20) NOT NULL COMMENT '订单id',
    private String sellerID;//` varchar(50) DEFAULT NULL COMMENT '卖家id',
    private Long buyerUserId;//` bigint(20) NOT NULL COMMENT '阿里UserId',
    private String tradeType;//` varchar(20) NOT NULL DEFAULT '' COMMENT '1:担保交易 2:预存款交易 3:ETC境外收单交易 4:即时到帐交易 5:保障金安全交易 6:统一交易流程 7:分阶段付款 8.货到付款交易 9.信用凭证支付交易 10.账期支付交易，50060 交易4.0',
    private BigDecimal totalAmount;//` decimal(10,2) NOT NULL COMMENT '应付款总金额，totalAmount = ∑itemAmount + shippingFee，单位为元',
    private BigDecimal shippingFee;//` decimal(11,2) NOT NULL COMMENT '运费',
    private Long discount;//` bigint(20) NOT NULL DEFAULT 0 COMMENT '折扣信息，单位分',
    private Date createTime;//` datetime NOT NULL COMMENT '创建时间',
    private Date modifyTime;//` datetime NOT NULL COMMENT '修改时间',
    private Date payTime;//` datetime DEFAULT NULL COMMENT '付款时间，如果有多次付款，这里返回的是首次付款时间',
    private Date confirmedTime;//` datetime DEFAULT NULL COMMENT '确认时间',
    private Date allDeliveredTime;//` datetime DEFAULT NULL COMMENT '完全发货时间',
    private Date completeTime;//` datetime DEFAULT NULL COMMENT '完成时间',
    private String refundStatus;//` varchar(20) NOT NULL DEFAULT '' COMMENT '订单的售中退款状态，等待卖家同意：waitselleragree ，待买家修改：waitbuyermodify，等待买家退货：waitbuyersend，等待卖家确认收货：waitsellerreceive，退款成功：refundsuccess，退款失败：refundclose',
    private String remark;//` varchar(50) DEFAULT '' COMMENT '备注，1688指下单时的备注',
    private BigDecimal sumProductPayment;//` decimal(15,2) NOT NULL COMMENT '产品总金额(该订单产品明细表中的产品金额的和)，单位元',
    private String buyerFeedback;//` varchar(500) DEFAULT NULL COMMENT '买家留言，不超过500字',
    private String closeReason;//` varchar(50) DEFAULT '' COMMENT '关闭原因。buyerCancel:买家取消订单，sellerGoodsLack:卖家库存不足，other:其它',
    private Date receivingTime;//` datetime DEFAULT NULL COMMENT '收货时间，这里返回的是完全收货时间',
    private String refundStatusForAs;//` varchar(20) DEFAULT '' COMMENT '订单的售后退款状态',
    private Integer stepPayAll;//` int(11) DEFAULT NULL COMMENT '是否一次性付款',
    private String sellerMemo;//` varchar(100) DEFAULT NULL COMMENT '卖家备忘信息',
    private String refundId;//` varchar(45) DEFAULT NULL COMMENT '退款单ID',
    private BigDecimal refund;//` decimal(10,2) DEFAULT NULL COMMENT '退款金额，单位为元',
    private Long refundPayment;//` bigint(20) DEFAULT NULL COMMENT '退款金额',
    private String status;//` varchar(50) NOT NULL DEFAULT '' COMMENT '交易状态，waitbuyerpay:等待买家付款;waitsellersend:等待卖家发货;waitlogisticstakein:等待物流公司揽件;waitbuyerreceive:等待买家收货;waitbuyersign:等待买家签收;signinsuccess:买家已签收;confirm_goods:已收货;success:交易成功;cancel:交易取消;terminated:交易终止;未枚举:其他状态',
    private Integer auditStatus;//订单审核状态（0待审核1已审核）
    private Long orderId;//平台订单Id
    private String logisticsCompany;//物流公司
    private String logisticsCode;//物流单号
    private Integer shopId;
    private Integer sendStatus;//发货状态（0待出库1拣货中2已拣货3已出库4已发货）

    private Integer developerId;//业务员Id
    private String developerName;
    private Integer clientUserId;//客户Id
    private String clientName;

    public Integer getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(Integer sendStatus) {
        this.sendStatus = sendStatus;
    }

    public Integer getDeveloperId() {
        return developerId;
    }

    public void setDeveloperId(Integer developerId) {
        this.developerId = developerId;
    }

    public String getDeveloperName() {
        return developerName;
    }

    public void setDeveloperName(String developerName) {
        this.developerName = developerName;
    }

    public Integer getClientUserId() {
        return clientUserId;
    }

    public void setClientUserId(Integer clientUserId) {
        this.clientUserId = clientUserId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public String getLogisticsCode() {
        return logisticsCode;
    }

    public void setLogisticsCode(String logisticsCode) {
        this.logisticsCode = logisticsCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSellerID() {
        return sellerID;
    }

    public void setSellerID(String sellerID) {
        this.sellerID = sellerID;
    }

    public Long getBuyerUserId() {
        return buyerUserId;
    }

    public void setBuyerUserId(Long buyerUserId) {
        this.buyerUserId = buyerUserId;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(BigDecimal shippingFee) {
        this.shippingFee = shippingFee;
    }

    public Long getDiscount() {
        return discount;
    }

    public void setDiscount(Long discount) {
        this.discount = discount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Date getConfirmedTime() {
        return confirmedTime;
    }

    public void setConfirmedTime(Date confirmedTime) {
        this.confirmedTime = confirmedTime;
    }

    public Date getAllDeliveredTime() {
        return allDeliveredTime;
    }

    public void setAllDeliveredTime(Date allDeliveredTime) {
        this.allDeliveredTime = allDeliveredTime;
    }

    public Date getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Date completeTime) {
        this.completeTime = completeTime;
    }

    public String getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(String refundStatus) {
        this.refundStatus = refundStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getSumProductPayment() {
        return sumProductPayment;
    }

    public void setSumProductPayment(BigDecimal sumProductPayment) {
        this.sumProductPayment = sumProductPayment;
    }

    public String getBuyerFeedback() {
        return buyerFeedback;
    }

    public void setBuyerFeedback(String buyerFeedback) {
        this.buyerFeedback = buyerFeedback;
    }

    public String getCloseReason() {
        return closeReason;
    }

    public void setCloseReason(String closeReason) {
        this.closeReason = closeReason;
    }

    public Date getReceivingTime() {
        return receivingTime;
    }

    public void setReceivingTime(Date receivingTime) {
        this.receivingTime = receivingTime;
    }

    public String getRefundStatusForAs() {
        return refundStatusForAs;
    }

    public void setRefundStatusForAs(String refundStatusForAs) {
        this.refundStatusForAs = refundStatusForAs;
    }

    public Integer getStepPayAll() {
        return stepPayAll;
    }

    public void setStepPayAll(Integer stepPayAll) {
        this.stepPayAll = stepPayAll;
    }

    public String getSellerMemo() {
        return sellerMemo;
    }

    public void setSellerMemo(String sellerMemo) {
        this.sellerMemo = sellerMemo;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public BigDecimal getRefund() {
        return refund;
    }

    public void setRefund(BigDecimal refund) {
        this.refund = refund;
    }

    public Long getRefundPayment() {
        return refundPayment;
    }

    public void setRefundPayment(Long refundPayment) {
        this.refundPayment = refundPayment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }
}
