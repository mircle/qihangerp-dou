package com.b2c.entity.datacenter;

import java.util.List;

/**
 * 描述：
 * 阿里订单列表vo
 *
 * @author qlp
 * @date 2019-09-16 19:56
 */
public class DcAliOrderListVo extends DcAliOrderEntity {
    private String buyerName;
    private String buyerLoginId;
    private String contactPerson;
    private String address;
    private String mobile;

    private Integer totalQuantity;//总商品数量


    private List<DcAliOrderItemEntity> items;

    public Integer getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerLoginId() {
        return buyerLoginId;
    }

    public void setBuyerLoginId(String buyerLoginId) {
        this.buyerLoginId = buyerLoginId;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public List<DcAliOrderItemEntity> getItems() {
        return items;
    }

    public void setItems(List<DcAliOrderItemEntity> items) {
        this.items = items;
    }
    
}
