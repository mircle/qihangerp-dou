package com.b2c.entity.fahuo;

import java.math.BigDecimal;

/**
 * 描述：
 * erp系统订单明细
 *
 * @author qlp
 * @date 2019-09-17 11:55
 */
public class OrderSendItemEntity {
    private Long id;//
    private Long orderSendId;//
   
    private String goodsName;//商品名',
    private Integer goodsId;//'erp系统商品id',
    private String goodsNumber;//erp商品编码',
    private String goodsSpecNumber;//erp系统sku编码',
    private String goodsSpec;//sku名
    private Integer goodsSpecId;//erp系统goodsSpecId
    private String goodsImg;//` varchar(100) DEFAULT NULL,

    private Integer quantity;//` bigint(20) NOT NULL,
    private Double price;
    private Double purPrice;
    private BigDecimal itemAmount;//子订单价格
    private Integer status;//子订单状态,0未处理1拣货中2已拣货3已发货',
    
    private Long stockOutFormId;//出库单号,关联erp_stock_out_form
    private Long stockOutFormItemId;//出库单明细id，关联表erp_stock_out_form_item
    private String stockOutNo;//出库单号
    private Integer outLocationId;//出库仓库id，用于订单出库
    private String locationName;    //仓库名
    private Long stockInfoItemId;

    private String createTime;      
    private String modifyTime;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getOrderSendId() {
        return orderSendId;
    }
    public void setOrderSendId(Long orderSendId) {
        this.orderSendId = orderSendId;
    }
    public String getGoodsName() {
        return goodsName;
    }
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    public Integer getGoodsId() {
        return goodsId;
    }
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }
    public String getGoodsNumber() {
        return goodsNumber;
    }
    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }
    public String getGoodsSpecNumber() {
        return goodsSpecNumber;
    }
    public void setGoodsSpecNumber(String goodsSpecNumber) {
        this.goodsSpecNumber = goodsSpecNumber;
    }
    public String getGoodsSpec() {
        return goodsSpec;
    }
    public void setGoodsSpec(String goodsSpec) {
        this.goodsSpec = goodsSpec;
    }
    public Integer getGoodsSpecId() {
        return goodsSpecId;
    }
    public void setGoodsSpecId(Integer goodsSpecId) {
        this.goodsSpecId = goodsSpecId;
    }
    public String getGoodsImg() {
        return goodsImg;
    }
    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }
    public Integer getQuantity() {
        return quantity;
    }
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price) {
        this.price = price;
    }
    public Double getPurPrice() {
        return purPrice;
    }
    public void setPurPrice(Double purPrice) {
        this.purPrice = purPrice;
    }
    public BigDecimal getItemAmount() {
        return itemAmount;
    }
    public void setItemAmount(BigDecimal itemAmount) {
        this.itemAmount = itemAmount;
    }
    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public Long getStockOutFormId() {
        return stockOutFormId;
    }
    public void setStockOutFormId(Long stockOutFormId) {
        this.stockOutFormId = stockOutFormId;
    }
    public Long getStockOutFormItemId() {
        return stockOutFormItemId;
    }
    public void setStockOutFormItemId(Long stockOutFormItemId) {
        this.stockOutFormItemId = stockOutFormItemId;
    }
    public String getStockOutNo() {
        return stockOutNo;
    }
    public void setStockOutNo(String stockOutNo) {
        this.stockOutNo = stockOutNo;
    }
    public Integer getOutLocationId() {
        return outLocationId;
    }
    public void setOutLocationId(Integer outLocationId) {
        this.outLocationId = outLocationId;
    }
    public String getLocationName() {
        return locationName;
    }
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
    public Long getStockInfoItemId() {
        return stockInfoItemId;
    }
    public void setStockInfoItemId(Long stockInfoItemId) {
        this.stockInfoItemId = stockInfoItemId;
    }
    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    public String getModifyTime() {
        return modifyTime;
    }
    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }
   
    
}
