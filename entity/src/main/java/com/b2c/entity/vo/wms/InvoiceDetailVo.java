package com.b2c.entity.vo.wms;

import com.b2c.entity.erp.InvoiceEntity;
import com.b2c.entity.erp.vo.InvoiceInfoListVo;

import java.util.List;

/**
 * 描述：
 * 采购表单detail
 *
 * @author qlp
 * @date 2019-05-29 11:37
 */
public class InvoiceDetailVo extends InvoiceEntity {
    /**
     * 表单商品items
     */
    private List<InvoiceInfoListVo> items;

    public List<InvoiceInfoListVo> getItems() {
        return items;
    }

    public void setItems(List<InvoiceInfoListVo> items) {
        this.items = items;
    }
}
