package com.b2c.entity.vo;

import com.b2c.entity.ErpGoodsStockInfoEntity;
import com.b2c.entity.ErpOrderReturnItemEntity;

import java.util.List;

/**
 * 描述：
 * 退货订单item展示
 *
 * @author qlp
 * @date 2019-10-15 15:39
 */
public class ErpOrderReturnItemVo extends ErpOrderReturnItemEntity {
    private String orderNum;
    private String sourceOrderNum;
    private String shopName;
    private String goodsName;
    private String goodsNumber;
    private String unitName;
    private String colorValue;
    private String colorImage;
    private String sizeValue;
    private String contactPerson;
    private List<ErpGoodsStockInfoEntity>  items;

    public String getColorImage() {

        return colorImage;
    }

    public void setColorImage(String colorImage) {
        this.colorImage = colorImage;
    }

    public String getSourceOrderNum() {
        return sourceOrderNum;
    }

    public void setSourceOrderNum(String sourceOrderNum) {
        this.sourceOrderNum = sourceOrderNum;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public List<ErpGoodsStockInfoEntity> getItems() {
        return items;
    }

    public void setItems(List<ErpGoodsStockInfoEntity> items) {
        this.items = items;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }
}
