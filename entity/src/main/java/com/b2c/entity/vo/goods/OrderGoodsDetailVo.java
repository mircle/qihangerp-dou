package com.b2c.entity.vo.goods;

import java.math.BigDecimal;

/**
 * 描述：
 * 订单商品详情VO
 * <p>
 * ly add 2019/1/9 14:29
 */
public class OrderGoodsDetailVo {
    /***
     * 后期删除
     */
    private String barCode;

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    private String specNumber;
    private String color;
    private Integer count;
    private Integer goodsId;
    private String image;
    private BigDecimal price;
    private String size;
    private Integer specId;
    private String title;
    private BigDecimal discountPrice;

    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }


    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    /*****不知道有没有用的字段******/
//    private Integer currentQty;
//    private Integer lockedNum;

//
//    public Integer getCurrentQty() {
//        return currentQty;
//    }
//
//    public void setCurrentQty(Integer currentQty) {
//        this.currentQty = currentQty;
//    }
//
//    public Integer getLockedNum() {
//        return lockedNum;
//    }
//
//    public void setLockedNum(Integer lockedNum) {
//        this.lockedNum = lockedNum;
//    }
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
