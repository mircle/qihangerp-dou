package com.b2c.entity.vo;

/**
 * @Description: pbd add 2019/9/28 11:32
 */
public class WmsMangeUserPermissionKeyVo {
    //权限标识
    private Integer permissionId;
    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }
}
