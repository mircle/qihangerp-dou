package com.b2c.entity.vo;

import com.b2c.entity.OrderCancelEntity;

/**
 * 描述：
 * 退货订单VO
 *
 * @author qlp
 * @date 2019-02-21 17:02
 */
public class RefundOrderListVo extends OrderCancelEntity {
    private String goodsTitle;
    private String goodsImage;
    private String goodsColor;
    private String goodsSize;
    private double goodsPrice;
    private Integer orderType;//订单类型（）
    private int goodsCount;
    private String orderNum;
    private Integer paymentMethod;  //支付方式
    private String paymentResultTime;//支付时间
    private String afterCreateOn; //售后申请时间
    private String afterFinishOn; //售后退款时间
    private String mjMobile;  //买家手机号
    private String tjrMobile;  //推荐人手机号
    private String consignee; //收货人姓名
    private String consigneeMobile; //收货人手机号
    private String address;  //收货地址
    private Long totalCount;    //总数量

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getConsigneeMobile() {
        return consigneeMobile;
    }

    public void setConsigneeMobile(String consigneeMobile) {
        this.consigneeMobile = consigneeMobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMjMobile() {
        return mjMobile;
    }

    public void setMjMobile(String mjMobile) {
        this.mjMobile = mjMobile;
    }

    public String getTjrMobile() {
        return tjrMobile;
    }

    public void setTjrMobile(String tjrMobile) {
        this.tjrMobile = tjrMobile;
    }

    public String getAfterCreateOn() {
        return afterCreateOn;
    }

    public void setAfterCreateOn(String afterCreateOn) {
        this.afterCreateOn = afterCreateOn;
    }

    public String getAfterFinishOn() {
        return afterFinishOn;
    }

    public void setAfterFinishOn(String afterFinishOn) {
        this.afterFinishOn = afterFinishOn;
    }

    public String getPaymentResultTime() {
        return paymentResultTime;
    }

    public void setPaymentResultTime(String paymentResultTime) {
        this.paymentResultTime = paymentResultTime;
    }

    public Integer getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Integer paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public int getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(int goodsCount) {
        this.goodsCount = goodsCount;
    }

    public double getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(double goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public String getGoodsTitle() {
        return goodsTitle;
    }

    public void setGoodsTitle(String goodsTitle) {
        this.goodsTitle = goodsTitle;
    }

    public String getGoodsImage() {
        return goodsImage;
    }

    public void setGoodsImage(String goodsImage) {
        this.goodsImage = goodsImage;
    }

    public String getGoodsColor() {
        return goodsColor;
    }

    public void setGoodsColor(String goodsColor) {
        this.goodsColor = goodsColor;
    }

    public String getGoodsSize() {
        return goodsSize;
    }

    public void setGoodsSize(String goodsSize) {
        this.goodsSize = goodsSize;
    }
}
