package com.b2c.entity.vo;

import com.b2c.entity.ErpStockInCheckoutEntity;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-29 15:31
 */
public class ErpStockInCheckoutFormDetailVo extends ErpStockInCheckoutEntity {
    private List<ErpStockInCheckoutItemVo> items;

    public List<ErpStockInCheckoutItemVo> getItems() {
        return items;
    }

    public void setItems(List<ErpStockInCheckoutItemVo> items) {
        this.items = items;
    }
}
