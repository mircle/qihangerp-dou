package com.b2c.entity.vo.order;

import com.b2c.entity.ErpOrderEntity;
import com.b2c.entity.ErpOrderItemEntity;

import java.util.List;

/**
 * 描述：
 * 待发货订单List
 *
 * @author qlp
 * @date 2019-06-20 15:46
 */
public class OrderWaitSendListVo extends ErpOrderEntity {
    private String shopName;
    private Long totalQuantity;

    public Long getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    private List<ErpOrderItemEntity> items;  //订单商品列表

    public List<ErpOrderItemEntity> getItems() {
        return items;
    }

    public void setItems(List<ErpOrderItemEntity> items) {
        this.items = items;
    }
}
