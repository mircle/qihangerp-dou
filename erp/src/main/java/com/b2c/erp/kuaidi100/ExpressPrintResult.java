package com.b2c.erp.kuaidi100;

/**
 * 描述：
 * 快递面单打印返回结果
 *
 * @author qlp
 * @date 2019-09-17 20:24
 */
public class ExpressPrintResult {
    private String kuaidicomName;//快递公司中文名
    private String kuaidicom;
    private String kuaidinum;
    private String imgBase64;

    public String getKuaidicomName() {
        return kuaidicomName;
    }

    public void setKuaidicomName(String kuaidicomName) {
        this.kuaidicomName = kuaidicomName;
    }

    public String getKuaidicom() {
        return kuaidicom;
    }

    public void setKuaidicom(String kuaidicom) {
        this.kuaidicom = kuaidicom;
    }

    public String getKuaidinum() {
        return kuaidinum;
    }

    public void setKuaidinum(String kuaidinum) {
        this.kuaidinum = kuaidinum;
    }

    public String getImgBase64() {
        return imgBase64;
    }

    public void setImgBase64(String imgBase64) {
        this.imgBase64 = imgBase64;
    }
}
