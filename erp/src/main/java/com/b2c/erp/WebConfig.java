package com.b2c.erp;


import com.b2c.erp.controller.api.AuthInterceptor;
import com.b2c.erp.interceptor.InitInterceptor;
import com.b2c.erp.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    private InitInterceptor initInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthInterceptor()).excludePathPatterns(Arrays.asList("/api/v1/manage_user_login"))
                        .addPathPatterns("/api/v1/**");

        registry.addInterceptor(new LoginInterceptor())
                .excludePathPatterns(
                        Arrays.asList("/css/**", "/js/**", "/login","/error", "/images/**", "/notify/**", "/layui/**","/oauth/**","/api/**")
                );
        registry.addInterceptor(initInterceptor)
                .excludePathPatterns(
                    Arrays.asList("/css/**", "/js/**", "/login","/error", "/images/**","/notify/**","/layui/**","/","/no_access","/sb/**","/api/**")
                );
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("*")
//                .allowedOrigins("*")
                .allowedOriginPatterns("*")
                .allowCredentials(true)
                .maxAge(3600);
    }
}
