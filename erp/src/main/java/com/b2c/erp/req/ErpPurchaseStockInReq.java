package com.b2c.erp.req;

import com.b2c.entity.vo.ErpPurchaseStockInItemVo;

import java.util.List;

/**
 * 描述：
 * 采购单入库req
 *
 * @author qlp
 * @date 2019-10-08 14:56
 */
public class ErpPurchaseStockInReq {
    private Long invoiceId;
    private List<ErpPurchaseStockInItemVo> items;

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public List<ErpPurchaseStockInItemVo> getItems() {
        return items;
    }

    public void setItems(List<ErpPurchaseStockInItemVo> items) {
        this.items = items;
    }
}
