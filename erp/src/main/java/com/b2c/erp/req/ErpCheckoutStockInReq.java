package com.b2c.erp.req;

import com.b2c.entity.vo.ErpCheckoutStockInItemVo;

import java.util.List;

/**
 * 描述：
 * 验货单入库req
 *
 * @author qlp
 * @date 2019-10-08 14:56
 */
public class ErpCheckoutStockInReq {
    private Long checkoutId;
    private List<ErpCheckoutStockInItemVo> items;

    public Long getCheckoutId() {
        return checkoutId;
    }

    public void setCheckoutId(Long checkoutId) {
        this.checkoutId = checkoutId;
    }

    public List<ErpCheckoutStockInItemVo> getItems() {
        return items;
    }

    public void setItems(List<ErpCheckoutStockInItemVo> items) {
        this.items = items;
    }
}
