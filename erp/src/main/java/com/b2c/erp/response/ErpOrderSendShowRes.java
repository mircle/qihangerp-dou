package com.b2c.erp.response;


import com.b2c.entity.ErpOrderEntity;
import com.b2c.entity.erp.vo.ErpOrderItemListVo;

import java.util.List;

/**
 * 描述：
 * 仓库系统订单发货显示RESPONSE
 *
 * @author qlp
 * @date 2019-06-21 18:24
 */
public class ErpOrderSendShowRes {
    private ErpOrderEntity order;
    private List<ErpOrderItemListVo> orderItems;

    public ErpOrderEntity getOrder() {
        return order;
    }

    public void setOrder(ErpOrderEntity order) {
        this.order = order;
    }

    public List<ErpOrderItemListVo> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<ErpOrderItemListVo> orderItems) {
        this.orderItems = orderItems;
    }
}
