package com.b2c.erp.response;

import com.b2c.entity.GoodsCategoryAttributeValueEntity;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-03-25 14:08
 */
public class SpecListResp {
//    private List<CategoryAttributeValueResp> color;
//    private List<CategoryAttributeValueResp> size;
//    private List<CategoryAttributeValueResp> style;
//
//    public List<CategoryAttributeValueResp> getStyle() {
//        return style;
//    }
//
//    public void setStyle(List<CategoryAttributeValueResp> style) {
//        this.style = style;
//    }
//
//    public List<CategoryAttributeValueResp> getColor() {
//        return color;
//    }
//
//    public void setColor(List<CategoryAttributeValueResp> color) {
//        this.color = color;
//    }
//
//    public List<CategoryAttributeValueResp> getSize() {
//        return size;
//    }
//
//    public void setSize(List<CategoryAttributeValueResp> size) {
//        this.size = size;
//    }
    private List<GoodsCategoryAttributeValueEntity> color;
    private List<GoodsCategoryAttributeValueEntity> size;
    private List<GoodsCategoryAttributeValueEntity> style;

    public List<GoodsCategoryAttributeValueEntity> getColor() {
        return color;
    }

    public void setColor(List<GoodsCategoryAttributeValueEntity> color) {
        this.color = color;
    }

    public List<GoodsCategoryAttributeValueEntity> getSize() {
        return size;
    }

    public void setSize(List<GoodsCategoryAttributeValueEntity> size) {
        this.size = size;
    }

    public List<GoodsCategoryAttributeValueEntity> getStyle() {
        return style;
    }

    public void setStyle(List<GoodsCategoryAttributeValueEntity> style) {
        this.style = style;
    }
}
