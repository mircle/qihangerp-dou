package com.b2c.erp.response;

import com.b2c.entity.erp.vo.GoodsSpecDetailVo;

/**
 * 描述：
 * 采购入库AJAX操作返回vo
 *
 * @author qlp
 * @date 2019-09-25 15:20
 */
public class PurchaseStockInAjaxResponse extends GoodsSpecDetailVo {
    private Long invoiceInfoId;//表单明细id
    private Long invoiceId;//表单id
    private Long inQuantity;//已入库数量
    private Long qualifiedQuantity;//合格数量

    public Long getInQuantity() {
        return inQuantity;
    }

    public void setInQuantity(Long inQuantity) {
        this.inQuantity = inQuantity;
    }

    public Long getQualifiedQuantity() {
        return qualifiedQuantity;
    }

    public void setQualifiedQuantity(Long qualifiedQuantity) {
        this.qualifiedQuantity = qualifiedQuantity;
    }

    public Long getInvoiceInfoId() {
        return invoiceInfoId;
    }

    public void setInvoiceInfoId(Long invoiceInfoId) {
        this.invoiceInfoId = invoiceInfoId;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }
}
