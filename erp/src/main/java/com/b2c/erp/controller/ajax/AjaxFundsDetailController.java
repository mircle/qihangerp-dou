package com.b2c.erp.controller.ajax;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.DataRow;
import com.b2c.entity.FundsDetailEntity;
import com.b2c.interfaces.funds.FundsDetailService;

@RestController
@RequestMapping(value = "/ajax_funds")
public class AjaxFundsDetailController {
    @Autowired
    private FundsDetailService fundsDetailService;
    
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ApiResult<Integer> getOrderByLogisticsCodeOrorderNum(@RequestBody DataRow req) {
        String createDate = req.getString("createDate");
        Integer shopId = req.getInt("shopId");
        Integer type = req.getInt("type");
        String source = req.getString("source");
        Double amount = req.getDouble("amount");
        String sourceNo = req.getString("sourceNo");
        String remark = req.getString("remark");
        FundsDetailEntity fEntity = new FundsDetailEntity();
        fEntity.setCreateDate(createDate);
        fEntity.setShopId(shopId);
        fEntity.setType(type);
        fEntity.setSource(source);
        fEntity.setAmount(amount);
        fEntity.setSourceNo(sourceNo);
        fEntity.setRemark(remark);

        var res = fundsDetailService.create(fEntity);
        return new ApiResult<>(0, "SUCCESS");
    }
}
