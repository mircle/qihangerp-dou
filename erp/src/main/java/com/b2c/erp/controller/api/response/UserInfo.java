package com.b2c.erp.controller.api.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class UserInfo{
    private String username;
    private String name;
    private String avatar;
}