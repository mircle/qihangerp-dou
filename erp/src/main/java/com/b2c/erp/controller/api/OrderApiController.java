package com.b2c.erp.controller.api;

import com.alibaba.fastjson2.JSONObject;
import com.b2c.entity.api.ApiResult;
import com.b2c.entity.api.ApiResultEnum;
import com.b2c.erp.controller.api.req.OrderSubmitReq;
import com.b2c.erp.controller.api.vo.GoodsDetailVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class OrderApiController {
    @RequestMapping(value = "/order_submit", method = RequestMethod.POST)
    public ApiResult<String> orderSubmit(@RequestBody OrderSubmitReq req) {

        /**
         *
         */
        String consignee = "";//收件人
        String mobile = "";//联系电话
        String province = "";
        String city = "";
        String area = "";
        String address = "";
        if(req.getShopId() == 6) {
            try {
                String[] info = req.getConsignee().split(" ");
                consignee = info[0];
                mobile = info[1];
                province = info[2];
                city = info[3];
                area = info[4];
                address = info[5];
            }catch (Exception e){
                return new ApiResult<>(ApiResultEnum.Fail,"收货地址格式不正确");
            }
        }else if(req.getShopId() == 5) {
            try {
                String[] info = req.getConsignee().split("\n");
                consignee = info[0];
                mobile = info[1];
                address = info[2];
                String[] s = info[2].split(" ");
                province = s[0];
                city = s[1];
                area = s[2];
            } catch (Exception e) {
                return new ApiResult<>(ApiResultEnum.Fail, "收货地址格式不正确");
            }
        }else if(req.getShopId() == 22) {
            return new ApiResult<>(ApiResultEnum.SystemException, "未实现");
        }
        log.info("收到新订单"+ JSONObject.toJSONString(req));
        return new ApiResult<>(ApiResultEnum.SUCCESS);
    }
}
