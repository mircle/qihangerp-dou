package com.b2c.erp.controller.ajax;

import com.b2c.interfaces.erp.ErpUserActionLogService;
import com.b2c.interfaces.wms.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 仓库商品规格异步管理
 * pbd add 2019/10/8 16:14
 */
@RestController
@RequestMapping(value = "/ajax_erp_good_spec")
public class AjaxWmsErpGoodSpecController {
    @Autowired
    private StockService stockService;
    @Autowired
    private ErpUserActionLogService logService;
    /**
     * 修改商品规格库存信息
     * @param model
     * @param req
     * @return
     */
//    @RequestMapping(value = "/upd_erp_good_spec_by_id", method = RequestMethod.POST)
//    public ApiResult<Integer> updErpGoodSpecById(@RequestBody DataRow model, HttpServletRequest req) {
//        var resultVo= stockService.updErpGoodSpecById(model.getInt("oldSpecId"),model.getInt("newSpecId"));
//        //添加系统日志
//        logService.addUserAction(Integer.parseInt(req.getSession().getAttribute("userId").toString()), EnumUserActionType.Modify,
//                "/ajax_erp_good_spec/upd_erp_good_spec_by_id","修改商品规格库存信息 ,变更前 : "+model.getInt("oldSpecId")+" ,变更后 : "+model.getInt("newSpecId")+" ,日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"成功");
//
//        return new ApiResult<>(resultVo.getCode(),resultVo.getMsg());
//    }
}
