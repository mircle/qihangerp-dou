package com.b2c.erp.controller.api.req;

import lombok.Data;

@Data
public class OrderSubmitReq {
    private Integer goodsId;
    private Integer skuId;
    private int num;

    private int shopId;//店铺id

    private String consignee;//收件人
    private String mobile;//联系电话

    private String address;//详细地址
}
