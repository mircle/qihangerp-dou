package com.b2c.erp.controller.funds;

import com.b2c.entity.funds.OrderSettlementPddEntity;
import com.b2c.entity.pdd.PddMarketingFeeEntity;

import java.util.List;

public class MarketingCostPddImportSubmitReq {
    private Integer shopId;

    private List<PddMarketingFeeEntity> orderList;//导入的订单

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public List<PddMarketingFeeEntity> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<PddMarketingFeeEntity> orderList) {
        this.orderList = orderList;
    }

    
}
