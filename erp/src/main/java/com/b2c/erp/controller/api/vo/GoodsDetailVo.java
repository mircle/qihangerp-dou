package com.b2c.erp.controller.api.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class GoodsDetailVo implements Serializable {
    private List<GoodsSkuAttrVo> skuAttr;//规格属性
    private List<GoodsDetailSkuListVo> skuList;//sku列表

    private Integer goodsId;
    private String picture;
    private String price;
    private Integer stockNum;


}
