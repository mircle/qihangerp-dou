$(function () {
    var _this = null;
    var _el = null;
    var _timer = null;
    var _menu = $('.hy-menu ul');
    var _menus = _menu.find('li');
    _menus.each(function (i, el) {
        if ($(el).hasClass('active')) {
            _el = $(el);
            return false;
        }
    });
    _menu.hover(function () {
        if (_el[0] === _this[0]) return;
        _el.removeClass('active');
    }, function () {
        if (_el[0] === _this[0]) return;
        _el.addClass('active');
    });
    _menus.hover(function () {
        _this = $(this);
        _this.siblings().removeClass('active');
        _this.addClass('active');
        _timer && clearTimeout(_timer);
    }, function () {
        _this = $(this);
        _this.removeClass('active');
        _timer = setTimeout(function () {
            _el.addClass('active');
        }, 300);
    });
});