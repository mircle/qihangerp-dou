package com.b2c.provider;


import com.b2c.entity.result.PagingResp;
import com.b2c.entity.test.OrderPddTEntity;
import com.b2c.interfaces.test.OrderPddTService;
import org.apache.dubbo.config.annotation.DubboService;

import javax.annotation.Resource;
import java.math.BigDecimal;


@DubboService
public class OrderPddTProviderImpl implements OrderPddTService {
    @Resource
    private  OrderPddTRepository orderPddRepository;
    @Override
    public PagingResp<OrderPddTEntity> getOrderList(Integer pageIndex, Integer pageSize, Integer shopId, Integer orderStatus, Integer refundStatus, String orderSn, String goodsId, String trackNum) {
        return orderPddRepository.getOrderList(pageIndex, pageSize, shopId, orderStatus, refundStatus, orderSn, goodsId, trackNum);
//        return  null;
    }

    @Override
    public OrderPddTEntity getOrder(Long orderId) {
        OrderPddTEntity entity = new OrderPddTEntity();
        entity.setId(10000L);
        entity.setOrderSn("SN2003898");
        entity.setAmout(new BigDecimal(220.00));
        return entity;
    }
}
