package com.b2c.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import com.b2c.entity.BaseAreaEntity;

/**
 * 描述：
 * 基础地区表
 *
 * @author qlp
 * @date 2019-01-30 13:51
 */
@Repository
public class BaseAreaRepository {
    @Autowired
    protected JdbcTemplate jdbcTemplate;

    /**
     * 根据地区code查询子类
     *
     * @param parentCode
     * @return
     */
    public List<BaseAreaEntity> getAreaListByParent(String parentCode) {
        List<BaseAreaEntity> list = new ArrayList<>();
        String sql = "SELECT * FROM " + Tables.BaseArea + " WHERE parent_code=? AND status=1 ORDER BY sibling_sort DESC";
        list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(BaseAreaEntity.class), parentCode);
        return list;
    }

}
