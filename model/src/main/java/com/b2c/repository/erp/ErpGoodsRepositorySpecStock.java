package com.b2c.repository.erp;

public class ErpGoodsRepositorySpecStock {

    private int currentQty;
    private int lockedQty;

    public int getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(int currentQty) {
        this.currentQty = currentQty;
    }

    public int getLockedQty() {
        return lockedQty;
    }

    public void setLockedQty(int lockedQty) {
        this.lockedQty = lockedQty;
    }
}
