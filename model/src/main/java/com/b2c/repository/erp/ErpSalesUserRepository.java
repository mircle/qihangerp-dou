package com.b2c.repository.erp;

import com.b2c.entity.UserEntity;
import com.b2c.repository.Tables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户管理
 */
@Repository
public class ErpSalesUserRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    /**
     * 查询业务员列表
     * @return
     */
    public List<UserEntity> getDeveloperUserList(){
        return jdbcTemplate.query("select * from "+Tables.User +" where type=2 and is_developer=1 and state=1",new BeanPropertyRowMapper<>(UserEntity.class));
    }

    /**
     * 查询客户列表
     * @return
     */
    public List<UserEntity> getUserList(){
        String sql = "SELECT u.id,IFNULL(u.user_name,u.nick_name) as userName,u.mobile,IFNULL(d.user_name,d.nick_name) as developerName FROM " + Tables.User + " as u " +
                " LEFT JOIN " +Tables.User+" as d on d.id = u.developer_id "+
                " WHERE u.type=3 and u.state=1 ";
        return jdbcTemplate.query(sql,new BeanPropertyRowMapper<>(UserEntity.class));
    }
    /**
     * 根据业务员id查询客户列表
     * @param developerId
     * @return
     */
    public List<UserEntity> getUserListByDeveloperId(Integer developerId){
        return jdbcTemplate.query("select * from "+Tables.User +" where developer_id=? and state=1",new BeanPropertyRowMapper<>(UserEntity.class),developerId);
    }
}
