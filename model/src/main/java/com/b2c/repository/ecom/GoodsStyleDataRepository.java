package com.b2c.repository.ecom;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ecom.GoodsImgDataEntity;
import com.b2c.entity.ecom.GoodsStyleDataEntity;

@Repository
public class GoodsStyleDataRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Transactional
    public PagingResponse<GoodsStyleDataEntity> getList(Integer pageIndex, Integer pageSize, String num,
            String platform) {
                StringBuilder sb = new StringBuilder();
                sb.append("SELECT SQL_CALC_FOUND_ROWS kw.*");
               
                sb.append(" FROM ecom_goods_style_data as kw ");
                sb.append(" where kw.isDelete=0 ");
        
                List<Object> params = new ArrayList<>();
                if (StringUtils.isEmpty(num) == false) {
                    sb.append(" AND (kw.goodsId = ? OR kw.goodsNum = ? ) ");
                    params.add(num);
                    params.add(num);
                }
                if (StringUtils.isEmpty(platform) == false) {
                    sb.append(" AND kw.platform = ?");
                    params.add(platform);
                }
               
                
        
                sb.append(" ORDER BY kw.id DESC LIMIT ?,? ");
                params.add((pageIndex - 1) * pageSize);
                params.add(pageSize);
        
                List<GoodsStyleDataEntity> list = jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(GoodsStyleDataEntity.class), params.toArray(new Object[params.size()]));
                Integer totalSize = getTotalSize();
                return new PagingResponse<>(pageIndex, pageSize, totalSize, list);
    }

    protected int getTotalSize() {
        return jdbcTemplate.queryForObject("SELECT FOUND_ROWS() as row_num;", int.class);
    }

    public void add(GoodsStyleDataEntity entity) {
        String sql ="INSERT INTO ecom_goods_style_data (goodsId,goodsNum,img,platform,startDate,endDate,ctr,cvr,remark,collects,cats,orders) VALUE (?,?,?,?,?,?,?,?,?,?,?,?)";
        jdbcTemplate.update(sql, entity.getGoodsId(),entity.getGoodsNum(),entity.getImg(),entity.getPlatform(),entity.getStartDate(),entity.getEndDate(),entity.getCtr(),entity.getCvr(),entity.getRemark()
        ,entity.getCollects(),entity.getCarts(),entity.getOrders()
        );
    }
}
