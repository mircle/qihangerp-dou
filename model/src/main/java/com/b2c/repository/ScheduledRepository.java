package com.b2c.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public class ScheduledRepository {
    private static Logger log = LoggerFactory.getLogger(ScheduledRepository.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void addWmsSalesData(String date){
        //总库存
        String  zkc= "SELECT SUM(currentQty) from erp_goods_stock_info WHERE isDelete=0";

        //采购入库
        String  cgrk= "SELECT IFNULL(SUM(ifi.quantity),0)  FROM erp_stock_in_form_item ifi LEFT JOIN erp_stock_in_form sif on ifi.formId=sif.id  WHERE sif.inType=1 AND from_unixtime(sif.stockInTime1,'%Y-%m-%d')=?";

        //销售出库
        String  xsck= "SELECT IFNULL(SUM(i.quantity),0) from erp_stock_out_form_item i LEFT JOIN erp_stock_out_form e ON i.formId=e.id  WHERE e.outType=1 and i.status=3 AND from_unixtime(e.completeTime,'%Y-%m-%d')=?";

        //退货入库
        String  thrk= "SELECT IFNULL(SUM(i.quantity),0) from erp_stock_in_form_item i LEFT JOIN erp_stock_in_form e ON i.formId=e.id  WHERE e.inType=2 AND from_unixtime(e.stockInTime1,'%Y-%m-%d')=?";

        //采购退货
        String  cgth= "SELECT IFNULL(SUM(i.quantity),0) from erp_stock_out_form_item i LEFT JOIN erp_stock_out_form e ON i.formId=e.id  WHERE  e.outType=2 AND from_unixtime(e.completeTime,'%Y-%m-%d')=?";

        //报损统计
        String bstj="SELECT IFNULL(SUM(i.quantity),0)  from erp_goods_stock_loss_item i LEFT JOIN erp_goods_stock_loss_form s ON i.formId=s.id WHERE from_unixtime(s.createTime,'%Y-%m-%d')=?";

        //采购总价
        String  cgzj= "SELECT IFNULL(SUM(currentQty*purPrice),0)  from erp_goods_stock_info_item WHERE currentQty>0";

        Long zkcCount = jdbcTemplate.queryForObject(zkc,Long.class);
        Long cgrkCount = jdbcTemplate.queryForObject(cgrk,Long.class,date);
        Long xsckCount = jdbcTemplate.queryForObject(xsck,Long.class,date);
        Long thrkCount = jdbcTemplate.queryForObject(thrk,Long.class,date);
        Long cgthCount = jdbcTemplate.queryForObject(cgth,Long.class,date);
        Long bstjCount=jdbcTemplate.queryForObject(bstj,Long.class,date);
        BigDecimal zgzjCount = jdbcTemplate.queryForObject(cgzj, BigDecimal.class);

        jdbcTemplate.update("insert into  daily_report set zkc=?,cgrk=?,xsck=?,thrk=?,cgth=?,cgzj=?,bstj=?,tjdate=?",zkcCount,cgrkCount,xsckCount,thrkCount,cgthCount,zgzjCount,bstjCount,date);

    }
}
