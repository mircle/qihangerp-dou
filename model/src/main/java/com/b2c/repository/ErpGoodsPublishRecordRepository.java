package com.b2c.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpGoodsPublishRecordEntity;

@Repository
public class ErpGoodsPublishRecordRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    protected int getTotalSize() {
        return jdbcTemplate.queryForObject("SELECT FOUND_ROWS() as row_num;", int.class);
    }

    @Transactional
    public PagingResponse<ErpGoodsPublishRecordEntity> getList(int pageIndex, int pageSize,Integer shopId,String goodsNum) {
        String sql = "SELECT SQL_CALC_FOUND_ROWS * FROM erp_goods_publish_record WHERE 1=1 ";
        List<Object> params = new ArrayList<>();

        if (!StringUtils.isEmpty(goodsNum)) {
            sql += " and goodsNum = ? ";
            params.add(goodsNum);
        }
        if (shopId != null) {
            sql += " and find_in_set (?,shopIds) ";
            params.add(shopId);
        }

        sql += " order by id desc ";

        List<ErpGoodsPublishRecordEntity> list = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<>(ErpGoodsPublishRecordEntity.class), params.toArray(new Object[params.size()]));
        return new PagingResponse<>(pageIndex, pageSize, getTotalSize(), list);
     
    }

    public ErpGoodsPublishRecordEntity getEntityById(Integer id) {
        try {
            String sql = "SELECT  * FROM erp_goods_publish_record WHERE id=? ";
            return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(ErpGoodsPublishRecordEntity.class),id);
        } catch (Exception e) {
            return null;
        }
        
    }

    public void add(ErpGoodsPublishRecordEntity entity) {
        String sql = "INSERT INTO erp_goods_publish_record (erpGoodsId,goodsName,goodsNum,goodsImage,imagePath,sourceUrl,shopIds,publishDate,goodsCost) VALUE (?,?,?,?,?,?,?,?,?) ";
        jdbcTemplate.update(sql,entity.getErpGoodsId(),entity.getGoodsName(),entity.getGoodsNum(),entity.getGoodsImage(),entity.getImagePath(),entity.getSourceUrl(),entity.getShopIds(),entity.getPublishDate(),entity.getGoodsCost());   
    }

    public void edit(ErpGoodsPublishRecordEntity entity) {
        String sql = "UPDATE erp_goods_publish_record SET erpGoodsId=?,goodsName=?,goodsNum=?,goodsImage=?,imagePath=?,sourceUrl=?,shopIds=?,publishDate=?,goodsCost=? WHERE id=? ";
        jdbcTemplate.update(sql,entity.getErpGoodsId(),entity.getGoodsName(),entity.getGoodsNum(),entity.getGoodsImage(),entity.getImagePath(),entity.getSourceUrl(),entity.getShopIds(),entity.getPublishDate(),entity.getGoodsCost(),entity.getId());   
    }

    public void publishToShop(Integer goodsSupplierId,String shopIds,String publishDate){
        try {
            //查询
            String sql = "SELECT * FROM erp_goods_publish_record where goodsSupplierId=? LIMIT 1";
            var entity = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(ErpGoodsPublishRecordEntity.class), goodsSupplierId);
            //存在修改
            jdbcTemplate.update("UPDATE erp_goods_publish_record SET shopIds=?,publishDate=? WHERE goodsSupplierId=?",shopIds,publishDate,goodsSupplierId);
        }catch (Exception e) {
            //不存在添加
            String sql1 = "INSERT INTO erp_goods_publish_record (goodsSupplierId,shopIds,publishDate) VALUE (?,?,?) ";
            jdbcTemplate.update(sql1, goodsSupplierId, shopIds, publishDate);
        }
    }


}
