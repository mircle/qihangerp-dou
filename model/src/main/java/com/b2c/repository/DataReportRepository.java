package com.b2c.repository;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.DailyReportEntity;
import com.b2c.entity.ErpGoodsStockLogsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Repository
public class DataReportRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    protected int getTotalSize() {
        return jdbcTemplate.queryForObject("SELECT FOUND_ROWS() as row_num;", int.class);
    }

    /**
     * 分页查询统计数据
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public PagingResponse<DailyReportEntity> dailyReportPageList(int pageIndex, int pageSize, String startDate, String endDate){
        List<Object> params = new ArrayList<>();
        StringBuilder sb = new StringBuilder("SELECT SQL_CALC_FOUND_ROWS * ");
        sb.append("from daily_report  where 1=1 ");
        if (!StringUtils.isEmpty(startDate)) {
            sb.append(" AND tjdate > = ");
            params.add(startDate);
        }
        if (!StringUtils.isEmpty(endDate)) {
            sb.append(" AND tjdate <= ");
            params.add(endDate);
        }
        sb.append(" ORDER BY tjdate desc LIMIT ?,?");
        params.add((pageIndex - 1) * pageSize);
        params.add(pageSize);
        List<DailyReportEntity> userList = jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(DailyReportEntity.class), params.toArray(new Object[params.size()]));
        return new PagingResponse<>(pageIndex, pageSize, getTotalSize(), userList);
    }

    public void dd(){
        var list = jdbcTemplate.query("select * from erp_goods_stock_logs where  sourceType=10 GROUP BY specNumber ",new BeanPropertyRowMapper<>(ErpGoodsStockLogsEntity.class));
        for(var s:list){
            var obj = jdbcTemplate.query("select * from erp_goods_stock_logs where  specNumber=? and id<? order by id desc limit 1 ",new BeanPropertyRowMapper<>(ErpGoodsStockLogsEntity.class),s.getSpecNumber(),s.getId());


        }
    }

}
