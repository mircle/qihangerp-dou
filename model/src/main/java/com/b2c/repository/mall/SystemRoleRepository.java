package com.b2c.repository.mall;


import com.b2c.repository.Tables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


/**
 * @Description:菜单权限管理 pbd add 2019/3/19 10:30
 */
@Repository
public class SystemRoleRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    protected int getTotalSize() {
        return jdbcTemplate.queryForObject("SELECT FOUND_ROWS() as row_num;", int.class);
    }


    /**
     * 添加管理员登录日志
     *
     * @param userId
     * @param loginIp
     */
    public void addManageUserLogin(Integer userId, String loginIp) {
        jdbcTemplate.update("insert into "+ Tables.SysManageLoginLog +" set user_id=?,login_ip=?,login_time=unix_timestamp(now())", userId, loginIp);
    }



}
