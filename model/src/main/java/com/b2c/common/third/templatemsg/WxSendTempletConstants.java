package com.b2c.common.third.templatemsg;

/**
 * 描述：模板消息之模板id
 *
 * @author pbd
 * @date 2019-04-02 12:17 PM
 */
public class WxSendTempletConstants {
    /*    *//**
     * 支付成功
     *//*
    public static final  String ZFCG = "MPsOQRC3CexqAsdtq9nFkho3UkQt9k1RAoV73ji4zcU";
    *//**
     * 退款成功
     *//*
    public static final  String TKCG="9ivsr5oCuUD-jYh8U6BvKduxXq2h-i6QjrSDIzjQODU";
    *//**
     * 订单发货
     *//*
    public static final String DDFH="0w4t0t_YEf_00rfKFA-tjHxtd8WeZ02aJkqbRAHQc9A";
    *//**
     * 订单返现
     *//*
    public static final String DDFX="6RLBFov3LscsawebK7kWIrCndyZpl_Xv1a8kU3hXwGo";*/


    /*华衣云购模板ID*/
    /**
     * 支付成功
     */
    public static final String ZFCG = "Aubjd8PywQLtQ1Ae7Cba2YbGpkgLnMM3UG-XdMOzIv0";
    /**
     * 退款成功
     */
    public static final String TKCG = "DgIkaMXzcup_hh4Kl2yyBTqyeN45dG5B5KKDqr21oBA";
    /**
     * 订单发货
     */
    public static final String DDFH = "pCvy9jXPsdqUHBNdJkKjZr9o6TFRddcJrNaJx-yT4oA";
    /**
     * 订单返现
     */
    public static final String DDFX = "13Z94hqrnT6XYE7ZGcBiHqNxh-KD7A1ovXAYHlnEYIg";


}
