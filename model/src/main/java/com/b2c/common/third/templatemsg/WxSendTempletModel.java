package com.b2c.common.third.templatemsg;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Description: 微信模板消息对象
 * pbd add 2019/4/2 9:50
 */
public class WxSendTempletModel {
    private Map<String, Object> map;

    private Map<String, Object> data;

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    /**
     * 支付成功
     *
     * @param params
     */
    public void sendTempletZfcg(Object... params) {
        map = new LinkedHashMap<>();

        data = new LinkedHashMap<>();

        LinkedHashMap<String, Object> first = new LinkedHashMap<>();

        first.put("value", "您好！您的订单已成功支付！");

        first.put("color", "#173177");

        data.put("first", first);

        LinkedHashMap<String, Object> keyword1 = new LinkedHashMap<>();

        keyword1.put("value", params[3]);

        keyword1.put("color", "#173177");

        data.put("orderMoneySum", keyword1);

        LinkedHashMap<String, Object> keyword2 = new LinkedHashMap<>();

        keyword2.put("value", params[4]);

        keyword2.put("color", "#173177");

        data.put("orderProductName", keyword2);

        LinkedHashMap<String, Object> remark = new LinkedHashMap<>();

        remark.put("value", "如有问题请致电400-828-1878或直接在微信留言，我们将第一时间为您服务！");

        remark.put("color", "#173177");

        data.put("Remark", remark);

        map.put("touser", params[0]);

        map.put("template_id", params[1]);

        map.put("url", params[2]);

        map.put("topcolor", "#F7C709");

        map.put("data", data);
    }

    /**
     * 退款成功
     *
     * @param params
     */
    public void sendTempletTkcg(Object... params) {
        map = new LinkedHashMap<>();

        data = new LinkedHashMap<>();

        LinkedHashMap<String, Object> first = new LinkedHashMap<>();

        first.put("value", "您好！您的订单退款已到帐！");

        first.put("color", "#173177");

        data.put("first", first);

        LinkedHashMap<String, Object> keyword1 = new LinkedHashMap<>();

        keyword1.put("value", params[3]);

        keyword1.put("color", "#173177");

        data.put("reason", keyword1);

        LinkedHashMap<String, Object> keyword2 = new LinkedHashMap<>();

        keyword2.put("value", params[4]);

        keyword2.put("color", "#173177");

        data.put("refund", keyword2);

        LinkedHashMap<String, Object> remark = new LinkedHashMap<>();

        remark.put("value", "如有问题请致电400-828-1878或直接在微信留言，我们将第一时间为您服务！");

        remark.put("color", "#173177");

        data.put("Remark", remark);

        map.put("touser", params[0]);

        map.put("template_id", params[1]);

        map.put("url", params[2]);

        map.put("topcolor", "#F7C709");

        map.put("data", data);
    }

    /**
     * 订单发货
     *
     * @param params
     */
    public void sendTempletDdfh(Object... params) {
        map = new LinkedHashMap<>();

        data = new LinkedHashMap<>();

        LinkedHashMap<String, Object> first = new LinkedHashMap<>();

        first.put("value", "您好！您的订单已发货！");

        first.put("color", "#173177");

        data.put("first", first);

        LinkedHashMap<String, Object> keyword1 = new LinkedHashMap<>();

        keyword1.put("value", params[3]);

        keyword1.put("color", "#173177");

        data.put("delivername", keyword1);

        LinkedHashMap<String, Object> keyword2 = new LinkedHashMap<>();

        keyword2.put("value", params[4]);

        keyword2.put("color", "#173177");

        data.put("ordername", keyword2);

        LinkedHashMap<String, Object> remark = new LinkedHashMap<>();

        remark.put("value", "如有问题请致电400-828-1878或直接在微信留言，我们将第一时间为您服务！");

        remark.put("color", "#173177");

        data.put("Remark", remark);

        map.put("touser", params[0]);

        map.put("template_id", params[1]);

        map.put("url", params[2]);

        map.put("topcolor", "#F7C709");

        map.put("data", data);
    }

    /**
     * 订单返现
     *
     * @param params
     */
    public void sendTempletDdfx(Object... params) {
        map = new LinkedHashMap<>();

        data = new LinkedHashMap<>();

        LinkedHashMap<String, Object> first = new LinkedHashMap<>();

        first.put("value", "您好！您的订单推广已经到帐！");

        first.put("color", "#173177");

        data.put("first", first);

        LinkedHashMap<String, Object> keyword1 = new LinkedHashMap<>();

        keyword1.put("value", params[3]);

        keyword1.put("color", "#173177");

        data.put("order", keyword1);

        LinkedHashMap<String, Object> keyword2 = new LinkedHashMap<>();

        keyword2.put("value", params[4]);

        keyword2.put("color", "#173177");

        data.put("money", keyword2);

        LinkedHashMap<String, Object> remark = new LinkedHashMap<>();

        remark.put("value", "如有问题请致电400-828-1878或直接在微信留言，我们将第一时间为您服务！");

        remark.put("color", "#173177");

        data.put("Remark", remark);

        map.put("touser", params[0]);

        map.put("template_id", params[1]);

        map.put("url", params[2]);

        map.put("topcolor", "#F7C709");

        map.put("data", data);
    }
}
