package com.b2c.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.List;

/**
 * @Description: json工具类
 * pbd add 2019/1/8 14:06
 */
public class JsonUtil {
    /**
     * 对象转换为json字符串
     *
     * @param obj 请求数据类
     * @return json字符串
     */
    public static String objToString(Object obj) {
        String jsonStr = JSON.toJSONString(obj);
        return jsonStr;
    }

    /**
     * 将JSON字符串转换对象
     *
     * @param jsonStr  JSON字符串
     * @return 数据类结果
     */
    public static <T> T strToObject(String jsonStr, Class<T> valueType) {
        JSON json = JSON.parseObject(jsonStr);
        return JSON.toJavaObject(json, valueType);
    }

    /**
     * json字符串转json数组
     *
     * @param jsonStr
     * @return
     */
    public static JSONArray strToArray(String jsonStr) {
        return JSONArray.parseArray(jsonStr);
    }

    /**
     * json字符转list
     *
     * @param jsonStr
     * @param objClass
     * @return
     */
    public static <T> List<T> strToList(String jsonStr, Class objClass) {
        return JSONObject.parseArray(jsonStr, objClass);
    }
}
