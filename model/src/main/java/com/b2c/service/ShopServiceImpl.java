package com.b2c.service;

import com.b2c.common.utils.DateUtil;
import com.b2c.entity.pdd.ShopDataReportModel;
import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.entity.datacenter.DcSysThirdSettingEntity;
import com.b2c.entity.vo.ShopOrderStatisticsModel;
import com.b2c.entity.vo.ShopOrderStatisticsVo;
import com.b2c.interfaces.ShopService;
import com.b2c.repository.ShopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ShopServiceImpl implements ShopService {
    @Autowired
    private ShopRepository repository;

    @Override
    public List<DcSysThirdSettingEntity> getDcList() {
        return repository.getDcList();
    }

    @Override
    public Integer addDcShop(DcShopEntity entity) {
        entity.setModifyOn(System.currentTimeMillis() / 1000);
        return repository.addDcShop(entity);
    }

    @Override
    public void delDcShop(DcShopEntity entity) {
        repository.delDcShop(entity);
    }

    @Override
    public PagingResponse<DcShopEntity> getDcShopList(Integer pageIndex, Integer pageSize, Integer type) {
        return repository.getDcShopList(pageIndex, pageSize, type);
    }

    @Override
    public List<DcShopEntity> getShopList(Integer type) {
        return repository.getShopList(type);
    }

    @Override
    public List<DcShopEntity> shopListShow() {
        return repository.shopListShow();
    }

    @Override
    public List<DcShopEntity> getShopListByUserId(Integer userId,Integer type) {
        return repository.getShopListByUserId(userId,type);
    }

    @Override
    public DcShopEntity getShop(Integer id) {
        return repository.getShop(id);
    }

    @Override
    public DcShopEntity getShopByMallId(Long mallId) {
        return repository.getShopByMallId(mallId);
    }

    @Override
    public void updateSessionKey(Integer shopId,Long mallId, String sessionKey) {
        repository.updateSessionKey(shopId,mallId, sessionKey);
    }

    @Override
    public ShopOrderStatisticsVo shopOrderStatistics(Integer shopId) {
        return repository.shopOrderStatistics(shopId);
    }

    @Override
    public ShopOrderStatisticsModel shopOrderStatisticsPDD(Integer shopId) {
        return repository.shopOrderStatisticsPDD(shopId);
    }

    @Override
    public List<ShopDataReportModel> getShopDateReport(String startDate, String endDate) {
        List<ShopDataReportModel> dataList = new ArrayList<>();
        dataList.add(repository.getShopDateReport(startDate));
        String currDate = DateUtil.getSpecifiedDayAfter(startDate);
        while (currDate.equals(endDate)==false){
            dataList.add(repository.getShopDateReport(currDate));
            currDate = DateUtil.getSpecifiedDayAfter(currDate);
        }
        dataList.add(repository.getShopDateReport(endDate));
        return dataList;
    }
}
