package com.b2c.service.erp;

import com.b2c.common.utils.StringUtil;
import com.b2c.entity.enums.EnumUserActionType;
import com.b2c.repository.erp.ErpUserActionLogRepository;
import com.b2c.interfaces.erp.ErpUserActionLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-24 09:17
 */
@Service
public class ErpUserActionLogServiceImpl implements ErpUserActionLogService {
    @Autowired
    private ErpUserActionLogRepository logRepository;
    @Override
    public void addUserAction(Integer userId, EnumUserActionType actionType, String actionUrl, String actionContent, String actionResult) {
        String ip = StringUtil.getLoalhostIP();
        logRepository.addUserAction(userId,actionType,actionUrl,ip,actionContent,actionResult);
    }
}
