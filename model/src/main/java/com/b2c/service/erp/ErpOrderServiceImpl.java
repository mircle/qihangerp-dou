package com.b2c.service.erp;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpOrderItemEntity;
import com.b2c.entity.apitao.OrderSendQuery;
import com.b2c.entity.ErpOrderEntity;
import com.b2c.entity.erp.vo.FaHuoDaiJianHuoGoodsVo;
import com.b2c.entity.erp.vo.ErpOrderItemListVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.vo.order.DouYinOrderCountVo;
import com.b2c.repository.erp.ErpOrderRepository;
import com.b2c.interfaces.erp.ErpOrderService;
import com.b2c.entity.enums.EnumErpOrderSendStatus;
import com.b2c.entity.vo.OrderScanCodeVo;
import com.b2c.entity.vo.order.OrderWaitSendListVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-17 15:01
 */
@Service
public class ErpOrderServiceImpl implements ErpOrderService {
    @Autowired
    private ErpOrderRepository orderRepository;

    @Override
    public PagingResponse<OrderWaitSendListVo> getWaitSendOrderList(Integer pageIndex, Integer pageSize, EnumErpOrderSendStatus sendStatus, String orderNum, String mobile, String logisticsCode, Integer startTime, Integer endTime,Integer saleType,Integer shopId) {
        if (pageIndex == null || pageIndex.intValue() <= 0) pageIndex = 1;
        if (pageSize == null || pageSize.intValue() <= 0) pageSize = 20;
        return orderRepository.getWaitSendOrderList(pageIndex, pageSize, sendStatus, orderNum, mobile, logisticsCode, startTime, endTime,saleType,shopId);
    }

    @Override
    public List<ErpOrderItemListVo> orderWaitSendListExport(Integer startDate, Integer endDate) {
        return orderRepository.orderWaitSendListExport(startDate,endDate);
    }

    @Override
    public PagingResponse<OrderWaitSendListVo> getNewWaitSendOrderList(OrderSendQuery query) {
        return orderRepository.getNewWaitSendOrderList(query);
    }


    @Override
    public List<ErpOrderItemListVo> getErporderItemSendList(Integer startTime, Integer endTime) {
        return orderRepository.getErporderItemSendList(startTime,endTime);
    }

    @Override
    public ErpOrderEntity getOrderEntityById(Long id) {
        return orderRepository.getOrderEntityById(id);
    }

    @Override
    public List<ErpOrderEntity> getOrderEntityByIds(String ids) {
        return orderRepository.getOrderEntityByIds(ids);
    }

    @Override
    public ErpOrderEntity getOrderEntityByNum(String num) {
        return orderRepository.getOrderEntityByNum(num);
    }

    @Override
    public List<ErpOrderItemListVo> getErpOrderItemsByOrderId(Long orderId) {
        return orderRepository.getErpOrderItemsByOrderId(orderId);
    }




    @Override
    public ResultVo<Long> generatingPickingListByOrderItem(List<Long> orderItemIdList) {
        return orderRepository.generatingPickingListByOrderItem(orderItemIdList);
    }


    @Override
    public ResultVo<Long> generatingPickingListByOrder(List<Long> orderIdList) {
        return orderRepository.generatingPickingListByOrder(orderIdList);
    }

    @Override
    public ResultVo<Integer> sendOrder(Long orderId) {
        return orderRepository.sendOrder(orderId);
    }

    @Override
    public ResultVo<Integer> orderHandExpress(Integer erpOrderId, String company, String companyCode, String code) {
        return orderRepository.orderHandExpress(erpOrderId, company, companyCode, code);
    }



    @Override
    public ResultVo<Integer> cancelOrderConfirm(List<Long> erpOrderIdArray) {
        return orderRepository.cancelOrderConfirm(erpOrderIdArray);
    }

    @Override
    public List<FaHuoDaiJianHuoGoodsVo> getDaiJianHuoOrderItemGoodsListAndDistinctSku(List<Long> orderIdList) {
        return orderRepository.getDaiJianHuoOrderItemGoodsListAndDistinctSku(orderIdList);
    }

    @Override
    public PagingResponse<ErpOrderItemListVo> getOrderItemList(Integer pageIndex, Integer pageSize, String orderNum, String skuNumber, Integer startTime, Integer endTime,Integer orderStatus) {
        return orderRepository.getOrderItemList( pageIndex,  pageSize,orderNum,skuNumber,startTime,endTime,orderStatus);
    }

    @Override
    public List<ErpOrderItemListVo> getOrderItemListForExcel(String orderNum, String skuNumber, Integer startTime, Integer endTime) {
        return orderRepository.getOrderItemListForExcel(orderNum,skuNumber,startTime,endTime);
    }

    @Override
    public void updErpOrderReturnCode(Long id, String company, String expressCode) {
        orderRepository.updErpOrderReturnCode(id,company,expressCode);
    }

    @Override
    public List<OrderScanCodeVo> getOrderAndItemsByLogisticsCodeOrorderNum(String num) {
        return orderRepository.getOrderAndItemsByLogisticsCodeOrorderNum(num);
    }

    @Override
    public List<OrderScanCodeVo> getOrderAndItemsByLogisticsCodeOrordeId(List orderIdArr) {
        return orderRepository.getOrderAndItemsByLogisticsCodeOrordeId(orderIdArr);
    }

    @Override
    public OrderWaitSendListVo getOrderAndItemsByOrderId(Long orderId) {
        return orderRepository.getOrderAndItemsByOrderId(orderId);
    }

    @Override
    public ResultVo<Integer> addErpOrderReturnItem(Long id, String specNumber, Integer num) {
        return orderRepository.addErpOrderReturnItem(id,specNumber,num);
    }

    @Override
    public List<DouYinOrderCountVo> getDouYinOrderCount(String strDate, String endDate) {
        return orderRepository.getDouYinOrderCount(strDate,endDate);
    }

    @Override
    public List<ErpOrderItemEntity> douyinOrderSettleExport(String startDate, String endDate) {
        return orderRepository.douyinOrderSettleExport(startDate,endDate);
    }

    @Override
    public void douyinOrderSettle(List<Long> ids, Integer isSettle) {
        orderRepository.douyinOrderSettle(ids,isSettle);
    }
}
