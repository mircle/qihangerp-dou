// package com.b2c.service;

// import com.alibaba.fastjson.JSONArray;
// import com.b2c.entity.result.PagingResponse;
// import com.b2c.mall.entity.MallGoodsEntity;
// import com.b2c.mall.entity.MallGoodsVo;
// import com.b2c.entity.vo.GoodsAddVo;
// import com.b2c.repository.MallGoodsRepository;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Service;

// @Service
// public class MallGoodsServiceImpl implements MallGoodsService {
//     @Autowired
//     private MallGoodsRepository repository;

//     @Override
//     public PagingResponse<MallGoodsEntity> getGoodsList(Integer pageIndex, Integer pageSize, String title, String number) {
//         return repository.getGoodsList(pageIndex,pageSize,title,number);
//     }

//     @Override
//     public Integer addGoods(GoodsAddVo addVo, String createBy) {
//         return repository.addGoods(addVo,createBy);
//     }


//     @Override
//     public MallGoodsVo getMallGoods(Long id) {
//         return repository.getMallGoods(id);
//     }

//     @Override
//     public void addMallGoods(JSONArray array) {
//         repository.addMallGoods(array);
//     }
// }
