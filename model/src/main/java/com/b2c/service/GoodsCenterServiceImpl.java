package com.b2c.service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.GoodsCenterEntity;
import com.b2c.repository.GoodsCenterRepository;
import com.b2c.interfaces.GoodsCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-11-08 17:32
 */
@Service
public class GoodsCenterServiceImpl implements GoodsCenterService {
    @Autowired
    private GoodsCenterRepository repository;

    @Override
    public PagingResponse<GoodsCenterEntity> getList(Integer pageIndex, Integer pageSize, String goodsNumber) {
        return repository.getList(pageIndex, pageSize, goodsNumber);
    }

}
