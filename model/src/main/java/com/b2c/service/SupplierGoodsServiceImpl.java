package com.b2c.service;

import com.b2c.entity.SupplierGoodsEntity;
import com.b2c.entity.result.PagingResponse;
import com.b2c.interfaces.SupplierGoodsService;
import com.b2c.repository.SupplierGoodsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SupplierGoodsServiceImpl implements SupplierGoodsService {
    @Autowired
    private SupplierGoodsRepository repository;
    @Override
    public SupplierGoodsEntity insert(SupplierGoodsEntity entity) {
         return repository.insert(entity);
    }

    @Override
    public PagingResponse<SupplierGoodsEntity> getList(Integer pageIndex, Integer pageSize, String number, Integer supplierId,Boolean publishStatus) {
        return repository.getList(pageIndex, pageSize, number, supplierId,publishStatus);
    }

    @Override
    public void updateAttr3(Integer id, String attr3) {
        repository.updateAttr3(id, attr3);
    }
}
