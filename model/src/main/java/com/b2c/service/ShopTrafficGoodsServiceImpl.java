package com.b2c.service;

import com.b2c.entity.tao.TrafficTaoModel;
import com.b2c.interfaces.ShopTrafficGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.shop.ShopTrafficEntity;
import com.b2c.entity.shop.ShopTrafficGoodsEntity;
import com.b2c.repository.ShopTrafficGoodsRepositroy;

import java.util.List;

@Service
public class ShopTrafficGoodsServiceImpl implements ShopTrafficGoodsService {
    @Autowired
    private ShopTrafficGoodsRepositroy repositroy;

    @Override
    public PagingResponse<ShopTrafficGoodsEntity> getGoodsTrafficList(Integer pageIndex, Integer pageSize, Integer shopType,
            Integer shopId, String goodsNum,String date) {

        return repositroy.getList(pageIndex, pageSize, shopType, shopId, goodsNum,date);
    }

    @Override
    public ResultVo<Integer> addGoodsTraffic(ShopTrafficGoodsEntity entity) {

        return repositroy.addGoodsTraffic(entity);
    }

    @Override
    public ResultVo<String> addGoodsTrafficTao(List<TrafficTaoModel> list, Integer shopId) {
        return repositroy.addGoodsTrafficTao(list,shopId);
    }

    @Override
    public PagingResponse<ShopTrafficEntity> getShopTrafficList(Integer pageIndex, Integer pageSize, Integer shopType,
            Integer shopId, String date) {
        
        return repositroy.getShopTrafficList(pageIndex, pageSize, shopType, shopId, date);
    }

    @Override
    public ResultVo<Integer> addShopTrafficForPdd(ShopTrafficEntity entity) {
  
        return repositroy.addShopTrafficForPdd(entity);
    }

    @Override
    public ResultVo<Integer> addShopTrafficForDy(ShopTrafficEntity entity) {
 
        return repositroy.addShopTrafficForDy(entity);
    }

    @Override
    public ShopTrafficEntity getById(Long id) {
      
        return repositroy.getById(id);
    }

    @Override
    public String getLastDay(int shopType,Integer shopId) {
        return repositroy.getLastDay(shopType, shopId);
    }

    @Override
    public ResultVo<Integer> editShopTrafficForPdd(ShopTrafficEntity entity) {

        return repositroy.editShopTrafficForPdd(entity);
    }

    @Override
    public ResultVo<Integer> addShopGoodsTrafficForPdd(ShopTrafficGoodsEntity entity) {
        return repositroy.addShopGoodsTrafficForPdd(entity);
    }
    
}
