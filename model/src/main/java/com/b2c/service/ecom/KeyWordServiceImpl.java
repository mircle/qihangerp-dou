package com.b2c.service.ecom;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.KeyWordEntity;
import com.b2c.repository.KeyWordRepository;
import com.b2c.interfaces.ecom.KeyWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KeyWordServiceImpl implements KeyWordService {
    @Autowired
    private KeyWordRepository repository;
    @Override
    public void addKeyWord(KeyWordEntity entity) {
        repository.addKeyWord(entity);
    }

    @Override
    public PagingResponse<KeyWordEntity> getList(Integer pageIndex, Integer pageSize, String source, String year, String keyword,Long parentId,Integer categoryId) {
        return repository.getList(pageIndex, pageSize, source, year, keyword,parentId,categoryId);
    }

    @Override
    public List<KeyWordEntity> getParentList(Long parentId) {
        return repository.getParentList(parentId);
    }

    @Override
    public KeyWordEntity getById(Long id) {
        return repository.getById(id);
    }
}
