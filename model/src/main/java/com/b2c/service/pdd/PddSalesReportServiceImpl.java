package com.b2c.service.pdd;

import java.util.ArrayList;
import java.util.List;

import com.b2c.interfaces.pdd.PddSalesReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2c.entity.pdd.FinanceMonthReportPddVo;
import com.b2c.entity.pdd.SalesAndRefundReportPddVo;
import com.b2c.entity.pdd.SalesReportPddVo;
import com.b2c.repository.pdd.PddSalesReportRepository;

@Service
public class PddSalesReportServiceImpl implements PddSalesReportService {
    @Autowired
    private PddSalesReportRepository reportRepository;
    @Override
    public List<SalesReportPddVo> getSalesReport(Integer shopId,String startDate,String endDate) {
        
        return reportRepository.getSalesReport( shopId,startDate,endDate);
    }
    @Override
    public List<SalesReportPddVo> getSendReport(String date, Integer shopId) {
        
        return reportRepository.getSendReport(date, shopId);
    }
    @Override
    public List<SalesAndRefundReportPddVo> getSalesAndRefundReport(Integer shopId) {
        
        return reportRepository.getSalesAndRefundReport(shopId);
    }
    @Override
    public List<FinanceMonthReportPddVo> getFinanceMonthReport(Integer shopId, String month) {
        List<FinanceMonthReportPddVo> list = new ArrayList<>();
        month = "2022-07";
        var model = reportRepository.getFinanceMonthReport(shopId, month);
        model.setMonth(month);
        list.add(model);

        month = "2022-08";
        model = reportRepository.getFinanceMonthReport(shopId, month);
        model.setMonth(month);
        list.add(0,model);
        month = "2022-09";
        model = reportRepository.getFinanceMonthReport(shopId, month);
        model.setMonth(month);
        list.add(0,model);
        month = "2022-10";
        model = reportRepository.getFinanceMonthReport(shopId, month);
        model.setMonth(month);
        list.add(0,model);
        month = "2022-11";
        model = reportRepository.getFinanceMonthReport(shopId, month);
        model.setMonth(month);
        list.add(0,model);
        month = "2022-12";
        model = reportRepository.getFinanceMonthReport(shopId, month);
        model.setMonth(month);
        list.add(0,model);
        month = "2023-01";
        model = reportRepository.getFinanceMonthReport(shopId, month);
        model.setMonth(month);
        list.add(0,model);
        month = "2023-02";
        model = reportRepository.getFinanceMonthReport(shopId, month);
        model.setMonth(month);
        list.add(0,model);
        month = "2023-03";
        model = reportRepository.getFinanceMonthReport(shopId, month);
        model.setMonth(month);
        list.add(0,model);
        return list;
    }
    
}
