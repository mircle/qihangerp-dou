package com.b2c.service.pdd;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.apierp.req.ErpSalesOrderRefundReq;
import com.b2c.entity.pdd.RefundPddEntity;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.oms.OrderPddRefundRepository;
import com.b2c.interfaces.pdd.OrderPddRefundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderPddRefundServiceImpl implements OrderPddRefundService {
    @Autowired
    private OrderPddRefundRepository refundRepository;

    @Override
    public PagingResponse<RefundPddEntity> getList(Integer pageIndex, Integer pageSize, String num, Integer status,Integer auditStatus,Integer shippingStatus, Integer shopId,Integer afterSalesType,Long goodsId) {
        return refundRepository.getList(pageIndex, pageSize, num,  status,auditStatus,shippingStatus,  shopId, afterSalesType,goodsId);
    }

    @Override
    public RefundPddEntity getEntityById(Long id) {
        return refundRepository.getEntityById(id);
    }

    @Override
    public RefundPddEntity getEntityByOrderSn(String orderSn) {
        return refundRepository.getEntityByOrderSn(orderSn);
    }

    @Override
    public ResultVo<Long> confirmRefund(Long id) {
        return refundRepository.confirmRefund(id);
    }

    @Override
    public void signRefund(Long id,Integer auditStatus,String remark) {
         refundRepository.signRefund(id,auditStatus,remark);
    }

    @Override
    public ResultVo<Long> updRefundPddSpec(Long id, Integer erpGoodSpecId, Integer quantity) {
        return refundRepository.updRefundPddSpec(id,erpGoodSpecId,quantity);
    }

    @Override
    public ResultVo<Integer> confirmBatchRefund(Integer shopId,String startDate, String endDate) {
        return refundRepository.confirmBatchRefund(shopId,startDate,endDate);
    }

    @Override
    public void updPddRefundLogisCode(Long id, String trackingNumber) {
        refundRepository.updPddRefundLogisCode(id,trackingNumber);
    }

    @Override
    public void updPddRefundDescribe(Long refundId, String describe) {
        refundRepository.updPddRefundDescribe(refundId, describe);
    }

    @Override
    public ResultVo<Integer> addPddOrderRefund(ErpSalesOrderRefundReq refund) {
        return refundRepository.addPddOrderRefund(refund);
    }

    @Override
    public Long getRefundIdMin() {
        return refundRepository.getRefundIdMin();
    }

    @Override
    public ResultVo<Integer> erpOrderNotFundAddStock(RefundPddEntity ritem) {
        return refundRepository.erpOrderNotFundAddStock(ritem);
    }
}
