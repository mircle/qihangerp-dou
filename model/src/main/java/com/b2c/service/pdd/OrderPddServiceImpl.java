package com.b2c.service.pdd;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.WaitSendGoodsSpecModel;
import com.b2c.entity.vo.ExcelOrderFileLogs;
import com.b2c.entity.apitao.PddOrderStatisVo;
import com.b2c.entity.apitao.UpdOrderTagReq;
import com.b2c.entity.pdd.OrderPddEntity;
import com.b2c.entity.pdd.OrderPddItemEntity;
import com.b2c.entity.pdd.OrderViewModel;
import com.b2c.entity.pdd.PddPrintOrderVo;
import com.b2c.entity.pdd.RefundPddEntity;
import com.b2c.entity.result.ResultVo;
import com.b2c.interfaces.pdd.OrderPddService;
import com.b2c.repository.fahuo.OrderSendRepository;
import com.b2c.repository.oms.OrderPddRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderPddServiceImpl implements OrderPddService {
    @Autowired
    private OrderPddRepository repository;
    // @Autowired
    // private OrderConfirmRepository orderConfirmRepository;
    @Autowired
    private OrderSendRepository orderSendRepository;

    @Override
    public ResultVo<Long> interceptOrder(String orderSn, Long refundId,String msg,Integer afterSalesType) {
        return repository.interceptOrder(orderSn,refundId,msg,afterSalesType);
    }

    @Override
    public ResultVo<Long> insertOrder(OrderPddEntity order,Integer shopId) {
        return repository.insertOrder(order,shopId);
    }

    @Override
    public ResultVo<Long> insertOrderForMessage(Integer shopId, String tid) {
        return repository.insertOrderForMessage(shopId,tid);
    }

    @Override
    public ResultVo<Long> editPddOrder(OrderPddEntity order, Integer shopId) {
        return repository.editPddOrder(order,shopId);
    }

    @Override
    public ResultVo<Integer> updateOrder(Integer orderStatus,String company,String number,String shipping_time,Integer refundStatus,Integer afterStatus,String remark,String orderSn,String lastShipTime,Integer shopId) {
        return repository.updateOrder(orderStatus,company,number,shipping_time,refundStatus,afterStatus,remark,orderSn,lastShipTime,shopId);
    }

    @Override
    public OrderPddEntity queryOrderBySn(String orderSn) {
        return repository.queryOrderBySn(orderSn);
    }

//    @Override
//    public ResultVo<ErpSalesPullCountResp> editErpSalesOrder(OrderPddEntity order,Integer shopId) {
//        return repository.editErpSalesOrder(order,shopId);
//    }

    @Override
    public ResultVo<Integer> editRefundPddOrder(RefundPddEntity r) {
        return repository.editRefundPddOrder(r);
    }

    @Override
    public ResultVo<Integer> updRefundPddOrder(RefundPddEntity r) {
        return repository.updRefundPddOrder(r);
    }

    /**
     * 
     * 查询订单和item（2022-10-12）
     */
    @Override
    public PagingResponse<OrderPddEntity> getOrderListAndItem(Integer pageIndex, Integer pageSize, String orderSn, Integer status, Integer shopId,Integer startTime, Integer endTime, Integer auditStatus,String pddGoodsId) {
        return repository.getOrderListAndItem(pageIndex,pageSize,orderSn,status,shopId,startTime,endTime,auditStatus,pddGoodsId);
    }

    @Override
    public PagingResponse<OrderPddEntity> getOrderListByStatus(Integer pageIndex, Integer pageSize, Integer shopId,Integer orderStatus, Integer refundStatus) {
        return repository.getOrderListByStatus(pageIndex,pageSize,shopId,orderStatus,refundStatus);
    }

    @Override
    public PagingResponse<OrderPddEntity> getOrderList(Integer pageIndex, Integer pageSize, String orderSn, Integer status, Integer shopId, List<Integer> tags, Integer startTime, Integer endTime, Integer auditStatus) {
        return repository.getOrderList(pageIndex,pageSize,orderSn,status,shopId,tags,startTime,endTime,auditStatus);
    }

    /**
     * 获取订单展示列表（order_item left join orders）
     * @param pageIndex
     * @param pageSize
     * @param orderSn
     * @param goodsSpecNum
     * @param status
     * @param shopId
     * @param startTime
     * @param endTime
     * @return
     */
    @Override
    public PagingResponse<OrderViewModel> getOrderViewList(Integer pageIndex, Integer pageSize, String orderSn, String goodsSpecNum, Integer status, Integer shopId, Integer startTime, Integer endTime,Integer printStatus,int orderBy,String trackingNumber) {
        return repository.getOrderViewList(pageIndex, pageSize, orderSn, goodsSpecNum, status,null, shopId, startTime, endTime, printStatus,orderBy,trackingNumber,null);
    }

    @Override
    public PagingResponse<OrderViewModel> getOrderViewList(Integer pageIndex, Integer pageSize, String orderSn, String goodsSpecNum, Integer status, Integer refundStatus, Integer shopId, Integer startTime, Integer endTime, Integer printStatus, int orderBy, String trackingNumber) {
        return repository.getOrderViewList(pageIndex, pageSize, orderSn, goodsSpecNum, status,refundStatus, shopId, startTime, endTime, printStatus,orderBy,trackingNumber,null);
    }

    @Override
    public PagingResponse<OrderViewModel> getOrderViewList(Integer pageIndex, Integer pageSize, String orderSn, String goodsSpecNum, Integer status, Integer shopId, Integer startTime, Integer endTime, Integer printStatus, int orderBy, String trackingNumber, String goodNum) {
        return repository.getOrderViewList(pageIndex, pageSize, orderSn, goodsSpecNum, status,null, shopId, startTime, endTime, printStatus,orderBy,trackingNumber,goodNum);
    }

    @Override
    public PagingResponse<OrderViewModel> getOrderViewListByPrint(Integer pageIndex, Integer pageSize, Integer shopId, Integer printStatus, String trackingNumber, String printStartTime, String printEndTime) {
        return repository.getOrderViewListByPrint(pageIndex,pageSize,shopId,printStatus,trackingNumber,printStartTime,printEndTime);
    }

    @Override
    public PagingResponse<OrderViewModel> getOrderViewListPrintWdy(Integer shopId, Integer pageIndex, Integer pageSize, String orderSn, String goodsNum, String goodsSpecNum, Integer printStatus, String startTime, String endTime) {
        return repository.getOrderViewListPrintWdy(shopId,pageIndex,pageSize,orderSn,goodsNum,goodsSpecNum,printStatus,startTime,endTime);
    }

    @Override
    public List<OrderPddEntity> getDshList(Integer shopId, Integer auditStatus, Long logId) {
        return repository.getDshList(shopId,auditStatus,logId);
    }

    @Override
    public List<OrderPddEntity> getPddOrderHebing(Integer shopId,Integer printStatus,String goodsNum) {
        return repository.getPddOrderHebing(shopId,printStatus,goodsNum);
    }

    @Override
    public List<OrderPddEntity> getPddOrderCodePrint(Integer shopId) {
        return repository.getPddOrderCodePrint(shopId);
    }

    @Override
    public OrderPddEntity getOrderDetailAndItemsById(Long id) {
        return repository.getOrderDetailAndItemsById(id);
    }

    @Override
    public ResultVo<Integer> orderConfirmAndJoinDeliveryQueueForPdd(Long orderId, String receiver, String mobile, String address, String remark) {
        return orderSendRepository.orderConfirmAndJoinDeliveryQueueForPdd(orderId, receiver, mobile, address, remark);
        //return orderConfirmRepository.orderConfirmAndJoinDeliveryQueueForPdd(orderId, receiver, mobile, address, sellerMemo);
    }

    @Override
    public ResultVo<Integer> orderBatchConfirm(Integer shopId, String startDate, String endDate) {
        return null;//orderConfirmRepository.orderBatchConfirm(shopId,startDate,endDate);
    }

    @Override
    public ResultVo<Long> updOrderSpec(Long orderItemId, Integer erpGoodSpecId, Integer quantity) {
        return repository.updOrderSpec(orderItemId,erpGoodSpecId,quantity);
    }

    @Override
    public ResultVo<Integer> orderCreatePdd(OrderPddEntity order) {
        return repository.orderCreatePdd(order);
    }
    @Override
    public ResultVo<Long> addOrderGift(Long orderId, Integer erpGoodsId, Integer erpGoodSpecId, Integer quantity) {
        return repository.addOrderGift(orderId,erpGoodsId,erpGoodSpecId,quantity);
    }

    @Override
    public void initExpress(String express) {
        repository.initExpress(express);
    }

    @Override
    public Integer getPddLogisticsCompanyId(String logisticsCompany) {
        return repository.getPddLogisticsCompanyId(logisticsCompany);
    }

    @Override
    public void updPddOrderStats(Long orderId) {
        repository.updPddOrderStats(orderId);
    }

    @Override
    public void updPddOrderStatus(String orderSN, Integer orderStatus, Integer refundStatus) {
        repository.updPddOrderStatus(orderSN,orderStatus,refundStatus);
    }

    @Override
    public void delPddOrderItemIsGift(Long orderItemId) {
        repository.delPddOrderItemIsGift(orderItemId);
    }

    @Override
    public void updOderTag(UpdOrderTagReq req) {
        repository.updOderTag(req);
    }

    @Override
    public ResultVo<Integer> createStockOurForm(Integer shopId, String startDate, String endDate) {
        return repository.createStockOurForm(shopId,startDate,endDate);
    }

    @Override
    public String getPddUpTodate(Integer shopId) {
        return repository.getPddUpTodate(shopId);
    }

    @Override
    public PddOrderStatisVo getOrderStatic(String date) {
        return repository.getOrderStatic(date);
    }

    @Override
    public Integer orderCount(String orderSn) {
        return repository.orderCount(orderSn);
    }

    @Override
    public List<OrderPddEntity> getPddRefundList(Integer shopId,String startDate, String endDate) {
        return repository.getPddRefundList(shopId,startDate, endDate);
    }

    @Override
    public OrderPddEntity getOrder(String orderNo) {
        return repository.getOrder(orderNo);
    }

    @Override
    public ResultVo<Integer> orderConfirmPddExcelIn(Long orderId,String trackingCompany,String trackingNumber,String shippingTime,Long logId,String phone,String name) {
        return null;//orderConfirmRepository.orderConfirmPddExcelIn(orderId,trackingCompany,trackingNumber,shippingTime,logId,phone,name);
    }

    @Override
    public Long addExcelOrderInLogs(Long id,Integer shopId,Integer totalCount,Integer successCount, Integer notFundCount,Integer failCount) {
        return null;//orderConfirmRepository.addExcelOrderInLogs(id,shopId,totalCount,successCount,notFundCount,failCount);
    }

    @Override
    public PagingResponse<ExcelOrderFileLogs> getOrderListAndItem(Integer pageIndex, Integer pageSize, Integer shopId) {
        return null;//orderConfirmRepository.getList(pageIndex,pageSize,shopId);
    }

    /*@Override
    public PagingResponse<OrderPddEntity> getDfhList(Integer pageIndex, Integer pageSize, Integer startTime, Integer endTime, Integer shopId,Integer printStatus) {
        return repository.getDfhList(pageIndex,pageSize,startTime,endTime,shopId,printStatus);
    }*/

    @Override
    public ResultVo<Integer> updPddOrderPrint(OrderViewModel order, String company, String code, String encryptedData, String signature) {
        return repository.updPddOrderPrint(order,company,code,encryptedData,signature);
    }

    @Override
    public ResultVo<String> checkPrint(List<OrderViewModel> orders) {
        return repository.checkPrint(orders);
    }

    public List<WaitSendGoodsSpecModel> getWaitSendGoodsSpecList() {
        return repository.getWaitSendGoodsSpecList();
    }

    @Override
    public ResultVo<PddPrintOrderVo> getPrintOrderList(String orderNo, Integer isHebing, String goodsNum) {
        return repository.getPrintOrderList(orderNo,isHebing,goodsNum);
    }

    @Override
    public void updPddOrderResult(Long orderId, String result,Integer printStatus,String printTime) {
        repository.updPddOrderResult(orderId,result,printStatus,printTime);
    }

    @Override
    public List<OrderViewModel> getPrintOrderRepeat(String orderSn) {
        return repository.getPrintOrderRepeat(orderSn);
    }

    @Override
    public List<OrderViewModel> getPrintOrderRepeatNumber(String trackingNumber) {
        return repository.getPrintOrderRepeatNumber(trackingNumber);
    }

    @Override
    public ResultVo<List<OrderViewModel>> getPrintOrderSelectList(ArrayList orderSnArr) {
        return repository.getPrintOrderSelectList(orderSnArr);
    }

    @Override
    public ResultVo<Integer> cancelOrderPrint(Long orderId) {
        return repository.cancelOrderPrint(orderId);
    }

    @Override
    public ResultVo<Integer> orderSend(Long orderId) {
        return repository.orderSend(orderId);
    }
    @Override
    public ResultVo<Integer> orderSendAndConfirm(OrderPddEntity order){
        return repository.orderSendAndConfirm(order);
    }
    @Override
    public ResultVo<Long> updOrderRefundSpec(Long refundId, Integer erpGoodSpecId, Integer quantity) {
        return repository.updOrderRefundSpec(refundId,erpGoodSpecId,quantity);
    }

    @Override
    public ResultVo<Integer> updOrderRemark(Long orderId, String remark) {
      
        return repository.updOrderRemark(orderId, remark);
    }
 
    @Override
    public ResultVo<Integer> updOrderAddress(Long orderId, String receiverName, String receiverPhone,
            String receiverAddress) {
        return repository.updOrderAddress(orderId, receiverName, receiverPhone, receiverAddress);
    }

    @Override
    public String getLashOrderCreateTime(Integer shopId) {
        return repository.getLashOrderCreateTime(shopId);
    }

    @Override
    public Long getLashRefundCreateTime(Integer shopId) {
        return repository.getLashRefundCreateTime(shopId);
    }

    @Override
    public OrderPddEntity getOrder(Long orderId) {
        return repository.getOrder(orderId);
    }

    @Override
    public OrderPddItemEntity getOrderItemByItemId(Long itemId) {
       
        return repository.getOrderItemByItemId(itemId);
    }
}
