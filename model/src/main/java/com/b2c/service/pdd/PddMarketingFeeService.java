package com.b2c.service.pdd;

import com.b2c.entity.pdd.PddMarketingFeeEntity;
import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;

import java.util.List;

public interface PddMarketingFeeService {
    PagingResponse<PddMarketingFeeEntity> getList(Integer pageIndex, Integer pageSize,Integer shopId,Integer type, String startTime, String endTime);

    ResultVo<String> importExcelFeeList(List<PddMarketingFeeEntity> list, Integer shopId);
}
