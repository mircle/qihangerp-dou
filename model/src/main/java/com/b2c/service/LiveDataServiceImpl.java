package com.b2c.service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.zbj.LiveDataEntity;
import com.b2c.entity.zbj.LiveExpensesEntity;
import com.b2c.interfaces.LiveDataService;
import com.b2c.repository.LiveDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LiveDataServiceImpl implements LiveDataService {
    @Autowired
    private LiveDataRepository repository;

    @Override
    public ResultVo<Long> addLiveData(LiveDataEntity liveDataEntity) {
        return repository.addLiveData(liveDataEntity);
    }

    @Override
    public PagingResponse<LiveDataEntity> getList(Integer pageIndex, Integer pageSize, String authorAccount) {
        return repository.getList(pageIndex, pageSize, authorAccount);
    }

    @Override
    public LiveExpensesEntity getLiveExpensesByLiveId(Long liveId) {
        return repository.getLiveExpensesByLiveId(liveId);
    }

    @Override
    public void editLiveExpenses(LiveExpensesEntity entity) {
        repository.editLiveExpenses(entity);
    }

    @Override
    public void addLiveReplayFile(Long liveId, String filePath) {
        repository.addLiveReplayFile(liveId, filePath);
    }

    @Override
    public void delLiveData(Long liveId) {
        repository.delLiveData(liveId);
    }

    @Override
    public LiveDataEntity getLiveById(Long liveId) {
        return repository.getLiveById(liveId);
    }
}
