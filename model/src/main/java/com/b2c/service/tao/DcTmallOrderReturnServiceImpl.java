package com.b2c.service.tao;

import com.b2c.interfaces.tao.DcTmallOrderReturnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.tao.TmallOrderRefundVo;
import com.b2c.repository.tao.DcTmallOrderReturnRepository;

@Service
public class DcTmallOrderReturnServiceImpl implements DcTmallOrderReturnService {
    @Autowired
    private DcTmallOrderReturnRepository repository;
    @Override
    public PagingResponse<TmallOrderRefundVo> getList(Integer shopId,Integer pageIndex, Integer pageSize, String orderId,
            String refundId,String logisticsCode) {
        
        return repository.getList(shopId, pageIndex, pageSize, orderId, refundId,logisticsCode);
    }
    
}
