package com.b2c.service.tao;

import com.b2c.interfaces.tao.TaoGoodsUpgradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.tao.TaoGoodsUpgradeEntity;
import com.b2c.repository.tao.TaoGoodsUpgradeRepository;

@Service
public class TaoGoodsUpgradeServiceImpl implements TaoGoodsUpgradeService {

    @Autowired
    private TaoGoodsUpgradeRepository repository;

    @Override
    public ResultVo<Long> addRecord(Integer shopId,Long goodsId, String oldTitle, Integer oldSales, String newTitle, String remark,
            String date) {
        
        return repository.addRecord(shopId,goodsId, oldTitle, oldSales, newTitle, remark, date);
    }

    @Override
    public PagingResponse<TaoGoodsUpgradeEntity> getList(Integer shopId, Integer pageIndex, Integer pageSize,
            String goodsId) {
  
        return repository.getList(shopId, pageIndex, pageSize, goodsId);
    }
    
}
