package com.b2c.service.wms;

import com.b2c.entity.ErpOrderItemEntity;
import com.b2c.entity.ErpStockLocationEntity;
import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpStockLocationBatchAddVo;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.erp.ErpStockLocationRepository;
import com.b2c.interfaces.wms.StockLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-03-21 18:18
 */
@Service
public class StockLocationServiceImpl implements StockLocationService {
    @Autowired
    private ErpStockLocationRepository repository;

    @Override
    public List<ErpStockLocationEntity> getListByParentId(Integer parentId) {
        return repository.getListByParentId(parentId);
    }

    @Override
    public List<ErpOrderItemEntity> localtionNumber() {
        return repository.localtionNumber();
    }
    //    @Override
//    public List<ErpStockLocationEntity> getListByDepth(Integer depth) {
//        return repository.getListByDepth(depth);
//    }

    @Override
    public PagingResponse<ErpStockLocationEntity> getListByDepth(Integer pageIndex, Integer pageSize, Integer depth,String number) {
        if (pageIndex <= 0) pageIndex = 1;
        if (pageSize <= 0) pageSize = 20;
        return repository.getListByDepth(pageIndex,pageSize,depth,number);
    }

    @Override
    public Integer getIdByName(String name) {
        return repository.getIdByName(name);
    }

    @Override
    public Integer getIdByNumber(String number) {
        return repository.getIdByNumber(number);
    }

    @Override
    public ErpStockLocationEntity getEntityByNumber(String number) {
        return repository.getEntityByNumber(number);
    }

    @Override
    public boolean checkStockLocationUsed(Integer id) {
        return repository.checkStockLocationUsed(id);
    }

    @Override
    public Integer addStockHouse(String number, String name) {
        return repository.addStockHouse(number, name);
    }

    @Override
    public Integer addReservoir(String number, String name, Integer parentId1) {
        return repository.addReservoir(number,name,parentId1);
    }

    @Override
    public Integer addShelf(String number, String name, Integer houseId, Integer reservoirId) {
        return repository.addShelf(number,name,houseId,reservoirId);
    }

    @Override
    public void updateShelf(Integer id, String number, String name, Integer houseId, Integer reservoirId) {
        repository.updateShelf(id,number,name,houseId,reservoirId);
    }

    @Override
    public void updateReservoir(Integer id, String number, String name, Integer houseId) {
        repository.updateReservoir(id,number,name,houseId);
    }

    @Override
    public void updateStockHouse(Integer stockId, String number, String name) {
        repository.updateStockHouse(stockId, number, name);
    }

    @Override
    public ResultVo<Integer> delStockHouse(Integer id) {
        if (repository.getStockNumById(id)) {
            return new ResultVo<>(EnumResultVo.HasAssociatedData, "该仓库还有库存，不能删除");
        }
        repository.delStockHouse(id);
        return new ResultVo<>(EnumResultVo.SUCCESS);
    }

    @Override
    public String getName(Integer id) {
        return repository.getName(id);
    }

    @Override
    public ResultVo<Integer> batchAddShelf(ErpStockLocationBatchAddVo shelf) {
        return repository.batchAddShelf(shelf);
    }

//    @Override
//    public List<ErpStockLocationEntity> getShelfByReservoirIdAndNotUsed(Integer reservoirId, Integer selfId) {
//        return repository.getShelfByReservoirIdAndNotUsed(reservoirId, selfId);
//    }

    @Override
    public List<ErpStockLocationEntity> getListByNumber(String number) {
        Integer pageSize=10;
        return repository.getListByNumber(pageSize,3,number);
    }
}
