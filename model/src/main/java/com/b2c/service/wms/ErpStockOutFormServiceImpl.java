package com.b2c.service.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.common.utils.DateUtil;
import com.b2c.entity.ErpOrderItemEntity;
import com.b2c.entity.StockDataVo;
import com.b2c.entity.ErpStockOutFormItemEntity;
import com.b2c.entity.ErpStockOutPickVo;
import com.b2c.entity.ErpStockOutFormEntity;
import com.b2c.entity.erp.vo.ErpStockOutGoodsListVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.erp.ErpStockOutFormRepository;
import com.b2c.interfaces.wms.ErpStockOutFormService;
import com.b2c.entity.vo.ErpStockOutFormDetailVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 描述：
 * 出库商品清单Service
 *
 * @author qlp
 * @date 2019-06-21 13:44
 */
@Service
public class ErpStockOutFormServiceImpl implements ErpStockOutFormService {
    @Autowired
    private ErpStockOutFormRepository repository;

    @Override
    public PagingResponse<ErpStockOutFormEntity> getPickingList(Integer pageIndex, Integer pageSize, String No, Integer status,Long id,String sourceNo) {
        return repository.getPickingList(pageIndex, pageSize, No, status,id,sourceNo);
    }

    @Override
    public ErpStockOutFormEntity getStockOutFormById(Long id) {
        return repository.getStockOutFormById(id);
    }

    @Override
    public ErpStockOutFormEntity getStockOutFormByNumber(String stockOutNo) {
        return repository.getStockOutFormByNumber(stockOutNo);
    }

    @Override
    public List<ErpStockOutGoodsListVo> getStockOutGoodsForOrderByFormId(Long stockOutId) {
        return repository.getStockOutGoodsForOrderByFormId(stockOutId);
    }

    @Override
    public List<ErpStockOutGoodsListVo> getStockOutFormItemListByFormId(Long stockOutId) {
        return repository.getStockOutFormItemListByFormId(stockOutId);
    }


    @Override
    public Integer printPicking(Long id) {
        return repository.printPicking(id);
    }


    @Override
    public ResultVo<Integer> orderGoodsStockOut(Long orderId, Integer userId, String userName) {

        return repository.orderGoodsStockOut(orderId, userId, userName);
    }

    @Override
    public PagingResponse<ErpStockOutFormEntity> getStockOutFormList(Integer pageIndex, Integer pageSize,Integer outType, String number, String startDate, String endDate) {
        Long star = 0L;
        if (!StringUtils.isEmpty(startDate)) star = DateUtil.strToLong(startDate);
        Long end = 0L;
        if (!StringUtils.isEmpty(endDate)) end = DateUtil.strToLong(endDate);
        return repository.getStockOutFormList(pageIndex, pageSize,outType, number, star, end);
    }

    @Override
    public ErpStockOutFormDetailVo getErpStockOutFormDetailVo(Long id) {
        return repository.getErpStockOutFormDetailVo(id);
    }




    @Override
    public ResultVo<Integer> cancelStockOut(Long stockOutFormItemId, Integer userId, String userName) {
        return repository.cancelStockOut(stockOutFormItemId,userId,userName);
    }

    @Override
    public Integer completePicking(Long id, List<ErpStockOutPickVo> list, Integer userId, String userName) {
        return repository.completePicking(id, list, userId, userName);
    }

    @Override
    public List<ErpOrderItemEntity> outList(String startDate, String endDate) {
        return repository.outList(startDate,endDate);
    }

    @Override
    public ResultVo<Long> joinStockOutQueueForXHS(String sourceNo, Long orderId, String createBy, List<StockDataVo> stockDataVos,String companyCode,String expressNo) {
        return repository.joinStockOutQueueForXHS(sourceNo, orderId, createBy, stockDataVos,companyCode,expressNo);
    }

    @Override
    public PagingResponse<ErpStockOutFormItemEntity> getStockOutFormItemWaitPickGoodsList(Integer pageIndex, Integer pageSize,String specNum) {
        return repository.getStockOutFormItemWaitPickGoodsList(pageIndex, pageSize,specNum);
    }

    @Override
    public ResultVo<Long> sendOrderXHS(Long orderId, String companyCode, String expressNo) {
        return repository.sendOrderXHS(orderId, companyCode, expressNo);
    }
}
