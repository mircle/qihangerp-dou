package com.b2c.service.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.erp.InvoiceEntity;
import com.b2c.entity.erp.InvoiceInfoEntity;
import com.b2c.entity.erp.enums.*;
import com.b2c.entity.erp.vo.ErpInvoiceInfoVo;
import com.b2c.entity.erp.vo.ErpInvoiceVo;
import com.b2c.entity.erp.vo.InvoiceInfoListVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.vo.wms.ErpPurchaseGoodVo;
import com.b2c.entity.vo.wms.InvoiceDetailVo;
import com.b2c.repository.erp.InvoiceRepository;
import com.b2c.interfaces.wms.InvoiceService;
import com.b2c.entity.vo.finance.ErpStockInFormItemVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-03-22 10:20
 */
@Service
public class InvoiceServiceImpl implements InvoiceService {
    @Autowired
    private InvoiceRepository repository;

    @Override
    public InvoiceEntity getInvoiceById(Long id) {
        return repository.getInvoiceById(id);
    }

    @Override
    public InvoiceEntity getInvoiceByBillNo(String billNo) {
        return repository.getInvoiceByBillNo(billNo);
    }

    @Override
    public Integer purchaseFormSubmit(InvoiceEntity invoiceForm, List<InvoiceInfoEntity> invoiceInfo) {
        return repository.purchaseAddSubmit(invoiceForm, invoiceInfo);
    }

    // @Override
    // public Integer saleFormSubmit(InvoiceEntity invoiceForm, List<InvoiceInfoEntity> invoiceInfo) {
    //     return repository.saleFormSubmit(invoiceForm, invoiceInfo);
    // }

    @Override
    public PagingResponse<InvoiceEntity> getList(Integer pageIndex, Integer pageSize, InvoiceTransTypeEnum transType, InvoiceBillTypeEnum billTypeEnum, InvoiceBillStatusEnum billStatusEnum, InvoiceCheckoutStatusEnum checkoutStatusEnum, InvoiceQualifiedStatusEnum qualifiedStatusEnum,
                                                 String billNo, String startDate, String endDate, Integer contactId) {
        return repository.getList(pageIndex, pageSize, transType, billTypeEnum, billStatusEnum, checkoutStatusEnum, qualifiedStatusEnum, billNo, startDate, endDate,contactId);
    }

    @Override
    public Integer deleteForm(Long id) {
        return repository.deleteForm(id);
    }


    @Override
    public InvoiceDetailVo getInvoiceDetail(Long id) {
        return repository.getInvoiceDetail(id);
    }

    @Override
    public InvoiceDetailVo getInvoiceDetail(String billNo) {
        return repository.getInvoiceDetail(billNo);
    }

    @Override
    public InvoiceInfoListVo getGoodsSpecByInvoiceAndSpec(Long invoiceId, String specNumber) {
        return repository.getGoodsSpecByInvoiceAndSpec(invoiceId, specNumber);
    }



//    @Override
//    public void updataCheckout(ArrayList<Integer> id, ArrayList<Integer> quantity, ArrayList<String> abnormalQuantity, String userName, InvoiceCheckoutStatusEnum checkoutStatusEnum) {
//        repository.updataCheckout(id, quantity, abnormalQuantity, userName, checkoutStatusEnum);
//    }

//    @Override
//    public void updataCheckoutIt(Integer id, String userName, Integer stutas) {
//        repository.updataCheckoutIt(id, userName, stutas);
//    }

//    @Override
//    public PagingResponse<InvoiceEntity> getCheckoutList(Integer pageIndex, Integer pageSize, InvoiceTransTypeEnum transType, InvoiceBillTypeEnum billTypeEnum, InvoiceBillStatusEnum billStatusEnum, String billNo) {
//        return repository.getCheckoutList(pageIndex, pageSize, transType, billTypeEnum, billStatusEnum, billNo);
//    }
//
//    @Override
//    public PagingResponse<InvoiceEntity> getCheckoutOk(Integer pageIndex, Integer pageSize, InvoiceTransTypeEnum transType, InvoiceBillTypeEnum billTypeEnum, InvoiceBillStatusEnum billStatusEnum, InvoiceCheckoutStatusEnum checkoutStatusEnum, String billNo) {
//        return repository.getCheckoutOk(pageIndex, pageSize, transType, billTypeEnum, billStatusEnum, checkoutStatusEnum, billNo);
//    }

    @Override
    public PagingResponse<InvoiceEntity> getAbnormal(Integer pageIndex, Integer pageSize, InvoiceTransTypeEnum transType, InvoiceBillTypeEnum billTypeEnum, InvoiceBillStatusEnum billStatusEnum, InvoiceCheckoutStatusEnum checkoutStatusEnum, String billNo, String startDate, String endDate, String transTypes) {
        return repository.getAbnormal(pageIndex, pageSize, transType, billTypeEnum, billStatusEnum, checkoutStatusEnum, billNo, startDate, endDate, transTypes);
    }

    @Override
    public PagingResponse<InvoiceEntity> getPurchaseCancelList(Integer pageIndex, Integer pageSize, InvoiceTransTypeEnum transType, String number, Integer status,Integer contactId) {
        return repository.getPurchaseCancelList(pageIndex,pageSize,transType,number,status,contactId);
    }

    @Override
    public PagingResponse<InvoiceInfoListVo> getInvoiceInfoList(Integer pageIndex, Integer pageSize, InvoiceTransTypeEnum transType, String number, String billDate) {
        return repository.getInvoiceInfoList(pageIndex,pageSize,transType,number,billDate);
    }

    @Override
    public List<ErpInvoiceInfoVo> getErpInvoiceInfoByIid(Long iid) {
        return repository.getErpInvoiceInfoByIid(iid);
    }

    @Override
    public ResultVo<Integer> purchaseGoodOut(Integer id, String[] ids, String[] stockId, String[] number,Integer stockInUserId, String stockInUserName) {
        return repository.purchaseGoodOut(id,ids,stockId,number,stockInUserId,stockInUserName);
    }

    @Override
    public ErpInvoiceVo getDetailById(Long id) {
        return repository.getDetailById(id);
    }

    @Override
    public void checkErpInvoice(Integer erpInvoiceId, Integer status) {
        repository.checkErpInvoice(erpInvoiceId,status);
    }
    @Override
    public Integer checkInvoice(Integer InvoiceId, Integer checked,String checkName) {
        return repository.checkInvoice(InvoiceId,checked,checkName);
    }

    @Override
    public void updInvoiceInfo(Integer erpInvoiceId,Integer InvoiceInfoId, Double price, Integer quantity) {
        repository.updInvoiceInfo(erpInvoiceId,InvoiceInfoId,price,quantity);
    }

    @Override
    public ResultVo<Integer> delErpInvoiceInfo(Long erpInvoiceInfoId, Long erpInvoiceId) {
        return repository.delErpInvoiceInfo(erpInvoiceInfoId,erpInvoiceId);
    }

    @Override
    public ResultVo<Integer> addInvoiceItem(Long id, ErpPurchaseGoodVo invoiceInfo) {
        return repository.addInvoiceItem(id,invoiceInfo);
    }

    @Override
    public List<ErpStockInFormItemVo> invoiceItemPOList(String startDate, String endDate) {
        return repository.invoiceItemPOList(startDate,endDate);
    }

    @Override
    public ResultVo<Integer> purchasePayAmount(Long id, Double amount) {
        return repository.purchasePayAmount(id,amount);
    }

    @Override
    public ResultVo<Integer> purchaseRefundAmount(Long id, Double amount) {
        return repository.purchaseRefundAmount(id,amount);
    }
}
