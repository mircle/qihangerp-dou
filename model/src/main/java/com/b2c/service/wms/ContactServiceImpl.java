package com.b2c.service.wms;

import com.b2c.entity.ContactEntity;
import com.b2c.entity.erp.enums.ContactTypeEnum;
import com.b2c.repository.erp.ContactRepository;
import com.b2c.interfaces.wms.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-03-21 13:53
 */
@Service
public class ContactServiceImpl implements ContactService {
    @Autowired
    private ContactRepository repository;

    @Override
    public List<ContactEntity> getList(ContactTypeEnum type) {
        return repository.getList(type);
    }
}
