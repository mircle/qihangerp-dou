package com.b2c.oms.controller.ajax;

import com.b2c.interfaces.erp.ErpGoodsService;
import com.b2c.interfaces.erp.ErpSalesOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RequestMapping("/sales")
@RestController
public class SalesOrderImportAjaxPipiController {
    @Autowired
    private ErpGoodsService erpGoodsService;
    @Autowired
    private ErpSalesOrderService salesOrderService;

    private static Logger log = LoggerFactory.getLogger(SalesOrderImportAjaxPipiController.class);


}
