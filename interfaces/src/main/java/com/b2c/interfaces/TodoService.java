package com.b2c.interfaces;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.TodoEntity;
import com.b2c.entity.result.ResultVo;

public interface TodoService {
    ResultVo<Long> addTodo(Integer shopId,Integer sourceType,String sourceId,String content);

    ResultVo<Long> handle(Integer id,String result);
    
    ResultVo<Long> completed(Integer id);
    
    PagingResponse<TodoEntity> getList(Integer pageIndex, Integer pageSize, Integer sourceType, String  sourceId, Integer status,String content);
}
