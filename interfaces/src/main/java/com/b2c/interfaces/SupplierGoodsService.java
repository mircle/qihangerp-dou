package com.b2c.interfaces;

import com.b2c.entity.SupplierGoodsEntity;
import com.b2c.entity.result.PagingResponse;

public interface SupplierGoodsService {
    SupplierGoodsEntity insert(SupplierGoodsEntity entity);
    PagingResponse<SupplierGoodsEntity> getList(Integer pageIndex,Integer pageSize,String number,Integer supplierId,Boolean publishStatus);
    void updateAttr3(Integer id,String attr3);
}
