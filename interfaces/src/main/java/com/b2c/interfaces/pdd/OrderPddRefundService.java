package com.b2c.interfaces.pdd;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.apierp.req.ErpSalesOrderRefundReq;
import com.b2c.entity.pdd.RefundPddEntity;
import com.b2c.entity.result.ResultVo;

/**
 * 拼多多退款
 */
public interface OrderPddRefundService {

    /**
     * 查询订单信息
     * @param pageIndex
     * @param pageSize
     * @param refundId 退款id
     * @param orderSn 订单编号
     * @param status after_sales_status 必填，售后状态 1：全部 2：买家申请退款，待商家处理 3：退货退款，待商家处理 4：商家同意退款，退款中 5：平台同意退款，退款中 6：驳回退款， 待买家处理 7：已同意退货退款,待用户发货 8：平台处理中 9：平台拒 绝退款，退款关闭 10：退款成功 11：买家撤销 12：买家逾期未处 理，退款失败 13：买家逾期，超过有效期 14 : 换货补寄待商家处理 15:换货补寄待用户处理 16:换货补寄成功 17:换货补寄失败 18:换货补寄待用户确认完成 31：商家同意拒收退款，待用户拒收;32: 待商家补寄发货
     * @param shopId
     * @param startTime
     * @param endTime
     * @return
     */
    PagingResponse<RefundPddEntity> getList(Integer pageIndex, Integer pageSize,String num, Integer status,Integer auditStatus,Integer shippingStatus, Integer shopId,Integer afterSalesType,Long goodsId);
    /**
     *
     * @param id
     * @return
     */
    RefundPddEntity getEntityById(Long id);

    /**
     * 根据订单号查询退货单
     * @param orderSn
     * @return
     */
    RefundPddEntity getEntityByOrderSn(String orderSn);

    /**
     * 退货确认到仓库
     * @param id
     * @return
     */
    ResultVo<Long> confirmRefund(Long id);

    /**
     * 标记已处理（auditStatus更新为2）
     * @param id
     * @return
     */
    void signRefund(Long id,Integer auditStatus,String remark);
     ResultVo<Long> updRefundPddSpec(Long id, Integer erpGoodSpecId, Integer quantity);
    /**
     * 批量确认退货
     * @param startDate
     * @param endDate
     * @return
     */
    public ResultVo<Integer> confirmBatchRefund(Integer shopId,String startDate,String endDate);
    /**
     * 修改物流信息
     * @param id
     * @param trackingNumber
     */
    public void updPddRefundLogisCode(Long id,String trackingNumber);

    void updPddRefundDescribe(Long refundId,String describe);

    public ResultVo<Integer> addPddOrderRefund(ErpSalesOrderRefundReq refund);

    /**
     * 获取最小的pdd_refund表ID
     * @return
     */
    Long getRefundIdMin();

    /**
     * erp无出库订单盘点入库
     * @param ritem
     * @return
     */
    public ResultVo<Integer> erpOrderNotFundAddStock(RefundPddEntity ritem);

}
