package com.b2c.interfaces;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.GoodsCenterEntity;
import com.b2c.entity.result.ResultVo;

/**
 * 描述：
 * 商品中心Service
 *
 * @author qlp
 * @date 2019-11-08 17:32
 */
public interface GoodsCenterService {
    PagingResponse<GoodsCenterEntity> getList(Integer pageIndex, Integer pageSize, String goodsNumber);


}
