package com.b2c.interfaces;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpGoodsPublishRecordEntity;

public interface ErpGoodsPublishRecordService {
    
    PagingResponse<ErpGoodsPublishRecordEntity> getList(int pageIndex,int pageSize,Integer shopId,String goodsNum);
    ErpGoodsPublishRecordEntity getEntityById(Integer id);
    void add(ErpGoodsPublishRecordEntity entity);
    void edit(ErpGoodsPublishRecordEntity entity);
    void publishToShop(Integer goodsSupplierId,String shopIds,String publishDate);


}
