package com.b2c.interfaces;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.zbj.LiveDataEntity;
import com.b2c.entity.zbj.LiveExpensesEntity;

public interface LiveDataService {
    /**
     * 添加数据
     * @param liveDataEntity
     * @return 返回自增ID
     */
    ResultVo<Long> addLiveData(LiveDataEntity liveDataEntity);

    /**
     * 获取分页数据
     * @param pageIndex
     * @param pageSize
     * @param authorAccount
     * @return
     */
    PagingResponse<LiveDataEntity> getList(Integer pageIndex, Integer pageSize, String authorAccount);

    /**
     * 获取直播营销费用
     * @param liveId
     * @return
     */
    LiveExpensesEntity getLiveExpensesByLiveId(Long liveId);

    /**
     * 编辑营销费用
     * @param entity
     */
    void editLiveExpenses(LiveExpensesEntity entity);

    /**
     * 插入直播复盘文件路径
     * @param liveId 直播ID
     * @param filePath 复盘文件路径
     */
    void addLiveReplayFile(Long liveId,String filePath);

    /**
     * 删除直播数据
     * @param liveId
     */
    void delLiveData(Long liveId);

    /**
     * 获取直播数据详情
     * @param liveId
     * @return
     */
    LiveDataEntity getLiveById(Long liveId);
}
