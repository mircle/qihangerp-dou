package com.b2c.interfaces;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.SampleListEntity;

/**
 * 直播间样品Service
 */
public interface SampleService {
    /**
     * 获取直播间样品List
     * @param pageIndex
     * @param pageSize
     * @param goodsNum
     * @param orderDate
     * @param zbjId
     * @return
     */
    PagingResponse<SampleListEntity> getSampleList(Integer pageIndex, Integer pageSize, String goodsNum, String orderDate, Integer zbjId);
}
