package com.b2c.interfaces.erp;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.erp.*;
import com.b2c.entity.erp.vo.ExpressInfoVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.vo.erp.ErpSalesOrderDetailVo;
import com.b2c.entity.vo.erp.ErpSalesOrderRefundDetailVo;
import com.b2c.entity.vo.OrderRefundApplyVo;
import com.b2c.entity.vo.OrderImportDaiFaEntity;

import java.util.List;

/**
 * 描述：
 * 销售外发订单service
 *
 * @author qlp
 * @date 2020-01-10 16:50
 */
public interface ErpSalesOrderService {
    /**
     * 查询订单list
     * @param pageIndex
     * @param pageSize
     * @param orderNum
     * @param contactMobile
     * @param saleType
     * @param clientUserId
     * @param status
     * @param shopId
     * @param auditStatus
     * @param startTime
     * @param endTime
     * @return
     */
    PagingResponse<ErpSalesOrderEntity> getList(int pageIndex, int pageSize, String orderNum, String contactMobile, Integer saleType, Integer clientUserId, Integer status, Integer shopId, Integer auditStatus, Integer startTime, Integer endTime);

    /**
     * 查询订单商品
     * @param pageIndex
     * @param pageSize
     * @param orderNum
     * @param sku
     * @param clientUserId
     * @param shopId
     * @param startTime
     * @param endTime
     * @return
     */
    PagingResponse<ErpSalesOrderItemView> getOrderItemList(int pageIndex, int pageSize, String orderNum, String sku, Integer clientUserId, Integer shopId, Integer startTime, Integer endTime);

    PagingResponse<ErpSalesOrderRefundEntity> getRefundList(int pageIndex, int pageSize, String refundNum, String orderNum, String logisticsCode, Integer saleType, Integer clientUserId, Integer status, Integer startTime, Integer endTime);

    /**
     * 获取详情
     * @param orderId
     * @return
     */
    ErpSalesOrderDetailVo getDetailById(Long orderId);

    /**
     * 获取订单item
     * @param orderItemId
     * @return
     */
    ErpSalesOrderItemEntity getOrderItemByItemId(Long orderItemId);

    /**
     * 获取退货详情
     * @param refundId
     * @return
     */
    ErpSalesOrderRefundDetailVo getRefundDetailById(Long refundId);

//    /**
//     * 导入批批网订单
//     * @param orderList 订单列表
//     * @param buyerUserId 客户userId
//     * @return
//     */
//    ResultVo<String> importExcelOrderForPiPi(List<OrderImportPiPiEntity> orderList, Integer buyerUserId);

    /**
     * 导入代发订单
     * @param orderList
     * @param buyerUserId
     * @return
     */
    ResultVo<String> importExcelOrderForDaiFa(List<OrderImportDaiFaEntity> orderList, Integer buyerUserId);

    /**
     * 修改orderItem 规格
     * @param orderItem
     */
    void updateOrderItemSkuByItemId(ErpSalesOrderItemEntity orderItem);
    /**
     * 删除orderItem 商品
     *
     */
    public void delOrderItemSkuByItemId(Long orderId, Long itemId);

    /**
     * 订单确认并加入到仓库发货队列
     *
     * @param orderId
     * @return
     */
    ResultVo<Integer> orderConfirmAndJoinDeliveryQueueForSales(Long orderId, String receiver, String mobile, String address, String sellerMemo);

    /**
     * 取消订单
     * @param orderId
     * @return
     */
    void cancelOrder(Long orderId);

    /**
     * 添加采购订单退货商品信息
     */
    ResultVo<Integer> refundApply(OrderRefundApplyVo applyVo);

    /**
     * OMS系统退货同意
     * @param refundId
     * @param exress
     * @return
     */
    ResultVo<Integer> refundAgree(Long refundId, ExpressInfoVo exress);
    ResultVo<Integer> refundRefuse(Long refundId);
    /**
     * HY下单
     * @param salesOrder
     * @return
     */
    ResultVo<Integer> editSalesOrder(ErpSalesOrderDetailVo salesOrder);
    /**
     * 添加订单更新日志
     * @param startTime
     * @param endTime
     * @param shopId
     */
    void addErpSalesPullOrderLog(Long startTime, Long endTime, Integer shopId, Integer addCount, Integer failCount, Integer updCount, Integer type);
    /**
     * 查询订单更新记录日志
     * @param shopId
     * @param type:0订单 1退货
     * @return
     */
    ErpPullOrderLogEntity getErpOrderPullLogByShopId(Integer shopId, Integer type);
    /**
     * 修改关联订单无商品Id
     * @param erpSalesOrderItemId
     * @param erpGoodSpecId
     * @return
     */
    ResultVo<Long> updErpSalesOrderSpec(Long erpSalesOrderItemId, Integer erpGoodSpecId, Integer quantity);
    /**
     * 添加待处理通知消息
     * @param jsonStr
     * @param shopId
     */
    void addNotifyMsg(String jsonStr, Integer shopId);

    /**
     * 添加礼品
     * @param orderId
     * @param erpGoodSpecId
     * @param quantity
     */
    void addGift(Long orderId, Integer erpGoodsId, Integer erpGoodSpecId, Integer quantity);

    /**
     * 删除礼品
     * @param orderId
     * @param orderItemId
     * @param erpGoodSpecId
     */
    void deleteGift(Long orderId, Long orderItemId, Integer erpGoodSpecId);
    /**
     * 编号与店铺id查询订单id
     * @param orderNum
     * @param shopId
     * @return
     */
    Long getErpSalesOrderId(String orderNum, Integer shopId);
    /**
     * 订单导入
     * @param orders
     * @return
     */
    ResultVo<Long> importErpSalesorder(List<ErpSalesOrderDetailVo> orders);
    /**
     * 店铺销售订单查询
     * @param pageIndex
     * @param pageSize
     * @param shopId
     * @param orderNum
     * @param startTime
     * @param endTime
     * @return
     */
     PagingResponse<ErpSalesOrderDetailVo> erpSalesOrderDetails(int pageIndex, int pageSize, Integer shopId, String orderNum, Integer startTime, Integer endTime);
}
