package com.b2c.interfaces.erp;

import com.b2c.entity.enums.EnumUserActionType;

/**
 * 描述：
 * 仓库系统用户操作日志
 *
 * @author qlp
 * @date 2019-10-24 09:16
 */
public interface ErpUserActionLogService {

    void addUserAction(Integer userId, EnumUserActionType actionType, String actionUrl, String actionContent, String actionResult);
}
