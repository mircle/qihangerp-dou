package com.b2c.interfaces.ecom;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ecom.GoodsImgDataEntity;

public interface GoodsImgDataService {
    PagingResponse<GoodsImgDataEntity> getList(Integer pageIndex, Integer pageSize,String num,String platform);
    void add(GoodsImgDataEntity entity);
}
