package com.b2c.interfaces.test;



import com.b2c.entity.result.PagingResp;
import com.b2c.entity.test.OrderPddTEntity;



public interface OrderPddTService {
    PagingResp<OrderPddTEntity> getOrderList(Integer pageIndex, Integer pageSize, Integer shopId, Integer orderStatus, Integer refundStatus, String orderSn, String goodsId, String trackNum);
    OrderPddTEntity getOrder(Long orderId);
}
