package com.b2c.interfaces;

import com.b2c.entity.pdd.ShopDataReportModel;
import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.entity.datacenter.DcSysThirdSettingEntity;
import com.b2c.entity.vo.ShopOrderStatisticsModel;
import com.b2c.entity.vo.ShopOrderStatisticsVo;

import java.util.List;

public interface ShopService {

    /**
     * 第三方平台列表
     * @return
     */
    public List<DcSysThirdSettingEntity> getDcList();

    /**
     * 新增店铺
     * @param entity
     * @return
     */
    public Integer addDcShop(DcShopEntity entity);

    /**

     *
     * 删除
     * @param entity
     */
    public void delDcShop(DcShopEntity entity);

    /**
     * 查询店铺列表(分页)
     * @return
     */
    public PagingResponse<DcShopEntity> getDcShopList(Integer pageIndex, Integer pageSize, Integer type);

    /**
     * 查询店铺列表
     * @return
     */
    List<DcShopEntity> getShopList(Integer type);
    public List<DcShopEntity> shopListShow();

    /**
     * 获取用户有权限的店铺list
     * @param userId
     * @return
     */
    List<DcShopEntity> getShopListByUserId(Integer userId,Integer type);

    /**
     * 获取店铺信息
     * @return
     */
    DcShopEntity getShop(Integer id);

    /**
     * 获取店铺信息by mallId
     * @param mallId
     * @return
     */
    DcShopEntity getShopByMallId(Long mallId);

    void updateSessionKey(Integer shopId, Long mallId, String sessionKey);

    /**
     * 店铺订单统计
     * @param shopId
     * @return
     */
    ShopOrderStatisticsVo shopOrderStatistics(Integer shopId);

    ShopOrderStatisticsModel shopOrderStatisticsPDD(Integer shopId);

    List<ShopDataReportModel> getShopDateReport(String startDate,String endDate);
}
