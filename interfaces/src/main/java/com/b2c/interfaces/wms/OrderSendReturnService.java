package com.b2c.interfaces.wms;

import java.util.List;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.tui.OrderSendReturnEntity;

public interface OrderSendReturnService {
    PagingResponse<OrderSendReturnEntity> getList(int pageIndex, int pageSize, String orderNum,String logisticsCode,Integer status);
    List<OrderSendReturnEntity> getListByIds(String ids);
    ResultVo<Integer> remark(Long id,String remark);
    /**
     * 退回商品计损
     * @param returnId
     * @param quantity
     * @param billNo
     * @return
     */
    ResultVo<Integer> returnOrderStockBadIn(Long returnId,Integer quantity, String billNo);

    ResultVo<Integer> returnOrderHuanhuoSend(Long returnId, String logisticsCode);
}
