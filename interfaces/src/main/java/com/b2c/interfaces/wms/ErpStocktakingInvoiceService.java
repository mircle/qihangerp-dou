package com.b2c.interfaces.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpStocktakingInvoice;
import com.b2c.entity.ErpStocktakingInvoiceItem;

import java.util.List;

public interface ErpStocktakingInvoiceService {
    PagingResponse<ErpStocktakingInvoice> getList(Integer pageIndex, Integer pageSize, String No);
    /**
     * 添加盘点单
     * @param billNo
     * @param billDate
     * @param stockLocationId
     * @param userId
     * @param userName
     * @param invoiceItems
     */
    void addInvoice(String billNo, String billDate, Long stockLocationId, Integer userId, String userName, List<ErpStocktakingInvoiceItem> invoiceItems);

    /**
     * 盘点录入
     * @param invoiceId
     * @param userId
     * @param userName
     * @param invoiceItems
     */
    void countedInvoiceItem(Long invoiceId, Integer userId, String userName, List<ErpStocktakingInvoiceItem> invoiceItems);

    ErpStocktakingInvoice getInvoiceById(Long id);
    List<ErpStocktakingInvoiceItem> getInvoiceItemByIid(Long iid);

    void countedConfirm(Long invoiceId, Integer userId, String userName);
}
