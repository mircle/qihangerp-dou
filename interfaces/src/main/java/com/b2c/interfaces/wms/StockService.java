package com.b2c.interfaces.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.erp.ErpGoodsSpecEntity;
import com.b2c.entity.ErpGoodsSpecMoveFromEntiy;
import com.b2c.entity.erp.vo.*;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.vo.ErpGoodsStockLogsListVo;

import java.util.List;

/**
 * 描述：
 * 供应商I
 *
 * @author qlp
 * @date 2019-03-21 13:53
 */
public interface StockService {

    /**
     * 库存分类查询
     *
     * @return
     */
    List<GoodsCategoryVo> getCategoryList();

    /**
     * 添加库存商品分类
     *
     * @param name
     * @param parentId
     * @param id
     */
    public void addStockCategory(String name, int parentId, int id);

    /**
     * 删除商品分类
     *
     * @param id 商品分类id
     */
    ResultVo<Integer> delStockCategory(int id);

    /**
     * 商品列表查询
     *
     * @param pageIndex
     * @param pageSize
     * @param number 商品编码
     * @param categoryId 商品一级大类id
     * @return
     */
    PagingResponse<ErpGoodsListVo> getGoodsList(int pageIndex, int pageSize, Integer contactId, String number, Integer categoryId, Integer status, String attr1, String attr4);
    List<ErpGoodsListVo> getGoodsListExport(String number,Integer categoryId,Integer status,String attr1,String attr4);

    /**
     * 商品列表查询
     * @param categoryId 商品分类id
     * @return
     */
    public ErpGoodsListVo getGoodsList_1(Integer categoryId,String attr1,String attr2,String attr4);

    /**
     * 商品列表查询,不分页
     * @param str
     * @return
     */
//    public List<ErpInviceListVo> getGoodsListN(String str);

    /**
     * 获取分类json对象
     *
     * @return
     */
    public String getCategoryString();

    /**
     * 编辑商品信息（新增，删除）
     *
     * @param goodsName
     * @param number
     * @param categoryId
     * @param unitId
     * @param goodsId
     * @return
     */
    public Integer editGoods(String goodsName, String number, Integer categoryId, Integer unitId, Integer locationId, Integer reservoirId, Integer shelfId, Integer goodsId);

    /**
     * 通过分类id查询分类名
     *
     * @param id
     * @return
     */
    public String getCategoryNameById(Integer id);

    /**
     * 编辑商品规格（新增，删除）
     *
     * @param specNumber
     * @param specName
     * @param lowQty
     * @param highQty
     * @param goodsId
     * @param specId
     * @return
     */
//    public Integer editGoodsSpec(String specNumber, String specName, String lowQty, String highQty, Integer goodsId, Integer specId, Integer locationId);

    /**
     * 商品编码是否存在
     *
     * @param number
     * @return 商品id
     */
    public Integer ifExsitNumber(String number);

    /**
     * 规格编码是否存在
     *
     * @param specNumber
     * @return
     */
    public Integer ifExsitSpecNumber(String specNumber);

    /**
     * 删除商品规格
     *
     * @param goodsId
     * @return
     */
    public ResultVo<Integer> deleteGoods(Integer goodsId);

    /**根据Id查看库存**/
    public Long getGooodsById(Integer goodsId);

    /**
     * 通过名称和父类id查询该名称是否已存在
     *
     * @param id
     * @param name
     * @return
     */
    public Integer getCateIdByNameAndParentId(Integer id, String name);

    /**
     * 查询计量表
     *
     * @return
     */
    public List<UnitEntiy> getUnitList();

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    public SpecRe getSpeckListById(Integer id);

    /**
     * 库存查询
     *
     * @param pageIndex
     * @param pageSize
     * @param locationNum 仓位编码
     * @param houseId
     * @return
     */
    PagingResponse<ErpGoodsSpecStockVo> goodsStockInfoSearch(int pageIndex, int pageSize, String locationNum, Integer houseId);

    /**
     * 商品SKU库存查询
     *
     * @param pageIndex
     * @param pageSize
     * @param number sku编码
     * @param filter0 是否过滤0数据
     * @param categoryId 商品分类id
     * @return
     */
    PagingResponse<ErpGoodsSpecStockVo> goodsSkuSearch(int pageIndex, int pageSize, String number,int filter0,Integer categoryId);
    List<ErpGoodsSpecStockVo> goodsSkuListExport();

    /**
     * 商品SKU库存查询记录
     * @param pageIndex
     * @param pageSize
     * @param number
     * @return
     */
    public PagingResponse<ErpGoodsStockLogsListVo> goodsSkuSearchLogs(int pageIndex, int pageSize, String number);

    public List<ErpGoodsSpecStockVo> goodsStockInfoSearch(String str, Integer houseId);

    /**
     * 根据有赞id查询商品规格列表
     *
     * @param yzItemId
     * @return
     */
    List<ErpGoodsSpecEntity> getGoodSpesByYzItemId(Long yzItemId);

    /**
     * 根据仓库，库区，货架，商品编码查询商品信息
     *
     * @param locationId   仓库Id
     * @param rreservoirId 库区Id
     * @param shelfId      货架Id
     * @param number       商品编码
     * @return
     */
//    List<ErpGoodsSpecVo> getList(Integer locationId, Integer rreservoirId, Integer shelfId, String number);

    /**
     * 根据规格编码查询商品库区
     * @param locationId
     * @param rreservoirId
     * @param shelfId
     * @param number
     * @return
     */
    List<ErpGoodsSpecVo> getGoodSpecByMoveSpec(Integer locationId, Integer rreservoirId, Integer shelfId, String number);



    /**
     * 添加商品移库单
     *
     * @param ids  erp_goods_stock_info.id
     * @param outLocaltionId 移出仓库Id
     * @param inLocationId 移入I
     * @param number      移库数量
     * @param moveNo      单号
     * @param remarks     备注
     * @param createBy    创建人
     */
    ResultVo<Integer> addGoodsSpecMoveForm(String[] ids,String[] number,String[] remarks, Integer outLocaltionId, Integer inLocationId, String moveNo,  String createBy);

    /**
     * 移库列表
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public PagingResponse<ErpGoodsSpecMoveFromEntiy> getStockHouseVo(Integer pageIndex, Integer pageSize,String moveNo);

    /**
     * 根据Id查询移库信息
     *
     * @param id
     * @return
     */
    public ErpGoodsSpecMoveFromEntiy getIdByErpGoodsSpecMoveFromEntiy(Integer id);

    /**
     * 根据商品id查询商品规格列表
     * @param goodId
     * @return
     */
    List<ErpGoodsSpecVo> getGoodSpecByGoodId(Integer goodId);

    /**
     * 根据id更新规格信息
     * @param oldId
     * @param newId
     * @return
     */
    ResultVo<Integer> updErpGoodSpecById(Integer oldId, Integer newId);

    /**
     * 根据sku编码搜索商品库存信息
     * @param number
     * @return
     */
    List<ErpGoodsSpecStockListVo> searchGoodSpecStockByNumber(String number);

    /**
     * 根据商品大类获取库存总数
     * @param categoryId 商品一级分类
     * @return
     */
//    ResultVo<Long> getStockQtyByGoodsCategory(Integer categoryId);

    /**
     * 获取分类库存统计
     * @return
     */
    List<ErpGoodsSpecStockListVo> getStockQtyByGoodsCategory();
    public List<ErpGoodsSpecEntity> specStockByExport();
}
