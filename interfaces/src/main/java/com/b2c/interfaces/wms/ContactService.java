package com.b2c.interfaces.wms;

import com.b2c.entity.ContactEntity;
import com.b2c.entity.erp.enums.ContactTypeEnum;

import java.util.List;

/**
 * 描述：
 * 供应商I
 *
 * @author qlp
 * @date 2019-03-21 13:53
 */
public interface ContactService {
    List<ContactEntity> getList(ContactTypeEnum type);
}
