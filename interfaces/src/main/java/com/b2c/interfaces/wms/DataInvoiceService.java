package com.b2c.interfaces.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.datacenter.ErpGoodsExprotVo;
import com.b2c.entity.erp.vo.ErpGoodsListVo;
import com.b2c.entity.erp.vo.ErpGoodsSpecListVo;
import com.b2c.entity.erp.vo.ErpGoodsSpecStockListVo;

import java.util.List;

public interface DataInvoiceService {
    /**
     * 商品库存查询
     *
     * @param pageIndex
     * @param pageSize
     * @param str
     * @return
     */
     PagingResponse<ErpGoodsSpecListVo> getSpecInventoryList(int pageIndex, int pageSize, String goodsNum, String goodsSpecNum);

    /**
     * 商品库存查询,不分页
     *
     * @param str
     * @return
     */
    public List<ErpGoodsExprotVo> getInvoiceList(String str);

    /**
     * 商品上架时间
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public PagingResponse<ErpGoodsSpecStockListVo> getGoodsList(int pageIndex, int pageSize);

    /**
     * Sku更新时间
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public PagingResponse<ErpGoodsSpecStockListVo> getGoodsSpecList(int pageIndex, int pageSize);

    /**
     * 商品入库更新时间
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public PagingResponse<ErpGoodsSpecStockListVo> getGoodsInvoiceList(int pageIndex, int pageSize);
}
