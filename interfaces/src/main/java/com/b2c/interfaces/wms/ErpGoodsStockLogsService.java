package com.b2c.interfaces.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.enums.erp.EnumGoodsStockLogSourceType;
import com.b2c.entity.enums.erp.EnumGoodsStockLogType;
import com.b2c.entity.vo.ErpGoodsStockLogsListVo;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-08 17:06
 */
public interface ErpGoodsStockLogsService {

    /**
     * 仓库日志 （仓库系统使用）
     * @param type 类型 入库 、出库
     * @param pageIndex
     * @param pageSize
     * @return
     */
    PagingResponse<ErpGoodsStockLogsListVo> getListByType(EnumGoodsStockLogType type,Integer pageIndex, Integer pageSize,Integer startTime,Integer endTime);


    PagingResponse<ErpGoodsStockLogsListVo> getListByTypeAndSource(EnumGoodsStockLogType type,EnumGoodsStockLogSourceType sourceType,Integer pageIndex, Integer pageSize);


    /**
     * 仓库入库
     * @param pageIndex
     * @param pageSize
     * @param sourceType EnumGoodsStockLogSourceType
     * @param goodsNumber
     * @param specNumber
     * @return
     */
    PagingResponse<ErpGoodsStockLogsListVo> getStockInListBySourceType(Integer pageIndex, Integer pageSize,EnumGoodsStockLogSourceType sourceType,String goodsNumber,String specNumber);

    List<ErpGoodsStockLogsListVo> getStockInListBySourceTypeForExcel(EnumGoodsStockLogSourceType sourceType,String goodsNumber,String specNumber);

    PagingResponse<ErpGoodsStockLogsListVo> getStockOutListBySourceType(Integer pageIndex, Integer pageSize,EnumGoodsStockLogSourceType sourceType,String goodsNumber,String specNumber);

    List<ErpGoodsStockLogsListVo> getStockOutListBySourceTypeForExcel(EnumGoodsStockLogSourceType sourceType,String goodsNumber,String specNumber);
}
