package com.b2c.interfaces.tao;


import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.tao.TaoMarketingFeeEntity;

import java.util.List;

public interface TaoMarketingFeeService {
    PagingResponse<TaoMarketingFeeEntity> getList(Integer pageIndex, Integer pageSize, Integer shopId, Integer type,String source, String startTime, String endTime);

    ResultVo<String> importExcelFeeList(List<TaoMarketingFeeEntity> list, Integer shopId,String source);
}
