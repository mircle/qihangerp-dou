package com.b2c.interfaces.tao;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.tao.TaoGoodsUpgradeEntity;

public interface TaoGoodsUpgradeService {
    ResultVo<Long> addRecord(Integer shopId,Long goodsId,String oldTitle,Integer oldSales,String newTitle,String remark,String date);
    PagingResponse<TaoGoodsUpgradeEntity> getList(Integer shopId, Integer pageIndex, Integer pageSize,String goodsId);
}
