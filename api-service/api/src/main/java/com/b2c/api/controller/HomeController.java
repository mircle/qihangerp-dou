package com.b2c.api.controller;

import com.b2c.api.response.LoginResp;
import com.b2c.api.response.NameValueVo;
import com.b2c.api.utils.AnnoManageUtil;
import com.b2c.entity.DataRow;
import com.b2c.entity.api.ApiResult;
import com.b2c.entity.api.ApiResultEnum;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class HomeController {

    @ResponseBody
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ApiResult<List<NameValueVo>> home( HttpServletRequest request) {
        List<Class<?>> list = AnnoManageUtil.getPackageController("com.b2c.api.controller", RestController.class);
//        Map<String,Object> mapp = new HashMap<>();
        List<String> urlList = new ArrayList<>();
        AnnoManageUtil.getRequestMappingMethod(list,urlList);
        List<NameValueVo> lists = new ArrayList<>();
        for (var i:urlList) {
            NameValueVo vo=new NameValueVo();
            vo.setName("url");
            vo.setValue(i);
            lists.add(vo);
        }
        return new ApiResult<>(ApiResultEnum.SUCCESS,lists);
    }
}
