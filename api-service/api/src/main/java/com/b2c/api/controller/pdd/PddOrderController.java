package com.b2c.api.controller.pdd;

import com.alibaba.fastjson.JSONObject;
import com.b2c.api.controller.CommonController;
import com.b2c.api.query.PddOrderQuery;
import com.b2c.entity.api.ApiResult;
import com.b2c.common.utils.DateUtil;
import com.b2c.common.utils.StringUtil;
import com.b2c.entity.DataRow;
import com.b2c.entity.apierp.req.ErpSalesOrderRefundReq;
import com.b2c.entity.apitao.PddOrderStatisVo;
import com.b2c.entity.erp.vo.ErpSalesPullCountResp;
import com.b2c.entity.pdd.EnumPddOrderStatus;
import com.b2c.entity.pdd.OrderPddEntity;
import com.b2c.entity.pdd.OrderPddItemEntity;
import com.b2c.entity.pdd.RefundPddEntity;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.interfaces.pdd.OrderPddRefundService;
import com.b2c.interfaces.pdd.OrderPddService;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class PddOrderController extends CommonController {
    private static Logger log = LoggerFactory.getLogger(PddOrderController.class);
    @Autowired
    private OrderPddService orderPddService;
    @Autowired
    private OrderPddRefundService refundService;
    /**
     * @api {post} /api/v1/pdd_order_list 分页PDD店铺订单列表
     * @apiVersion 1.0.0
     * @apiName pddOrderList
     * @apiGroup pddOrder
     * @apiParam {Number}  pageIndex 页数
     * @apiParam {Number}  pageSize 每页的条数
     * @apiParam {String}  [orderNo] 订单号
     * @apiParam {String}  [startTime] 开始时间
     * @apiParam {String}  [endTime] 结束时间
     * @apiParam {Number}  [status] 订单状态
     * @apiParam {Number}  [auditStatus] 审核状态0待审核1已审核
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK{
    "code": 0,
    "msg": "成功",
    "data": {
    "pageIndex": 1,
    "pageSize": 10,
    "totalSize": 165,
    "totalPage": 17,
    "hasNextPage": true,
    "hasPrePage": false,
    "list": [{
    "id": "id",
    "buyerName": "买家名",
    "totalAmount": "订单总额",
    "shippingFee": "邮费",
    "discountAmount": "优惠金额",
    "payAmount": "支付总额",
    "salesAmount": "销售总额",
    "discountRemark": "",
    "createTime": "2020-08-13T08:10:55.000+0000",
    "modifyTime": "修改时间",
    "payTime":"支付时间",
    "confirmedTime": null,
    "deliveryTime": null,
    "completeTime": null,
    "sellerMemo": "卖家备忘信息",
    "buyerFeedback": "买家留言",
    "closeReason": "",
    "receivingTime": null,
    "statusStr": "TRADE_CLOSED_BY_TAOBAO",
    "status": 11,
    "logisticsCompany": "快递公司",
    "logisticsCompanyCode": null,
    "logisticsCode": "快递单号",
    "sendStatus": 0,
    "refundId": null,
    "refundAmount": null,
    "refundStatus": null,
    "auditStatus": 0,
    "contactPerson": "",
    "mobile": "",
    "tel": null,
    "province": "省",
    "city": "市",
    "area": "区",
    "address": "地址",
    "orderSource": 0,
    "createOn": "创建时间",
    "shopId": 7,
    "developerId": 0,
    "developerName": null,
    "clientUserId": 0,
    "clientName": null,
    "isComment":"是否评价(0:未评1:已评)",
    "items": [{
    "id": 26840,
    "orderId": "订单id",
    "subItemId": "子订单id",
    "itemAmount": "子订单金额",
    "discountFee": "优惠金额",
    "adjustFee": 0.00,
    "goodsTitle": "商品标题",
    "goodsNumber": "商品编码",
    "productImgUrl": "图片",
    "productUrl": null,
    "productId": "0",
    "skuId": "0",
    "specNumber": "商品规格",
    "skuInfo": "商品规格字符串",
    "price": "价格",
    "quantity": "数量",
    "erpGoodsId": 17,
    "erpGoodsSpecId": 109,
    "statusStr": null,
    "status": null,
    "refundStatusStr": "退货状态",
    "refundStatus": null,
    "refundAmount": null,
    "refundId": null,
    "logisticsStatus": null,
    "newSpecId": 0,
    "newSpecNumber": null,
    "erpSpec": null,
    "newSpec": null,
    "currentQty": null,
    "lockedQty": null,
    "pickQty": null,
    "afterSaleState": 0
    }]
    }],
    "first": true,
    "end": false
    }
    }
     */
    @RequestMapping(value = "/pdd_order_list", method = RequestMethod.POST)
    public ApiResult<PagingResponse<OrderPddEntity>> orderList(@RequestBody PddOrderQuery query, @RequestHeader String token) {
//        var auth = JSONObject.parseObject(Authorization, com.b2c.apitao.request.Authorization.class);
//        var tokenInfo=getTokenInfo(token);
//        if(tokenInfo.getCode()>0)return new ApiResult<>(tokenInfo.getCode(),tokenInfo.getMsg());
        try {
            var result = orderPddService.getOrderListAndItem(
                    query.getPageIndex(),
                    query.getPageSize(),
                    query.getOrderNo(),
                    query.getStatus(),
                    query.getShopId(),
                    query.getStartTime(),query.getEndTime(),query.getAuditStatus(),query.getGoodsId());

            return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(),"成功",result);
        }catch (Exception ex){
            return new ApiResult<>(EnumResultVo.Fail.getIndex(),EnumResultVo.Fail.getName()+ex.getMessage());
        }
    }
    /**
     * @api {post} /api/v1/pdd_order_dsh_list 分页PDD店铺待确认订单列表
     * @apiVersion 1.0.0
     * @apiName pddOrderDshList
     * @apiGroup pddOrder
     * @apiParam {Number}  pageIndex 页数
     * @apiParam {Number}  pageSize 每页的条数
     * @apiParam {String}  [orderNo] 订单号
     * @apiParam {String}  [startTime] 开始时间
     * @apiParam {String}  [endTime] 结束时间
     * @apiParam {Number}  [LogId] 导入文件id
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK{
    "code": 0,
    "msg": "成功",
    "data": {
    "pageIndex": 1,
    "pageSize": 10,
    "totalSize": 165,
    "totalPage": 17,
    "hasNextPage": true,
    "hasPrePage": false,
    "list": [{

    "items": [{

    }]
    }],
    "first": true,
    "end": false
    }
    }
     */
    @RequestMapping(value = "/pdd_order_dsh_list", method = RequestMethod.POST)
    public ApiResult<List<OrderPddEntity>> pdd_order_dsh_list(@RequestBody PddOrderQuery query) {

//        var tokenInfo=getTokenInfo(token);
//        if(tokenInfo.getCode()>0)return new ApiResult<>(tokenInfo.getCode(),tokenInfo.getMsg());
        try {
            var result = orderPddService.getDshList(query.getShopId(), query.getAuditStatus(),query.getLogId());
            return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(),"成功",result);
        }catch (Exception ex){
            return new ApiResult<>(EnumResultVo.Fail.getIndex(),EnumResultVo.Fail.getName()+ex.getMessage());
        }
    }
    /**
     * @api {post} /api/v1/pdd_order_detail PDD订单详情
     * @apiVersion 1.0.0
     * @apiName pddOrderDetail
     * @apiGroup pddOrder
     * @apiParam {Number}  id 订单id
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK{
    "code": 0,
    "msg": "成功",
    "data": {
    "id": "id",
    "buyerName": "买家名",
    "totalAmount": "订单总额",
    "shippingFee": "邮费",
    "discountAmount": "优惠金额",
    "payAmount": "支付总额",
    "salesAmount": "销售总额",
    "discountRemark": "",
    "createTime": "2020-08-13T08:10:55.000+0000",
    "modifyTime": "修改时间",
    "payTime":"支付时间",
    "confirmedTime": null,
    "deliveryTime": null,
    "completeTime": null,
    "sellerMemo": "卖家备忘信息",
    "buyerFeedback": "买家留言",
    "closeReason": "",
    "receivingTime": null,
    "statusStr": "TRADE_CLOSED_BY_TAOBAO",
    "status": 11,
    "logisticsCompany": "快递公司",
    "logisticsCompanyCode": null,
    "logisticsCode": "快递单号",
    "sendStatus": 0,
    "refundId": null,
    "refundAmount": null,
    "refundStatus": null,
    "auditStatus": 0,
    "contactPerson": "",
    "mobile": "",
    "tel": null,
    "province": "省",
    "city": "市",
    "area": "区",
    "address": "地址",
    "orderSource": 0,
    "createOn": "创建时间",
    "shopId": 7,
    "developerId": 0,
    "developerName": null,
    "clientUserId": 0,
    "clientName": null,
    "isComment":"是否评价(0:未评1:已评)",
    "items": [{
    "id": 26840,
    "orderId": "订单id",
    "subItemId": "子订单id",
    "itemAmount": "子订单金额",
    "discountFee": "优惠金额",
    "adjustFee": 0.00,
    "goodsTitle": "商品标题",
    "goodsNumber": "商品编码",
    "productImgUrl": "图片",
    "productUrl": null,
    "productId": "0",
    "skuId": "0",
    "specNumber": "商品规格",
    "skuInfo": "商品规格字符串",
    "price": "价格",
    "quantity": "数量",
    "erpGoodsId": 17,
    "erpGoodsSpecId": 109,
    "statusStr": null,
    "status": null,
    "refundStatusStr": "退货状态",
    "refundStatus": null,
    "refundAmount": null,
    "refundId": null,
    "logisticsStatus": null,
    "newSpecId": 0,
    "newSpecNumber": null,
    "erpSpec": null,
    "newSpec": null,
    "currentQty": null,
    "lockedQty": null,
    "pickQty": null,
    "afterSaleState": 0
    }
    }
    }
     */
    @RequestMapping(value = "/pdd_order_detail", method = RequestMethod.POST)
    public ApiResult<OrderPddEntity> orderList(@RequestBody DataRow reqData, @RequestHeader String token) {
        var tokenInfo=getTokenInfo(token);
        if(tokenInfo.getCode()>0)return new ApiResult<>(tokenInfo.getCode(),tokenInfo.getMsg());
        try {
            OrderPddEntity result = orderPddService.getOrderDetailAndItemsById(reqData.getLong("id"));
            return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(),"成功",result);
        }catch (Exception ex){
            return new ApiResult<>(EnumResultVo.Fail.getIndex(),EnumResultVo.Fail.getName()+ex.getMessage());
        }
    }


}
