package com.b2c.api.controller;

import com.b2c.api.response.NameIndexVo;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.result.ResultVo;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;


/***
 *
 * 枚举类接口
 */
@RestController
@RequestMapping("/common_api")
public class CommonEnumController {

    @RequestMapping(value = "/enum_dict", method = RequestMethod.GET)
    public ResultVo<List<NameIndexVo>> getEnumDict(@RequestParam String enumClassName) {
        try {
            String packageName = "com.b2c.entity.enums.";
            Class clazz = Class.forName(packageName+enumClassName);

            if(clazz.isEnum()==false) return new ResultVo(EnumResultVo.DataError,"不是枚举类");

            List<NameIndexVo> lists = new ArrayList<>();


            Object[] enumConstants = clazz.getEnumConstants();

            Method getName = clazz.getMethod("getName");
            Method getIndex = clazz.getMethod("getIndex");

            for (Object object:enumConstants) {

                Integer index = Integer.parseInt(getIndex.invoke(object).toString());
                String name = getName.invoke(object).toString();
                NameIndexVo vo = new NameIndexVo(name,index);

                lists.add(vo);
            }

            return new ResultVo(EnumResultVo.SUCCESS,lists);
//            for (int i=0;i<enumConstants.length;i++) {
//                NameValueVo vo = new NameValueVo();
//                var s1 = (Enum)enumConstants[i];
//
//                //vo.setValue();
//                vo.setName(s1.name());
//            }
//            String s = "";

        }catch (Exception e){
            return new ResultVo(EnumResultVo.NotFound,"没有找到类名");
        }


    }

}
