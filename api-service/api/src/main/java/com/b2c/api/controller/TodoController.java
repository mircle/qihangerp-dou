package com.b2c.api.controller;

import com.b2c.api.query.TodoQuery;
import com.b2c.entity.TodoEntity;
import com.b2c.entity.api.ApiResult;
import com.b2c.entity.api.ApiResultEnum;
import com.b2c.entity.result.PagingResponse;
import com.b2c.interfaces.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1")
public class TodoController {
    @Autowired
    private TodoService todoService;

    @RequestMapping(value = "/todo_list", method = RequestMethod.POST)
    public  ApiResult<PagingResponse<TodoEntity>> getList(@RequestBody TodoQuery query) {

        
        var result = todoService.getList(query.getPageIndex(), query.getPageSize(), null, null, query.getStatus(), query.getContent());
        return new ApiResult<>(ApiResultEnum.SUCCESS,result);
    }
}
