package com.b2c.api.response;

import lombok.Data;

@Data
public class NameValueVo {
    private String name;
    private String value;
}
