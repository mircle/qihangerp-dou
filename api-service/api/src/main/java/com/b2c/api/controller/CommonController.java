package com.b2c.api.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.b2c.api.DataConfigObject;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.result.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonController {


    /**
     * 严重用户登录状态
     * @param token
     * @return
     */
    public ResultVo<Integer> getTokenInfo(String token){
        try {
            String secret = DataConfigObject.getInstance().getJwtSecret();
            Algorithm algorithm = Algorithm.HMAC256(secret);
            JWTVerifier verifier = JWT.require(algorithm).withIssuer("auth0").build();
            DecodedJWT jwt = verifier.verify(token);
            return new ResultVo<>(EnumResultVo.SUCCESS,"成功",jwt.getClaim("userId").asInt());
        }catch (TokenExpiredException e){
            return new ResultVo<>(EnumResultVo.TokenFail,"账号信息过期了，请重新登录");
        }catch (Exception exception) {
            return new ResultVo<>(EnumResultVo.TokenFail,exception.getMessage());
        }
    }




}
