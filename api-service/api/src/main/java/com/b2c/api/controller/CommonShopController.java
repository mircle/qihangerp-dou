package com.b2c.api.controller;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.api.ApiResultEnum;
import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.interfaces.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/common_api")
public class CommonShopController {
    @Autowired
    private ShopService shopService;
    @RequestMapping(value = "/get_shop_list", method = RequestMethod.GET)
    public ApiResult<List<DcShopEntity>> getShopList() {


        var result = shopService.shopListShow();
        return new ApiResult<>(ApiResultEnum.SUCCESS,result);
    }
}
