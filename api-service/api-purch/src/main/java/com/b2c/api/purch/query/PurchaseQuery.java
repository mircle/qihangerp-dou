package com.b2c.api.purch.query;

import com.b2c.entity.erp.enums.InvoiceBillStatusEnum;
import com.b2c.entity.erp.enums.InvoiceBillTypeEnum;
import com.b2c.entity.erp.enums.InvoiceTransTypeEnum;

public class PurchaseQuery extends PageQuery {
    private String billNo;
    private InvoiceTransTypeEnum transType;
    private InvoiceBillTypeEnum billType;
    private InvoiceBillStatusEnum billStatus;

    public InvoiceBillStatusEnum getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(InvoiceBillStatusEnum billStatus) {
        this.billStatus = billStatus;
    }

    public InvoiceBillTypeEnum getBillType() {
        return billType;
    }

    public void setBillType(InvoiceBillTypeEnum billType) {
        this.billType = billType;
    }

    private String startDate;

    private String endDate;

    private Integer contactId;

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public InvoiceTransTypeEnum getTransType() {
        return transType;
    }

    public void setTransType(InvoiceTransTypeEnum transType) {
        this.transType = transType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }
}
