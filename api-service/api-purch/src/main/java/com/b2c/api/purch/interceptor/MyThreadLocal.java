package com.b2c.api.purch.interceptor;

public class MyThreadLocal {
    private static final ThreadLocal<LoginUserLocal> threadLocal = new ThreadLocal<LoginUserLocal>();
    /**
     * 对当前线程中的map域设置属性值，不会对其他线程产生影响  Thread.currentThread()
     * @param str
     */
    public static void set(LoginUserLocal user){

        threadLocal.set(user);
    }

    public static LoginUserLocal get(){

        return threadLocal.get();
    }

    /**
     * 本质上是调用当前线程的remove，不会对其他线程产生影响
     */
    public static void remove(){

        threadLocal.remove();
    }

}
