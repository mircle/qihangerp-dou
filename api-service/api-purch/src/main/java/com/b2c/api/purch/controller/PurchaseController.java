package com.b2c.api.purch.controller;

import com.alibaba.fastjson.JSON;
import com.b2c.api.purch.DataConfigObject;
import com.b2c.api.purch.interceptor.MyThreadLocal;
import com.b2c.api.purch.query.PurchaseQuery;
import com.b2c.common.utils.DateUtil;
import com.b2c.entity.ContactEntity;
import com.b2c.entity.api.ApiResult;
import com.b2c.entity.api.ApiResultEnum;
import com.b2c.entity.enums.EnumUserActionType;
import com.b2c.entity.enums.IsDeleteEnum;
import com.b2c.entity.erp.InvoiceEntity;
import com.b2c.entity.erp.InvoiceInfoEntity;
import com.b2c.entity.erp.enums.ContactTypeEnum;
import com.b2c.entity.erp.enums.InvoiceBillStatusEnum;
import com.b2c.entity.erp.enums.InvoiceBillTypeEnum;
import com.b2c.entity.erp.enums.InvoiceTransTypeEnum;
import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.vo.ErpPurchaseStockInItemVo;
import com.b2c.entity.vo.finance.ErpStockInFormItemVo;
import com.b2c.entity.vo.wms.InvoiceDetailVo;
import com.b2c.interfaces.wms.*;
import com.b2c.repository.utils.OrderNumberUtils;
import org.apache.poi.hssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 描述：
 * 采购Controller
 *
 * @author qlp
 * @date 2019-03-21 13:41
 */
@RestController
@RequestMapping("/api/v1")
public class PurchaseController {
    @Autowired
    private ContactService contactService;
    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private StockInService stockInService;
    private static Logger log = LoggerFactory.getLogger(PurchaseController.class);
    /**
     * 出货单列表
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/purchase/list", method = RequestMethod.POST)
    public ApiResult<PagingResponse<InvoiceEntity>> getPurchaseList(@RequestBody PurchaseQuery query) {
        log.info("访问purchase_list,userId:"+MyThreadLocal.get().getUserId()+"userName:"+ MyThreadLocal.get().getUserName());
        query.setBillType(InvoiceBillTypeEnum.Purchase);
        PagingResponse<InvoiceEntity> result = invoiceService.getList(
                query.getPageIndex(),
                query.getPageSize(),
                query.getTransType(),
                query.getBillType(),
                query.getBillStatus(), null, null,
                query.getBillNo(),
                query.getStartDate(),
                query.getEndDate(),
                query.getContactId());

        return new ApiResult<>(ApiResultEnum.SUCCESS,result);

    }


    /***
     * 单详情(打印，弹窗)
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/purchase/detail", method = RequestMethod.GET)
    public String purchaseDetail(Model model, HttpServletRequest request, @RequestParam Long id) {
        var detail = invoiceService.getInvoiceDetail(id);
        model.addAttribute("detail", detail);
        return "purchase_detail";
    }



//
//    /**
//     * 添加购货单POST
//     *
//     * @param model
//     * @param request
//     * @return
//     */
//    @RequestMapping(value = "/purchase/add", method = RequestMethod.POST)
//    public String getPurchaseAddPost(Model model, HttpServletRequest request) {
//
//
//        String userName = request.getParameter("userName");
//
//        /***表单信息***/
//        String transType = request.getParameter("transType");
//        String transTypeName = "";
//        if(StringUtils.isEmpty(transType)){
//            transType = InvoiceTransTypeEnum.Purchase.getIndex();
//            transTypeName = InvoiceTransTypeEnum.Purchase.getName();
//        }else{
//            transTypeName = InvoiceTransTypeEnum.valueOf(transType).getName();
//        }
//        String srcOrderId = request.getParameter("srcOrderId");
//
//        Long supplierId = Long.parseLong(request.getParameter("supplierId"));
//        String billNo = request.getParameter("billNo");
//        String billDate = request.getParameter("billDate");
//        Double invoiceDisRate = Double.valueOf(request.getParameter("invoiceDisRate"));//优惠率
//        Double invoiceDisAmount = Double.valueOf(request.getParameter("invoiceDisAmount"));//优惠金额
//        Double invoiceAmount = Double.valueOf(request.getParameter("invoiceAmount"));//优惠后金额
//        Double rpAmount = Double.valueOf(request.getParameter("rpAmount"));//本次付款
//        Double arrears = Double.valueOf(request.getParameter("arrears"));//本次欠款
//        String contractNo = request.getParameter("contractNo");
//
//        /***要计算的内容***/
//        Double totalAmount = 0.0;//购货总金额（商品价格*数量之和）
//        Long totalQuantity = 0l;//总数量(商品数量之和）
//                Double totalDiscount = 0.0;//总折扣（计算商品折扣和单据折扣之和）
//
//
//        /***商品信息****/
//        String[] specNumber = request.getParameterValues("specNumber");//规格编码组合
//        String[] goodsNumber = request.getParameterValues("goodsNumber");//商品编码组合
//        String[] goodsId = request.getParameterValues("goodsId");//商品id组合
//        String[] specId = request.getParameterValues("specId");//商品规格id组合
//
//        String[] quantity = request.getParameterValues("quantity");//数量组合
//        String[] price = request.getParameterValues("price");//购货单价
//        String[] disRate = request.getParameterValues("disRate");//折扣率(%)
//        String[] disAmount = request.getParameterValues("disAmount");//折扣额
//        String[] amount = request.getParameterValues("amount");//购货金额
//        //String[] number = request.getParameterValues("number");//序列号
//        String[] note = request.getParameterValues("note");//备注
//        String[] itemId = request.getParameterValues("itemId");
//
//
//        List<InvoiceInfoEntity> invoiceInfo = new ArrayList<>();
//        if (specNumber != null && specNumber.length > 0) {
//            //循环组合商品信息
//            for (int i = 0; i < specNumber.length; i++) {
//                InvoiceInfoEntity info = new InvoiceInfoEntity();
//                if (StringUtils.isEmpty(specNumber[i]) == false) {
//                    info.setSpecNumber(specNumber[i]);
//                    info.setGoodsNumber(goodsNumber[i]);
//                    if (goodsId.length > i) info.setGoodsId(Integer.valueOf(goodsId[i]));
//                    if (specId.length > i) info.setSpecId(Integer.valueOf(specId[i]));
//                    if (goodsNumber.length > i) info.setGoodsNumber(goodsNumber[i]);
//                    if (quantity.length > i) info.setQuantity(Long.valueOf(quantity[i]));
//                    if (price.length > i) info.setPrice(Double.valueOf(price[i]));
//                    if (disRate.length > i) info.setDisRate(Double.valueOf(disRate[i]));
//                    if (disAmount.length > i) info.setDisAmount(0d);//Double.valueOf(disAmount[i])
//                    if (amount.length > i) info.setAmount(Double.valueOf(amount[i]));
//                    //if (number.length > i) info.setSerialno(number[i]);
//                    if (note.length > i) info.setDescription(note[i]);
//                    //if (locationId.length > i) info.setLocationId(Integer.valueOf(locationId[i]));
//                    if (info.getPrice() != null && info.getQuantity() != null) {
//                        totalQuantity += info.getQuantity();
//                        totalAmount += info.getPrice() * info.getQuantity();
//                    }
//                    if (info.getDisAmount() != null) totalDiscount += info.getDisAmount();
//                    if(itemId!=null && itemId.length > i){
//                        info.setSrcOrderNo(itemId[i]);
//                    }
//                    invoiceInfo.add(info);
//                }
//            }
//        }
//        if (invoiceDisAmount != null) totalDiscount += invoiceDisAmount;
//
//        if (invoiceInfo == null || invoiceInfo.size() == 0) return "redirect:/stock/purchase_add";
//
//        InvoiceEntity invoiceEntity = new InvoiceEntity();
//        invoiceEntity.setSrcOrderId(srcOrderId);
//        invoiceEntity.setContactId(supplierId);
//        invoiceEntity.setBillNo(billNo);
//        invoiceEntity.setBillDate(billDate);
//        invoiceEntity.setContractNo(contractNo);
//        invoiceEntity.setUserId(userId);
//        invoiceEntity.setUserName(userName);
//        invoiceEntity.setTransType(transType);
//        invoiceEntity.setTransTypeName(transTypeName);
//        invoiceEntity.setTotalAmount(totalAmount);
//        invoiceEntity.setDisRate(invoiceDisRate);
//        invoiceEntity.setDisAmount(invoiceDisAmount);
//        invoiceEntity.setAmount(invoiceAmount);
//        invoiceEntity.setTotalDiscount(totalDiscount);
//        invoiceEntity.setTotalQuantity(totalQuantity);
//        invoiceEntity.setFreight(StringUtils.hasText(request.getParameter("freight")) ? Double.valueOf(request.getParameter("freight")):0.0);
//        invoiceEntity.setRpAmount(rpAmount);
//        invoiceEntity.setArrears(arrears);
//        invoiceEntity.setBillType(InvoiceBillTypeEnum.Purchase.getIndex());
//        invoiceEntity.setBillStatus(InvoiceBillStatusEnum.WaitAudit.getIndex());
//        invoiceEntity.setChecked(InvoiceBillStatusEnum.WaitAudit.getIndex());
//        invoiceEntity.setIsDelete(IsDeleteEnum.Normal.getIndex());
//        invoiceEntity.setCreateTime(System.currentTimeMillis() / 1000);
//
//        int result = invoiceService.purchaseFormSubmit(invoiceEntity, invoiceInfo);
//
//
//        return "redirect:/purchase/list";
//    }

    @ResponseBody
    @RequestMapping(value = "/purchase/purchase_in_export", method = RequestMethod.GET)
    public void purchasePutDetail(Model model, HttpServletRequest req, HttpServletResponse response) {

        var lists = invoiceService.invoiceItemPOList(req.getParameter("startTime"),req.getParameter("endTime"));

        /***************根据店铺查询订单导出的信息*****************/
        String fileName = "purchase_in_export_";//excel文件名前缀
        //创建Excel工作薄对象
        HSSFWorkbook workbook = new HSSFWorkbook();

        //创建Excel工作表对象
        HSSFSheet sheet = workbook.createSheet();
        HSSFRow row = null;
        HSSFCell cell = null;
        //第一行为空
        row = sheet.createRow(0);
        cell = row.createCell(0);
        cell.setCellValue("");
        cell = row.createCell(0);
        cell.setCellValue("采购单号");
        cell = row.createCell(1);
        cell.setCellValue("合同号");
        cell = row.createCell(2);
        cell.setCellValue("入库时间");
        cell = row.createCell(3);
        cell.setCellValue("数量");
        cell = row.createCell(4);
        cell.setCellValue("采购价");
        cell = row.createCell(5);
        cell.setCellValue("标题");
        cell = row.createCell(6);
        cell.setCellValue("规格编码");
        cell = row.createCell(7);
        cell.setCellValue("规格属性");

        int currRowNum = 0;
        HSSFPatriarch patriarch = sheet.createDrawingPatriarch();
        // 循环写入数据
        for (int i = 0; i < lists.size(); i++) {
            currRowNum++;
            //写入订单
            ErpStockInFormItemVo itemVo = lists.get(i);
            //创建行
            row = sheet.createRow(currRowNum);
            cell = row.createCell(0);
            cell.setCellValue(itemVo.getBillNo());
            //商品名称
            cell = row.createCell(1);
            cell.setCellValue(itemVo.getContractNo());
            cell = row.createCell(2);
            cell.setCellValue(DateUtil.stampToDateTime(itemVo.getStockInTime1()));

            cell = row.createCell(3);
            cell.setCellValue(itemVo.getQuantity());
            //退货单号
            cell = row.createCell(4);
            cell.setCellValue(String.valueOf(itemVo.getPrice()));
            //规格
            cell = row.createCell(5);
            cell.setCellValue(itemVo.getGoodsName());
            cell = row.createCell(6);
            cell.setCellValue(itemVo.getSpecNumber());
            cell = row.createCell(7);
            cell.setCellValue(itemVo.getColorValue()+itemVo.getSizeValue());
        }
        response.reset();
        response.setContentType("application/msexcel;charset=UTF-8");
        try {
            SimpleDateFormat newsdf = new SimpleDateFormat("yyyyMMdd");
            String date = newsdf.format(new Date());
            response.addHeader("Content-Disposition", "attachment;filename=\""
                    + new String((fileName + date + ".xls").getBytes("GBK"),
                    "ISO8859_1") + "\"");
            OutputStream out = response.getOutputStream();
            workbook.write(out);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "导出失败!");
            e.printStackTrace();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "导出失败!");
            e.printStackTrace();
        }
    }


}
